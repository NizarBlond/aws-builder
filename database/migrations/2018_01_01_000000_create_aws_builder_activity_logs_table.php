<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwsBuilderActivityLogsTable extends Migration
{
    /**
     * The table name.
     *
     * @var string
     */
    private $tableName;

    /**
     * The constructor.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->tableName = config('aws-builder.database.table');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('template')->nullable();
            $table->string('service')->index();
            $table->string('function');
            $table->jsonb('input')->nullable();
            $table->jsonb('output')->nullable();
            $table->integer('duration_s')->nullable();
            $table->longText('error_msg')->nullable();
            $table->longText('error_stack')->nullable();
            $table->integer('error_level')->default(-1);
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->tableName);
    }
}
