AWS Builder Package
===================

## Important Notice

This is an internal developer tool and has not been tested, thus stability is NOT guaranteed. Please use at your own risk.

I just felt like sharing it for it might be useful to someone.

### Install

#### Install Through Composer

You can either add the package directly by firing this command

``` bash
$ composer require nizarblond/aws-builder:~1.0
```
    
Or add in the `require` key of `composer.json` file manually

``` json
"nizarblond/aws-builder": "~1.0"
```

And Run the Composer update command

``` bash
$ composer update
```

#### Add Service Provider

Add this service provider to your `bootstrap/app.php` file.

``` php
$app->register(NizarBlond\AwsBuilder\AwsBuilderServiceProvider::class);
```

### Config Input Sample

``` php

// TO BE ADDED

```

### AWS Policy

You're strongly advised NOT to use it in production before testing it on a dev account.

The dev account should have the following IAM policy that only allows it to modify resources
that has the text "dev" included in their name.

``` php
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "RestrictedAWSFullAccess",
            "Effect": "Allow",
            "NotAction": [
                "s3:*",
                "rds:*",
                "iam:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "AllowUserToSeeBucketListInTheConsole",
            "Action": [
                "s3:ListAllMyBuckets",
                "s3:GetBucketLocation"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::*"
            ]
        },
        {
            "Sid": "FullPermissionstoS3BucketWithTextDev",
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "arn:aws:s3:::*dev*"
            ]
        },
        {
            "Sid": "S3RdsIamNonResourceLevelPermissions",
            "Effect": "Allow",
            "Action": [
                "s3:HeadBucket",
                "s3:ListObjects",
                "iam:Get*",
                "iam:List*",
                "iam:Simulate*",
                "rds:Describe*",
                "rds:List*",
                "rds:DownloadCompleteDBLogFile",
                "rds:PurchaseReservedDBInstancesOffering"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "RdsAccessOnlyToDevResources",
            "Action": [
                "rds:*"
            ],
            "Effect": "Allow",
            "Resource": "arn:aws:rds:*:*:*:*dev*"
        },
        {
            "Sid": "IamAccessOnlyToDevResources",
            "Action": [
                "iam:*"
            ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:iam::*:user/*dev*",
                "arn:aws:iam::*:group/*dev*",
                "arn:aws:iam::*:role/*dev*",
                "arn:aws:iam::*:policy/*dev*",
                "arn:aws:iam::*:server-certificate/*dev*",
                "arn:aws:iam::*:saml-provider/*dev*",
                "arn:aws:iam::*:oidc-provider/*dev*"
            ]
        }
    ]
}
```

Enjoy and don't forget, this is an incomplete package.