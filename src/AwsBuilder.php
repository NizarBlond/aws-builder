<?php

namespace NizarBlond\AwsBuilder;

use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use NizarBlond\LaravelPlus\Support\Arr;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\Url;
use NizarBlond\AwsBuilder\Clients\Pdo\PdoClient;
use NizarBlond\AwsBuilder\Clients\Aws\Ec2Client;
use NizarBlond\AwsBuilder\Clients\Aws\ElasticBeanstalkClient;
use NizarBlond\AwsBuilder\Clients\Aws\S3Client;
use NizarBlond\AwsBuilder\Clients\Aws\RdsClient;
use NizarBlond\AwsBuilder\Clients\Aws\IamClient;
use NizarBlond\AwsBuilder\Clients\Aws\Route53Client;
use NizarBlond\AwsBuilder\Clients\Aws\CloudFrontClient;
use NizarBlond\AwsBuilder\Clients\Aws\AcmClient;
use NizarBlond\AwsBuilder\Clients\Aws\LambdaClient;
use NizarBlond\AwsBuilder\Clients\Aws\ElastiCacheClient;
use TorMorten\Eventy\Events;
use Aws\Exception\AwsException;
use Exception;
use Throwable;

class AwsBuilder
{
    use InstanceHelpers;

    /**
     * The AWS region.
     *
     * @var string
     */
    private $awsRegion;

    /**
     * The AWS ACM region.
     *
     * @var string
     */
    private $awsAcmRegion;

    /**
     * The resources spec.
     *
     * @var array
     */
    private $resources;

    /**
     * The AWS Elastic Beanstalk client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\ElasticBeanstalkClient
     */
    private $ebClient;

    /**
     * The AWS IAM client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\AwsIamClient
     */
    private $iamClient;

    /**
     * The AWS RDS client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\RdsClient
     */
    private $rdsClient;

    /**
     * The AWS S3 client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\S3Client
     */
    private $s3Client;

    /**
     * The AWS EC2 client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\Ec2Client
     */
    private $ec2Client;

    /**
     * The AWS Route53 client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\Ec2Client
     */
    private $route53Client;

    /**
     * The AWS CloudFront client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\CloudFrontClient
     */
    private $cloudFrontClient;

    /**
     * The AWS Lambda client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\LambdaClient
     */
    private $lambdaClient;

    /**
     * The AWS EC2 client.
     *
     * @var \NizarBlond\AwsBuilder\Clients\Aws\ElastiCacheClient
     */
    private $elastiCacheClient;

    /**
     * The current template's VPC ID.
     *
     * @var string
     */
    private $vpcId;

    /**
     * The current template's VPC subnet IDs.
     *
     * @var array
     */
    private $vpcSubnetIds;

    /**
     * Whether to ignore production warning.
     *
     * @var array
     */
    private $ignoreProductionWarning = false;

    /**
     * WordPress-like actions and filters for Laravel.
     *
     * @var \TorMorten\Eventy\Events;
     */
    private $events;

    /**
     * The templates stack.
     *
     * @var array
     */
    private $templates;

    /**
     * The current template being handled.
     *
     * @var array
     */
    private $currentTemplate;

    /**
     * The last template was handled but might not be finished.
     *
     * @var string
     */
    private $lastTemplateHandled;

    /**
     * The last template was handled but might not be finished.
     *
     * @var string
     */
    private $lastResourceHandled;

    /**
     * Whether to suppress throwing exception on failure.
     *
     * @var boolean
     */
    private $suppressException = false;

    /**
     * The list of thrown exceptions.
     *
     * @var array
     */
    private $thrownExceptions = [];

    /**
     * The constructor.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->validateAwsBuilderConfig();

        $this->events = new Events;
        $this->awsRegion = config('aws-builder.aws.region');
        $this->awsAcmRegion = config('aws-builder.aws.acm_region');
    }

    public function addTemplate($params, $name = "Template")
    {
        if (is_string($params)) {
            $params = json_decode($params, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                $this->exception("Badly formatted JSON template params.");
            }
        }

        $this->validateTemplateParams($params);

        $this->templates[$name] = [
            'Name'          => $name,
            'Params'        => $this->resolveTemplateParamsVars($params),
            'AwsResults'    => []
        ];
    }

    public function hasTemplates()
    {
        return ! empty($this->templates);
    }

    public function getTemplate($name)
    {
        if (! isset($this->templates[$name])) {
            $this->exception("Template not found.");
        }

        return $this->templates[$name];
    }

    /**
     * Builds the stack.
     *
     * @return array
     */
    public function build()
    {
        if (empty($this->templates)) {
            $this->exception("You cannot build empty templates.");
        }

        $this->validateBuildParams();
        
        foreach ($this->templates as $name => $template) {
            $this->currentTemplate = $template;
            $this->initAwsClients($name);
            try {
                $this->lastTemplateHandled = $name;
                $this->buildTemplate($template);
            } catch (Throwable | Exception | \Exception $e) {
                $error = sprintf(
                    "Encountered exception (build): class=%s, message: %s",
                    get_class($e),
                    $e->getMessage()
                );
                $this->logError($error);

                // Add exception to the thrown exception array
                $this->thrownExceptions[] = $e;
                
                // Try destroy on failure
                try {
                    $this->destroyOnFailure($e);
                } catch (Throwable | Exception | \Exception $e2) {
                    // Ignore exception on destroy but just report it
                    $this->thrownExceptions[] = $e2;
                    $error2 = sprintf(
                        "Encountered exception (destroy on failure): class=%s, message: %s",
                        get_class($e2),
                        $e2->getMessage()
                    );
                    $this->logError($error2);
                }

                // Check whether to throw exception
                if ($this->suppressException) {
                    return false;
                }
                throw $this->getFirstException();
            }

            $this->templates[$name] = $this->currentTemplate;
            $this->cleanCurrentTemplate();
        }

        return $this->templates;
    }

    /**
     * Destroys the stack.
     *
     * @param   boolean  $stopAtLastHandledTemplate
     * @return  void
     */
    public function destroy($stopAtLastHandledTemplate = false, $stopAtLastHandledResource = false)
    {
        if (empty($this->templates)) {
            $this->exception("You cannot destroy empty templates.");
        }

        foreach ($this->templates as $name => $template) {
            $this->currentTemplate = $template;
            $this->initAwsClients($name);
            $this->destroyTemplate($template, $stopAtLastHandledResource);
            $this->cleanCurrentTemplate();
            if ($stopAtLastHandledTemplate && $this->lastTemplateHandled === $name) {
                $this->log("Stopped destroying stack after handling '$name' template.");
                break;
            }
        }
    }

    /**
     * Destroys backwardly on failure.
     *
     * @return boolean
     */
    private function destroyOnFailure()
    {
        $name = $this->currentTemplate['Name'];

        // Stop execution and destroy stack
        $this->logError("Building '$name' failed.");
        $errorMsg = sprintf(
            "Stopping execution at '%s' (included) resource in '%s' template...",
            $this->lastResourceHandled,
            $this->lastTemplateHandled
        );
        $this->logError($errorMsg);
        $this->destroy(true, true);
    }

    /**
     * Returns all thrown exceptions.
     *
     * @return array
     */
    public function getExceptions()
    {
        return $this->thrownExceptions;
    }

    /**
     * Returns the first exception.
     *
     * @return \Exception
     */
    public function getFirstException()
    {
        return Arr::first($this->thrownExceptions ?? []);
    }

    /**
     * Returns the last exception.
     *
     * @return \Exception
     */
    public function getLastException()
    {
        return Arr::last($this->thrownExceptions ?? []);
    }

    /**
     * Suppresses throwing exception.
     *
     * @return void
     */
    public function suppressException()
    {
        $this->suppressException = true;
    }

    /**
     * Allows throwing exception.
     *
     * @return void
     */
    public function unsuppressException()
    {
        $this->suppressException = false;
    }

    /**
     * Ignores production warning when destroying resources.
     *
     * @return  void
     */
    public function ignoreProductionWarning()
    {
        $this->ignoreProductionWarning = true;
    }

    /**
     * Returns the template's VPC ID.
     *
     * @return  void
     */
    public function getCurrentTemplateVpcId()
    {
        if (! empty($this->vpcId)) {
            return $this->vpcId;
        }

        $tmpltParams = $this->currentTemplate['Params'];
        if (! empty($tmpltParams['VpcId'])) {
            $this->vpcId = $tmpltParams['VpcId'];
        } elseif (! empty($tmpltParams['VpcName'])) {
            $this->vpcId = $this->ec2Client->describeVpcByName($tmpltParams['VpcName'])['VpcId'];
        } else {
            $this->exception("The template has no VPC specified.");
        }

        return $this->vpcId;
    }

    /**
     * Starts the follownig type of resources if their name end with
     * the specified suffix:
     * - RDS (instance identifier)
     * - ElasticBeanstalk (environment name)
     * - EC2 (tag:Name value)
     *
     * @param   string      $name
     * @param   Function    $func
     * @param   integer     $priority
     * @param   integer     $numOfArgs
     *
     * @return  void
     */
    public function startResourcesByNameSuffix($suffix)
    {
        $this->print("Starting resources that their name contains '$suffix'...");

        /* Elastic Beanstalk */
        $this->print("Trying to start Elastic Beanstalk instances...");

        $ebEnvs = $this->ebClient->describeEnvironments();
        foreach ($ebEnvs ?? [] as $ebEnv) {
            $id = $ebEnv['EnvironmentId'];
            $name = $ebEnv['EnvironmentName'];
            $status = $ebEnv['Status'];

            if (! Str::endsWith($name, $suffix)) {
                $this->print("Not starting $name (name doesn't ends with '$suffix')");
                continue;
            }

            $this->print("Starting $name evnironment...");
            $this->ebClient->startEnvironment($id);
        }

        /* RDS */
        $this->print("Trying to start RDS instances...");

        $dbInstances = $this->rdsClient->describeDbInstances();
        foreach ($dbInstances ?? [] as $dbInstance) {
            $instanceId = $dbInstance['DBInstanceIdentifier'];

            if (! Str::endsWith($instanceId, $suffix)) {
                $this->print("Not starting $instanceId (name doesn't ends with '$suffix')");
                continue;
            }

            $instanceStatus = $dbInstance['DBInstanceStatus'];
            if ($instanceStatus !== 'stopped') {
                $this->print("Not starting $instanceId instance (status=$instanceStatus).");
                continue;
            }

            $this->print("Starting $instanceId (status=$instanceStatus)...");
            $this->rdsClient->startDbInstance($instanceId);
        }

        /* EC2 */
        $this->print("Trying to start EC2 instances...");

        $ec2Instances = $this->ec2Client->describeInstances();
        foreach ($ec2Instances ?? [] as $ec2Instance) {
            $state = $ec2Instance['State']['Name'];
            $tags = $ec2Instance['Tags'] ?? [];
            $name = $this->getAwsTagValue($tags, 'Name');
            $appEnv = $this->getAwsTagValue($tags, 'APP_ENV');
            $isEb = ! empty($this->getAwsTagValue($tags, 'elasticbeanstalk:environment-id'));

            if (! Str::endsWith($name, $suffix)) {
                $this->print("Not starting $name instance (name doesn't end with '$suffix').");
                continue;
            }

            if ($isEb) {
                $this->print("Not starting $name instance (part of Elastic Beanstalk).");
                continue;
            }

            $instanceId = $ec2Instance['InstanceId'];
            $this->print("Starting $name instance (InstanceId=$instanceId).");
            $this->ec2Client->startInstance($instanceId);
        }
    }

    /**
     * Stops the follownig type of resources if their name end with
     * the specified suffix:
     * - RDS (instance identifier)
     * - ElasticBeanstalk (environment name)
     * - EC2 (tag:Name value)
     *
     * @param   string      $name
     * @param   Function    $func
     * @param   integer     $priority
     * @param   integer     $numOfArgs
     *
     * @return  void
     */
    public function stopResourcesByNameSuffix($suffix)
    {
        $this->print("Starting resources that their name contains '$suffix'...");

        /* Elastic Beanstalk */
        $this->print("Trying to stop Elastic Beanstalk instances...");

        $ebEnvs = $this->ebClient->describeEnvironments();
        foreach ($ebEnvs ?? [] as $ebEnv) {
            $id = $ebEnv['EnvironmentId'];
            $name = $ebEnv['EnvironmentName'];
            $status = $ebEnv['Status'];

            if (! Str::endsWith($name, $suffix)) {
                $this->print("Not stopping $name (name doesn't ends with '$suffix')");
                continue;
            }

            if ($status === 'Terminated') {
                $this->print("Not stopping $name (status=$status)");
                continue;
            }

            $this->print("Stopping $name environment...");
            $this->ebClient->stopEnvironment($id);
        }

        /* RDS */
        $this->print("Trying to stop RDS instances...");

        $dbInstances = $this->rdsClient->describeDbInstances();
        foreach ($dbInstances ?? [] as $dbInstance) {
            $instanceId = $dbInstance['DBInstanceIdentifier'];

            if (! Str::endsWith($instanceId, $suffix)) {
                $this->print("Not stopping $instanceId (name doesn't ends with '$suffix')");
                continue;
            }

            $instanceStatus = $dbInstance['DBInstanceStatus'];
            if ($instanceStatus !== 'available') {
                $this->print("Not stopping $instanceId (status is $instanceStatus)...");
                continue;
            }

            $this->print("Stopping $instanceId RDS instance...");
            $this->rdsClient->stopDBInstance($instanceId);
        }

        // EC2
        $this->print("Trying to stop EC2 instances...");

        $ec2Instances = $this->ec2Client->describeInstances();
        foreach ($ec2Instances as $ec2Instance) {
            $state = $ec2Instance['State']['Name'];
            $tags = $ec2Instance['Tags'];
            $name = $this->getAwsTagValue($tags, 'Name');
            $appEnv = $this->getAwsTagValue($tags, 'APP_ENV');
            $isEb = ! empty($this->getAwsTagValue($tags, 'elasticbeanstalk:environment-id'));

            if (! Str::endsWith($name, $suffix)) {
                $this->print("Not stopping $name instance (name doesn't end with '$suffix').");
                continue;
            }

            if ($isEb) {
                $this->print("Not stopping $name instance (part of Elastic Beanstalk).");
                continue;
            }

            if ($state !== 'running') {
                $this->print("Not stopping $name instance (state is '$state' and not running).");
                continue;
            }

            $instanceId = $ec2Instance['InstanceId'];
            $this->print("Stopping $name instance (InstanceId=$instanceId).");
            $this->ec2Client->stopInstance($instanceId);
        }
    }

    /**
     * Adds a filter function to be called dynamically.
     *
     * @param   string      $name
     * @param   Function    $func
     * @param   integer     $priority
     * @param   integer     $numOfArgs
     *
     * @return  void
     */
    public function addFilter($name, $func, $priority = 10, $numOfArgs = 1)
    {
        $this->events->addFilter($name, $func, $priority, $numOfArgs);
    }

    /**
     * Adds an action function to be called dynamically.
     *
     * @param   string      $name
     * @param   Function    $func
     * @param   integer     $priority
     * @param   integer     $numOfArgs
     *
     * @return  void
     */
    public function addAction($name, $func, $priority = 10, $numOfArgs = 1)
    {
        $this->events->addAction($name, $func, $priority, $numOfArgs);
    }

    /*******************************************
     * EC2 Methods
     *******************************************/

    /**
     * Builds the specified EC2 spec.
     *
     * @param   array     $ec2
     *
     * @return  void
     */
    private function buildEc2Resources($ec2)
    {
        $vpc            = $this->createDefaultVpc($ec2['Vpc'] ?? []);
        $securityGroups = $this->createSecurityGroups($ec2['SecurityGroups'] ?? []);
        $ec2Instances   = $this->createEc2Instances($ec2['Instances'] ?? []);

        return [
            'Vpc'               => $vpc,
            'SecurityGroups'    => $securityGroups,
            'Instances'         => $ec2Instances,
            'VpcSubnetIds'      => $this->getCurrentTemplateVpcSubnetIds()
        ];
    }

    /**
     * Destroys the specified EC2 spec.
     *
     * @param   array     $ec2
     *
     * @return  void
     */
    private function destroyEc2Resources($ec2)
    {
        $this->terminateEc2Instances($ec2['Instances'] ?? []);
        $this->deleteSecurityGroups($ec2['SecurityGroups'] ?? []);

        // $vpcId = $this->getCurrentTemplateVpcId();
        // if (! empty($vpcId)) {
        //     $this->ec2Client->deleteVpc($vpcId ?? []);
        // }
    }

    /**
     * Creates a default VPC.
     *
     * @param   array     $vpc
     *
     * @return  void
     */
    private function createDefaultVpc($vpc)
    {
        if (empty($vpc)) {
            return null;
        }

        // Check if VPC name exists
        if (! is_null($this->ec2Client->describeVpcByName($vpc['Name']))) {
            $this->exception("The VPC name already exists.");
        }

        // Create the VPC
        return $this->ec2Client->createDefaultVpc(
            $vpc['BlockCidrFormat'],
            $vpc['Name'] ?? ""
        )['Vpc'];
    }

    /**
     * Creates the specified EC2 settings security groups.
     *
     * @param   array     $ec2
     *
     * @return  void
     */
    private function createSecurityGroups($groups)
    {
        if (empty($groups)) {
            return null;
        }

        $responses = [];

        foreach ($groups as $group) {
            $responses[] = $this->createSecurityGroup($group);
        }

        foreach ($groups as $group) {
            $this->setSecurityGroupIngressRules($group);
        }

        return $responses;
    }

    /**
     * Deletes the specified EC2 security groups.
     *
     * @param   array     $ec2
     *
     * @return  void
     */
    private function deleteSecurityGroups($groups)
    {
        foreach ($groups as $group) {
            $grpId = $this->getSecurityGroupId($group['Name']);
            if (! empty($grpId)) {
                $this->ec2Client->deleteSecurityGroup($grpId);
            }
        }
    }

    /**
     * Creates a new key pair if not exists.
     *
     * @param   string     $keyPairName
     *
     * @return  array
     */
    private function createKeyPairIfNotExists($keyPairName)
    {
        if ($this->ec2Client->keyPairExists($keyPairName)) {
            $this->log("The key pair '$keyPairName' already exists.");
            return;
        }

        $newKeyPair = $this->ec2Client->createKeyPair($keyPairName);

        /**
         * This hook is called during after the EC2 Key Pair is created.
         *
         * @param   array   $newKeyPair
         *
         * @return  void
         */
        $this->events->action('after_ec2_key_pair_created', $newKeyPair);

        return $newKeyPair;
    }

    /**
     * Creates the specified EC2 instances.
     *
     * @param   array     $instances
     *
     * @return  void
     */
    private function createEc2Instances($instances)
    {
        if (empty($instances)) {
            return null;
        }

        return array_map(function ($instance) {
            return $this->createEc2Instance($instance);
        }, $instances);
    }

    /**
     * Creates an EC2 instance.
     *
     * @param   array       $instance
     * @param   boolean     $associateIp
     *
     * @return  array
     */
    private function createEc2Instance($instance)
    {
        $requiredDataFields = [ 'ImageId', 'InstanceType', 'Name' ];
        Arr::validateFields($instance, $requiredDataFields, true);

        // Check if ec2 instance exists
        if ($this->ec2InstanceExists($instance, true /* ignore terminated */)) {
            $this->exception("An EC2 instance with a similar name already exists.");
        }

        // Check if tags number less than 50
        if (isset($instance['Tags']) && count($instance['Tags']) > 50) {
            $this->exception("An EC2 instance can only have 50 tags.");
        }

        // Try get user data from file
        $dataFile = $instance['UserData']['File'] ?? null;
        if (! empty($dataFile)) {
            $path = base_path($dataFile);
            $instance['UserData']['Content'] = file_get_contents($path);
        }

        // Try to create a default or custom key name if not exists
        $keyPairName = $instance['KeyName'] ?? $instance['Name'];
        $this->createKeyPairIfNotExists($keyPairName);

        // Create instance
        $ec2Instance = $this->ec2Client->runSingleInstance(
            $instance['Name'],
            $instance['ImageId'],
            $instance['SubnetId'] ?? $this->getCurrentTemplateVpcSubnetIds()[0],
            $instance['InstanceType'],
            $this->mapSecurityGroupNamesToIds($instance['SecurityGroups']),
            $keyPairName,
            $instance['Tags'],
            $instance['UserData']['Content'] ?? null,
            $instance['IamInstanceProfileArn'],
            $instance['BlockDeviceMappings'] ?? null,
            $instance['Monitoring']['Enabled'] ?? false
        );

        $eip = $instance['PublicIp'] ?? null;

        // Check whether we need to create a new EIP or associate a new one
        if (is_null($eip)) {
            // Allocate a new EIP
            $eip = $this->ec2Client->allocateAddress();
        } else {
            // Disassociate EIP from its instance
            $this->ec2Client->disassociateAddress($eip);
        }

        // Wait until EC2 instance is ready
        $this->waitUntilEc2InstanceIsReady($ec2Instance['InstanceId']);

        // Associate EIP
        $this->ec2Client->associateAddress($ec2Instance['InstanceId'], $eip);

        // Get instance after associating EIP
        $ec2Instance = $this->ec2Client->describeSingleInstance($ec2Instance['InstanceId']);

        return $ec2Instance;
    }

    /**
     * Terminates the specified EC2 instances.
     *
     * @param   array     $instances
     *
     * @return  void
     */
    private function terminateEc2Instances($instances)
    {
        if (empty($instances)) {
            return null;
        }

        return array_map(function ($instance) {
            return $this->terminateEc2Instance($instance);
        }, $instances);
    }

    /**
     * Terminates the specified EC2 instance.
     *
     * @param   array     $instances
     *
     * @return  void
     */
    private function terminateEc2Instance($instance)
    {
        $requiredDataFields = [ 'Name' ];
        Arr::validateFields($instance, $requiredDataFields, true);

        $ec2Instances = $this->ec2Client->describeInstancesByNameTag($instance['Name']);
        foreach ($ec2Instances as $ec2Instance) {
            // Terminate instance
            $this->ec2Client->terminateInstance($ec2Instance['InstanceId']);
        }

        if (!empty($instance['KeyName']) && !empty($instance['DeleteKeyOnDestroy'])) {
            $this->log("Deleting key pair {$instance['KeyName']}...");
            $this->ec2Client->deleteKeyPair($instance['KeyName']);
        }
    }

    private function ec2InstanceExists($instance, $ignoreTerminated = false)
    {
        $similarEc2s = $this->ec2Client->describeInstancesByNameTag($instance['Name']) ?? null;
        if (empty($similarEc2)) {
            return false;
        }

        if ($ignoreTerminated) {
            foreach ($similarEc2s as $ec2) {
                if ($ec2['State'] != 'terminated') {
                    return true;
                } else {
                    $this->log("Ignored terminated EC2 instance while checking if EC2 exists.");
                }
            }
            return false;
        }

        return true;
    }

    /**
     * Returns the VPC subnet IDs.
     *
     * @return  array
     */
    private function getCurrentTemplateVpcSubnetIds()
    {
        if (! empty($this->vpcSubnetIds)) {
            return $this->vpcSubnetIds;
        }

        $subnets = $this->ec2Client->describeSubnets($this->getCurrentTemplateVpcId());
        $this->vpcSubnetIds = empty($subnets) ? [] : array_column($subnets, "SubnetId");
        return $this->vpcSubnetIds;
    }
    
    /**
     * Returns the security group ID.
     *
     * @param   string  $groupName
     *
     * @return  string
     */
    private function getSecurityGroupId($groupName)
    {
        return $this->ec2Client->getSecurityGroupId($groupName, $this->getCurrentTemplateVpcId());
    }

    /**
     * Creates a new security group and returns ID.
     *
     * @param   array   $group
     *
     * @return  string
     */
    private function createSecurityGroup($group)
    {
        $name = $group['Name'];
        if (empty($name)) {
            $this->exception("Invalid security group name.");
        }

        return $this->ec2Client->createSecurityGroup($name, $this->getCurrentTemplateVpcId());
    }

    /**
     * Creates security group rules.
     *
     * @return  void
     */
    private function setSecurityGroupIngressRules($group)
    {
        foreach ($group['IngressRules'] ?? [] as $rule) {
            if (isset($rule['SecurityGroup'])) {
                $srcSgName = $rule['SecurityGroup'];
                $this->ec2Client->addSecurityGroupIngressSourceSecurityGroupRule(
                    $group['Name'],
                    $srcSgName,
                    $this->getSecurityGroupId($group['Name']),
                    $this->getSecurityGroupId($srcSgName),
                    $this->getCurrentTemplateVpcId()
                );
            } elseif (isset($rule['Port'])) {
                $this->ec2Client->addSecurityGroupIngressIpRule(
                    $group['Name'],
                    $rule['Port'],
                    $this->getSecurityGroupId($group['Name']),
                    $rule['AllowAllIps'] ?? false,
                    $rule['IpV4Range'] ?? [],
                    $rule['IpV6Range'] ?? []
                );
            } else {
                $this->exception("Bad security group rule.");
            }
        }
    }

    private function setFullAccessIpIngressRule($sgName, $port)
    {
        $this->ec2Client->addSecurityGroupIngressIpRule(
            $sgName,
            $port,
            $this->getSecurityGroupId($sgName),
            true /* allow all IPs */
        );
    }

    private function unsetFullAccessIpIngressRule($sgName, $port)
    {
        $this->ec2Client->removeSecurityGroupIngressIpRule(
            $sgName,
            $port,
            $this->getSecurityGroupId($sgName),
            true /* allow all IPs */
        );
    }

    private function mapSecurityGroupNamesToIds($securityGroups)
    {
        return array_map(function ($sgName) {
            return $this->getSecurityGroupId($sgName);
        }, $securityGroups);
    }

    private function waitUntilEc2InstanceIsReady($instanceId, $maxWaitInMins = 5)
    {
        $sleepTimeInSecs    = 30;
        $maxWaitInSecs      = $maxWaitInMins * 60;
        $sleepCount         = 0;

        $this->log("Waiting for EC2 instance '$instanceId' to become ready...");

        while (true) {
            try {
                if ($this->ec2Client->isInstanceRunning($instanceId)) {
                    return; // Instance is running
                }
            } catch (Exception $ex) {
                // Sometimes instance is not yet updated in AWS database
            }

            $this->log("Sleep #".($sleepCount+1)." for $sleepTimeInSecs seconds...");

            sleep($sleepTimeInSecs);

            $sleepCount++;

            if ($sleepCount * $sleepTimeInSecs > $maxWaitInSecs) {
                $this->exception("Max wait time reached ($maxWaitInSecs mins) and no result found.");
            }
        }
    }

    /*******************************************
     * CloudFront Methods
     *******************************************/

    /**
     * Allocates the specified CloudFront spec.
     *
     * @param   array     $cloudfront
     *
     * @return  void
     */
    private function buildCloudFrontResources($cloudfront)
    {
        $dists = $this->createCloudFrontDistributions($cloudfront['Distributions'] ?? []);

        return [
            'Distributions' => $dists
        ];
    }

    private function createCloudFrontDistributions($dists)
    {
        $responses = [];
        foreach ($dists as $config) {
            $responses[] = $this->createCloudFrontDistribution($config);
        }

        return $responses;
    }

    private function createCloudFrontDistribution($distConfig)
    {
        // Origins config
        $apiOriginConfig = [
            'Items' => $distConfig['Origins']['Items'] ?? [],
            'Quantity' => $distConfig['Origins']['Quantity'] ?? 0
        ];
        foreach ($distConfig['Origins']['S3Buckets'] ?? [] as $bucketConfig) {
            $apiOriginConfig['Items'][] = $this->cloudFrontClient->createDefaultS3OriginConfig(
                $bucketConfig['Id'],
                $bucketConfig['Name'],
                $bucketConfig['Path'] ?? ''
            );
        }
        foreach ($distConfig['Origins']['Custom'] ?? [] as $customConfig) {
            $item = $this->cloudFrontClient->createDefaultCustomOriginConfig(
                $customConfig['Id'],
                $customConfig['Name'],
                $customConfig['Path'] ?? '',
                $customConfig['Policy'] ?? 'match-viewer'
            );
            // Add AWS params customization
            if (!empty($customConfig['CustomConfig'])) {
                $item['CustomOriginConfig'] = array_merge($item['CustomOriginConfig'], $customConfig['CustomConfig']);
            }
            $apiOriginConfig['Items'][] = $item;
        }
        $apiOriginConfig['Quantity'] = count($apiOriginConfig['Items']);
        $distConfig['Origins'] = $apiOriginConfig;

        // OriginGroups config
        $apiOriginGroupsConfig = [
            'Items' => $distConfig['OriginGroups']['Items'] ?? [],
            'Quantity' => $distConfig['OriginGroups']['Quantity'] ?? 0
        ];
        foreach ($distConfig['OriginGroups'] ?? [] as $groupConfig) {
            $apiOriginGroupsConfig['Items'][] = $this->cloudFrontClient->createDefaultOriginGroupConfig(
                $groupConfig['Id'],
                $groupConfig['Members'],
                $groupConfig['FailoverCriteria'] ?? [],
            );
        }
        $apiOriginGroupsConfig['Quantity'] = count($apiOriginGroupsConfig['Items']);
        $distConfig['OriginGroups'] = $apiOriginGroupsConfig;

        // Rebuild cache behavior config
        $cacheBehavior = $distConfig['CacheBehaviors'] ?? [];
        unset($distConfig['CacheBehaviors']);

        // Rebuild cache behavior for API
        foreach ($cacheBehavior as $cacheConfig) {
            // Create cloudfront cache behavior config
            $cfDistConfig = $this->cloudFrontClient->createCacheBehaviorConfig($cacheConfig);
            // Check whether the cache behavior is default or custom
            if ($cacheConfig['Default'] ?? false) {
                // Set the default config
                $distConfig['DefaultCacheBehavior'] = $cfDistConfig;
            } else {
                // Initializes the custom cache behavior config
                if (empty($distConfig['CacheBehaviors']['Items'])) {
                    $distConfig['CacheBehaviors'] = [
                        'Items' => [],
                        'Quantity' => 0
                    ];
                }
                // Add to the list of custom cache behavior items
                $distConfig['CacheBehaviors']['Items'][] = $cfDistConfig;
                $distConfig['CacheBehaviors']['Quantity'] = count($distConfig['CacheBehaviors']['Items']);
            }
        }

        // Tags
        if (! empty($distConfig['Tags'])) {
            $distConfig['Tags'] = $this->cloudFrontClient->prepareTagsArray($distConfig['Tags']);
        }

        // Aliases
        if (! empty($distConfig['Aliases'])) {
            $distConfig['Aliases'] = [
                'Items' => $distConfig['Aliases'],
                'Quantity' => count($distConfig['Aliases'])
            ];
        }

        // ViewerCertificate
        if (! empty($distConfig['AcmCertificate'])) {
            $distConfig['ViewerCertificate'] = $this->cloudFrontClient->createDefaultViewerCertificate(
                $distConfig['AcmCertificate']['Arn']
            );
        }

        // Create distribution
        return $this->cloudFrontClient->createDistribution($distConfig);
    }

    /**
     * Destroys the specified CloudFront spec.
     *
     * @param   array     $cloudfront
     *
     * @return  void
     */
    private function destroyCloudFrontResources($cloudfront)
    {
        $dists = $cloudfront['Distributions'] ?? [];
        foreach ($dists as $dist) {
            if (! empty($dist['Id'])) {
                // It cannot delete immediately as the disabling process takes up to 15mins so
                // we just mark it in order for it to be deleted by other job
                $this->cloudFrontClient->disableDistribution($dist['Id'], [
                    'DisabledAt' => gmdate("Y-m-d\TH:i:s\Z"),
                    'Action' => 'delete'
                ]);
            }
        }
    }

    /*******************************************
     * Route53 Methods
     *******************************************/

    /**
     * Allocates the specified Route53 spec.
     *
     * @param   array     $route53
     *
     * @return  void
     */
    private function buildR53Resources($route53)
    {
        $hostedZones = $this->createR53HostedZones($route53['HostedZones'] ?? []);
        $recordSets = $this->handleR53RecordSets($route53['RecordSets'] ?? [], 'upsert');

        return [
            'HostedZones' => $hostedZones,
            'RecordSets' => $recordSets
        ];
    }

    /**
     * Allocates the specified Route53 spec.
     *
     * @param   array     $route53
     *
     * @return  void
     */
    private function destroyR53Resources($route53)
    {
        $this->destroyR53HostedZones($route53['HostedZones'] ?? []);
        $this->handleR53RecordSets($route53['RecordSets'] ?? [], 'delete');
    }

    /**
     * Handles Route53 hosted zone creation.
     *
     * @param   array   $recordSets
     * @param   string  $action
     * @return  array
     */
    private function createR53HostedZones($hostedZones)
    {
        $responses = [];
        foreach ($hostedZones ?? [] as $hz) {
            $responses[] = $this->route53Client->createHostedZone($hz['Name']);
        }

        return $responses;
    }

    /**
     * Handles Route53 hosted zone deletion.
     *
     * @param   array   $recordSets
     * @param   string  $action
     * @return  array
     */
    private function destroyR53HostedZones($hostedZones)
    {
        $responses = [];
        foreach ($hostedZones ?? [] as $hz) {
            // Get hosted zone ID
            $hzId = $hz['Id'] ?? $this->getR53HostedZoneId($hz['Name']);
            // Destroy hosted zone
            $responses[] = $this->route53Client->deleteHostedZone($hzId);
        }

        return $responses;
    }

    /**
     * Handles Route53 record sets creation/deletion.
     *
     * @param   array   $recordSets
     * @param   string  $action
     * @return  array
     */
    private function handleR53RecordSets($recordSets, $action)
    {
        if (! in_array($action, [ 'create', 'upsert', 'delete' ])) {
            $this->exception("Invalid Route53 action.");
        }

        $responses = [];

        foreach ($recordSets as $set) {
            $hzId = $set['HostedZoneId'] ?? $this->getR53HostedZoneId($set['HostedZoneName']);
            if (is_null($hzId)) {
                $this->exception("Invalid Route53's hosted zone ID.");
            }

            $method = $action.'ResourceRecordSet';
            if (!empty($set['Values'])) {
                foreach ($set['Values'] as $value) {
                    $records = explode(",", $value['Value']) ?? [];
                    $responses[] = $this->route53Client->$method(
                        $hzId,
                        $value['Type'],
                        $value['Name'],
                        $records,
                        null, // alias
                        $value['Comment'] ?? '',
                        $value['TTL'] ?? 300
                    );
                }
            } elseif (!empty($set['Alias'])) {
                $responses[] = $this->route53Client->$method(
                    $hzId,
                    'A',
                    $set['Alias']['Name'],
                    [], // records
                    $set['Alias']['Value'],
                    $set['Alias']['Comment'] ?? ''
                );
            } else {
                $this->exception('Invalid record / alias configuration.');
            }
        }

        return $responses;
    }

    /**
     * Returns the hosted zone ID of the default hosting domain.
     *
     * @param   $name
     *
     * @return  string
     */
    private function getR53HostedZoneId($name)
    {
        $hz = $this->route53Client->getHostedZoneByName($name);
        return $hz['Id'] ?? null;
    }

    /*******************************************
     * Elastic Beanstalk Methods
     *******************************************/

    private function buildEbResources($eb)
    {
        $apps = $this->createEbApps($eb['Applications'] ?? []);

        return [
            'Applications' => $apps
        ];
    }

    private function destroyEbResources($eb)
    {
        $this->destroyEbApps($eb['Applications'] ?? []);
    }

    private function createEbApps($apps)
    {
        $appResponses = [];
        
        foreach ($apps ?? [] as $app) {
            $appResponses[] = $this->createEbApp($app);
        }

        return $appResponses;
    }

    private function createEbApp($app)
    {
        $appName = $app['Name'] ?? null;
        if (empty($appName)) {
            $this->exception("Invalid EB app Name parameter.");
        }

        // Check if the EB app exists
        if ($this->ebClient->applicationExists($appName)) {
            $this->exception("'$appName' EB application already exists.");
        }

        // Create application
        $appResponse = $this->ebClient->createApplication($appName);

        // Create sample app version
        $sampleAppVersion = $this->ebClient->createSampleApplicationVersion($appName);
        
        // Create environments
        $envResponses = [];
        foreach ($app['Environments'] ?? [] as $env) {
            $env['VersionLabel'] = $env['VersionLabel'] ?? $sampleAppVersion['VersionLabel'];
            $envResponses[] = $this->createEbEnv($app, $env);
        }

        return [
            'Application'   => $appResponse,
            'Environments'  => $envResponses
        ];
    }

    private function createEbEnv($app, $env)
    {
        Arr::validateFields($env, ['Name', 'SolutionStackName', 'VersionLabel']);
        
        $options = $env['Options'];

        // Set VPC settings
        if (empty($options['aws:ec2:vpc'])) {
            $options['aws:ec2:vpc'] = [
                'VPCId' => $this->getCurrentTemplateVpcId(),
                'Subnets' => implode(",", $this->getCurrentTemplateVpcSubnetIds()),
            ];
        }

        // Map security groups to ID
        $securityGroups = $options['aws:autoscaling:launchconfiguration']['SecurityGroups'] ?? [];
        $securityGroupIds = $this->mapSecurityGroupNamesToIds($securityGroups);
        $options['aws:autoscaling:launchconfiguration']['SecurityGroups'] = implode(",", $securityGroupIds);
        
        // Check if the specified key pair exists and create one if not
        $keyPairName = $options['aws:autoscaling:launchconfiguration']['EC2KeyName'] ?? null;
        if (! empty($keyPairName)) {
            $this->createKeyPairIfNotExists($keyPairName);
        }

        /**
         * Filters the environment variables before the ElasticBeanstalk
         * environment is created.
         *
         * @param   array   $envVars
         * @param   array   $awsResults
         * @param   array   $parameters
         */
        $options['aws:elasticbeanstalk:application:environment'] = $this->events->filter(
            'modify_env_vars_before_elasticbeanstalk_env_created',
            $options['aws:elasticbeanstalk:application:environment'],
            $this->currentTemplate['AwsResults'],
            $this->currentTemplate['Params']
        );

        // Create environment
        $this->ebClient->createEnvironment(
            $app['Name'],
            $env['Name'],
            $env['VersionLabel'],
            $env['SolutionStackName'] ?? null,
            $env['PlatformArn'] ?? null,
            $options,
            $env['Tags'] ?? []
        );
    }

    private function destroyEbApps($apps)
    {
        foreach ($apps ?? [] as $app) {
            try {
                $this->ebClient->deleteApplication($app['Name'], true /* force */);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /*******************************************
     * S3 Methods
     *******************************************/

    private function buildS3Resources($s3)
    {
        $buckets = $this->createS3Buckets($s3['Buckets'] ?? []);

        return [
            'Buckets' => $buckets
        ];
    }

    private function destroyS3Resources($s3)
    {
        return $this->destroyS3Buckets($s3['Buckets'] ?? []);
    }

    private function createS3Buckets($buckets)
    {
        $responses = [];
        
        foreach ($buckets as $bucket) {
            $name = $bucket['Name'];
            $responses[] = $this->s3Client->createBucket($name, $bucket['Acl'] ?? 'private');

            if (isset($bucket['BucketAcl'])) {
                $this->s3Client->putBucketAcl(
                    $bucket['Name'],
                    $bucket['BucketAcl']
                );
            }

            if (isset($bucket['LoggingConfig'])) {
                $this->s3Client->putBucketLogging(
                    $bucket['Name'],
                    $bucket['LoggingConfig']['TargetBucket'],
                    $bucket['LoggingConfig']['TargetPrefix']
                );
            }
        }

        return $responses;
    }

    private function destroyS3Buckets($buckets)
    {
        foreach ($buckets as $bucket) {
            $name = $bucket['Name'];
            try {
                $this->s3Client->deleteBucket($name);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /*******************************************
     * IAM Methods
     *******************************************/

    private function buildIamResources($iam)
    {
        $groups     = $this->createIamGroups($iam['Groups'] ?? []);
        $users      = $this->createIamUsers($iam['Users'] ?? []);
        $roles      = $this->createIamRoles($iam['Roles'] ?? []);
        $profiles   = $this->createIamInstanceProfiles($iam['InstanceProfiles'] ?? []);

        return [
            'Groups'            => $groups,
            'Users'             => $users,
            'Roles'             => $roles,
            'InstanceProfiles'  => $profiles
        ];
    }

    private function destroyIamResources($iam)
    {
        $this->deleteIamUsers($iam['Users'] ?? []);
        $this->deleteIamGroups($iam['Groups'] ?? []);
        $this->deleteIamRoles($iam['Roles'] ?? []);
        $this->deleteIamInstanceProfiles($iam['InstanceProfiles'] ?? []);
    }

    private function createIamGroups($groups)
    {
        $responses = [];
        foreach ($groups ?? [] as $group) {
            $responses[$group['Name']] = $this->iamClient->createGroup($group['Name']);
            foreach ($group['Policies'] ?? [] as $policy) {
                $this->iamClient->putGroupPolicy(
                    $group['Name'],
                    $policy['Name'],
                    $policy['Content']
                );
            }
        }
        
        return $responses;
    }

    private function createIamUsers($users)
    {
        $responses = [];
        foreach ($users ?? [] as $user) {
            $responses[$user['Name']] = $this->iamClient->createUser(
                $user['Name'],
                null,
                true,
                $user['Path'] ?? '/'
            );
            foreach ($user['Policies'] ?? [] as $policy) {
                $this->iamClient->putUserPolicy(
                    $user['Name'],
                    $policy['Name'],
                    $policy['Content']
                );
            }
        }
        
        return $responses;
    }

    private function createIamRoles($roles)
    {
        $responses = [];
        foreach ($roles ?? [] as $role) {
            $responses[$role['Name']] = $this->iamClient->createRole(
                $role['Name'],
                $role['AssumeRolePolicyDocument'] ?? '',
                $role['Path'] ?? '/'
            );
            foreach ($role['Policies'] ?? [] as $policy) {
                $this->iamClient->putRolePolicy(
                    $role['Name'],
                    $policy['Name'],
                    $policy['Content']
                );
            }
        }
        
        return $responses;
    }

    private function createIamInstanceProfiles($profiles)
    {
        $responses = [];
        foreach ($profiles ?? [] as $profile) {
            // Create instance profile and add roles to it
            $responses[$profile['Name']] = $this->iamClient->createInstanceProfile(
                $profile['Name'],
                $profile['Path'] ?? '/',
                $profile['Role'] ?? null
            );
        }
        
        return $responses;
    }

    private function deleteIamUsers($users)
    {
        foreach ($users ?? [] as $user) {
            try {
                $this->iamClient->deleteUser($user['Name']);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    private function deleteIamGroups($groups)
    {
        foreach ($groups ?? [] as $group) {
            try {
                $this->iamClient->deleteGroup($group['Name']);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    private function deleteIamRoles($roles)
    {
        foreach ($roles ?? [] as $role) {
            try {
                $this->iamClient->deleteRole($role['Name']);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    private function deleteIamInstanceProfiles($profiles)
    {
        foreach ($profiles ?? [] as $profile) {
            try {
                $this->iamClient->deleteInstanceProfile($profile['Name']);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /*******************************************
     * ElasticCache Methods
     *******************************************/

    private function buildElastiCacheResources($ec)
    {
        $clusters = $this->createElastiCacheClusters($ec['Clusters'] ?? []);

        return [
            'Clusters' => $clusters
        ];
    }

    private function createElastiCacheClusters($clusters)
    {
        $responses = [];

        // Create all clusters
        foreach ($clusters ?? [] as $i => $clusterConfig) {
            // Create cluster
            $awsResult = $this->createElastiCacheCluster($clusterConfig);

            // Save result
            $clusterId                          = $clusterConfig['CacheClusterId'];
            $clusters[$i]['AwsResult']          = $awsResult;
            $responses['Clusters'][$clusterId]  = $awsResult;
        }

        // Wait until all clusters are ready
        foreach ($clusters as $i => $clusterConfig) {
            $clusterId = $clusterConfig['CacheClusterId'];
            $awsResult = $this->waitUntilElastiCacheClusterIsReady($clusterId);
            $responses[$clusterId] = $awsResult;
        }

        return $responses;
    }

    private function createElastiCacheCluster($cluster)
    {
        // Check if subnet group exists
        $group = $this->elastiCacheClient->describeCacheSubnetGroup($cluster['CacheSubnetGroupName']);

        // Create subnet group if doesn't exist
        if (empty($group)) {
            $this->createElastiCacheSubnetGroup(
                $cluster['CacheSubnetGroupName'],
                $cluster['CacheSubnetGroupDescription'],
                $this->getCurrentTemplateVpcSubnetIds()
            );
        }

        // Get instance settings
        $cacheClusterId     = $cluster['CacheClusterId'];
        $subnetGroupName    = $cluster['CacheSubnetGroupName'];
        $securityGroupIds   = $this->mapSecurityGroupNamesToIds($cluster['SecurityGroups']);
        $spec               = $cluster['Spec'];

        // Create instance
        return $this->elastiCacheClient->createCacheCluster(
            $cacheClusterId,
            $spec,
            $securityGroupIds,
            $subnetGroupName
        );
    }

    private function createElastiCacheSubnetGroup($name, $desc, $subnetIds)
    {
        return $this->elastiCacheClient->createCacheSubnetGroup(
            $name,
            $desc,
            $subnetIds
        );
    }

    private function waitUntilElastiCacheClusterIsReady($clusterId, $maxWaitInMins = 10)
    {
        return $this->waitUntilElastiCacheClusterIs($clusterId, 'available', $maxWaitInMins);
    }

    private function waitUntilElastiCacheClusterIs($clusterId, $status, $maxWaitInMins)
    {
        $sleepTimeInSecs    = 45;
        $maxWaitInSecs      = $maxWaitInMins * 60;
        $sleepCount         = 0;

        while (true) {
            $cluster = $this->elastiCacheClient->describeCacheCluster($clusterId);
            $currentStatus = $cluster['CacheClusterStatus'];
            if ($currentStatus === $status) {
                return $cluster;
            }

            $this->log("Sleep #".($sleepCount+1)." for $sleepTimeInSecs seconds...", (array)$cluster);

            sleep($sleepTimeInSecs);

            $sleepCount++;

            if ($sleepCount * $sleepTimeInSecs > $maxWaitInSecs) {
                $this->exception("Max wait time reached ($maxWaitInSecs secs) and no result found.");
            }
        }
    }

    private function destroyElastiCacheResources($ec)
    {
        $this->destroyElastiCacheClusters($ec['Clusters'] ?? []);
    }

    private function destroyElastiCacheClusters($clusters, $sync = true)
    {
        foreach ($clusters ?? [] as $cluster) {
            $this->destroyElastiCacheCluster($cluster);
        }

        if ($sync) {
            $sleepTimeInSecs = 90;
            $this->log("Sleeping for $sleepTimeInSecs seconds until clusters are destroyed...");
            sleep($sleepTimeInSecs); // To implement later.
        }
    }

    private function destroyElastiCacheCluster($cluster)
    {
        try {
            $this->elastiCacheClient->deleteCacheSubnetGroup($cluster['CacheSubnetGroupName']);
        } catch (Exception $e) {
            //
        }

        try {
            $this->elastiCacheClient->deleteCacheCluster($cluster['CacheClusterId']);
        } catch (Exception $e) {
            //
        }
    }

    /*******************************************
     * RDS Methods
     *******************************************/

    private function buildRdsResources($rds)
    {
        $dbSubnetGroup = $this->createDbSubnetGroup(
            $rds['DbSubnetGroupName'] ?? null,
            $rds['DbSubnetGroupDescription'] ?? null,
            $this->getCurrentTemplateVpcSubnetIds()
        );

        $instances = $this->createRdsInstances(
            $rds['Instances'] ?? [],
            $dbSubnetGroup['DBSubnetGroupName'],
            $rds['SkipWaitUntilAllReady'] ?? false
        );
        
        return [
            'Instances' => $instances,
            'DBSubnetGroup' => $dbSubnetGroup
        ];
    }

    private function destroyRdsResources($rds)
    {
        $this->destroyRdsInstances(
            $rds['Instances'] ?? [],
            $rds['DbSubnetGroupName'] ?? null
        );

        try {
            $this->rdsClient->deleteDbSubnetGroup($rds['DbSubnetGroupName'] ?? null);
        } catch (Exception $e) {
            //
        }
    }

    private function createRdsInstances($instances, $subnetGroupName, $skipWait = false)
    {
        $responses = [];

        // Create instances
        foreach ($instances as $i => $instance) {
            // Get instance settings
            $spec               = $instance['Spec'];
            $name               = $instance['Name'];
            $securityGroupIds   = $this->mapSecurityGroupNamesToIds($instance['SecurityGroups']);

            // Create instance
            $awsResult = $this->createRdsInstance(
                $name,
                $spec,
                $securityGroupIds,
                $subnetGroupName
            );

            $instances[$i]['AwsResult'] = $awsResult;
            $responses[$name]           = $awsResult;
        }

        if ($skipWait) {
            return $responses;
        }

        // Wait until all instances are ready
        foreach ($instances as $i => $instance) {
            $awsResult = $this->waitUntilRdsInstanceIsReady($instance['Name']);
            $instances[$i]['AwsResult']     = $awsResult;
            $responses[$instance['Name']]   = $awsResult;
        }

        // Create databases and users
        foreach ($instances as $i => $instance) {
            // Get users and databases
            $databases  = $instance['Databases'] ?? [];
            $users      = $instance['Users'] ?? [];
            if (empty($users) && empty($databases)) {
                continue;
            }

            // Allow connection to database
            $this->setFullAccessIpIngressRule($instance['SecurityGroups'][0] ?? null, 3306);

            // Create DB client
            $dbClient = new PdoClient(
                [
                    'Host' => $instance['AwsResult']['Endpoint']['Address'],
                    'Port' => $instance['AwsResult']['Endpoint']['Port'],
                    'Username' => $instance['Spec']['MasterUsername'],
                    'Password' => $instance['Spec']['MasterUserPassword']
                ]
            );

            foreach ($databases as $db) {
                $dbClient->createDatabase($db['Name']);
            }

            // Create users
            foreach ($users as $user) {
                // Set permission by access level
                $user['Permissions'] =  isset($user['AccessLevel'])
                                        ? $this->getMysqlPermissionsByAccessLevel($user['AccessLevel'])
                                        : $user['Permissions'];
                // Set privileges to the database if provided
                if (isset($user['Database'])) {
                    $user['PrivilegesOn'] = $user['PrivilegesOn'] ?? "{$user['Database']}.*";
                }

                // Create user
                $dbClient->createUser(
                    $user['Username'],
                    $user['Password'],
                    $user['Permissions'] ?? "ALL PRIVILEGES",
                    $user['PrivilegesOn'] ?? "*.*",
                    $user['ValidHost'] ?? "%",
                    $user['WithGrantOption'] ?? false
                );
            }

            // Cloes database connection
            $dbClient->close();

            $this->unsetFullAccessIpIngressRule($instance['SecurityGroups'][0] ?? null, 3306);
        }

        return $responses;
    }

    private function createRdsInstance(
        $name,
        $spec,
        $securityGroupIds,
        $dbSubnetGroupName,
        $dbName = null
    ) {
        return $this->rdsClient->createDbInstance(
            $name,
            $spec,
            $dbName,
            $securityGroupIds,
            $dbSubnetGroupName
        );
    }

    private function destroyRdsInstances($instances, $subnetGroupName, $sync = true)
    {
        foreach ($instances ?? [] as $instance) {
            try {
                $this->rdsClient->deleteDbInstance($instance['Name']);
            } catch (Exception $e) {
                continue;
            }
        }

        if ($sync) {
            $this->waitUntilRdsInstancesAreDeleted($instances, 5);
        }
    }
    
    private function createDbSubnetGroup($name, $desc, $vpcSubnetIds = [])
    {
        return $this->rdsClient->createDbSubnetGroup(
            $name,
            $desc,
            $vpcSubnetIds
        );
    }

    private function waitUntilRdsInstancesAreDeleted($instances, $maxWaitInMins = 3)
    {
        $names = array_column($instances, 'Name');

        $sleepTimeInSecs    = 90;
        $maxWaitInSecs      = $maxWaitInMins * 60;
        $sleepCount         = 0;

        $this->log("Sleep for $maxWaitInMins minutes or until the RDS instances are deleted...", $names);

        while (true) {
            // Get all instances
            $dbInstances    = $this->rdsClient->describeDbInstances();
            $shouldCont     = false;
            // Check if any of the instances being deleted already exists
            foreach ($dbInstances ?? [] as $dbInstance) {
                if (in_array($dbInstance['DBInstanceIdentifier'], $names)) {
                    $shouldCont = true; // Should continue
                }
            }

            if (!$shouldCont) {
                $this->log("All instances were deleted.");
                return;
            }

            $this->log("Sleep #".($sleepCount+1)." for $sleepTimeInSecs seconds...");
            sleep($sleepTimeInSecs);
            $sleepCount++;
            if ($sleepCount * $sleepTimeInSecs > $maxWaitInSecs) {
                $this->exception("Max wait time reached ($maxWaitInSecs secs) and no result found.");
            }
        }
    }

    private function waitUntilRdsInstanceIsReady($dbInstanceId, $maxWaitInMins = 20)
    {
        $this->log("Sleep for $maxWaitInMins minutes or until the RDS instance is ready...");
        return $this->waitUntilRdsInstanceIs($dbInstanceId, $maxWaitInMins, 'available');
    }

    private function waitUntilRdsInstanceIs($dbInstanceId, $maxWaitInMins, $status)
    {
        $sleepTimeInSecs    = 90;
        $maxWaitInSecs      = $maxWaitInMins * 60;
        $sleepCount         = 0;

        while (true) {
            $result = $this->rdsClient->describeDbInstance($dbInstanceId);

            $instanceStatus = $result['DBInstances'][0]['DBInstanceStatus'] ?? null;
            if ($instanceStatus === $status) {
                return $result['DBInstances'][0];
            }

            $this->log("Sleep #".($sleepCount+1)." for $sleepTimeInSecs seconds...", (array)$result);

            sleep($sleepTimeInSecs);

            $sleepCount++;

            if ($sleepCount * $sleepTimeInSecs > $maxWaitInSecs) {
                $this->exception("Max wait time reached ($maxWaitInSecs secs) and no result found.");
            }
        }
    }

    /*******************************************
     * ACM Methods
     *******************************************/

    private function buildAcmResources($acm)
    {
        // Request certificates
        $requested = [];
        foreach ($acm['Certificates'] ?? [] as $certRequest) {
            // Request certificate
            $certArn = $this->acmClient->requestCertificate(
                $certRequest['DomainName'],
                $certRequest['ValidationMethod'],
                $certRequest['SubjectAlternativeNames'] ?? [],
                $certRequest['IgnoreIfExists'] ?? false
            );

            // Add certificate tags
            $tags = $certRequest['Tags'] ?? [];
            if (! empty($tags)) {
                $this->acmClient->addTagsToCertificate($certArn, $tags);
            }

            $requested[] = [
                'CertificateArn' => $certArn,
                'DomainName' => $certRequest['DomainName'],
                'SubjectAlternativeNames' => $certRequest['SubjectAlternativeNames'],
                'ValidateViaRoute53' => $certRequest['ValidateViaRoute53'] ?? false,
                'ValidationHzId' => $certRequest['ValidationRoute53HostedZoneId'] ?? null,
                'Tags' => $tags
            ];
        }

        // Try validate certificates with Route53 registered domain
        $validatedRecords = [];
        foreach ($requested ?? [] as $request) {
            if (! empty($request['ValidateViaRoute53'])) {
                $validatedRecords[] = $this->tryValidateAcmCertificateViaRoute53($request);
            }
        }

        return [
            'Certificates' => Arr::pluck($requested, 'CertificateArn'),
            'Route53ValidatedRecords' => array_filter($validatedRecords)
        ];
    }

    private function tryValidateAcmCertificateViaRoute53($info)
    {
        $this->log("Trying to validate ACM certificate via Route53...", $info);

        // Get the registrable domain to search for in Route53
        $validationDomainHost = Url::getRegistrableDomain($info['DomainName']);

        // Search and add validation CNAME record to Route53
        $hzId = $info['ValidationHzId'] ?? $this->getR53HostedZoneId($validationDomainHost);
        if (empty($hzId)) {
            $this->log(sprintf(
                "The certificate domain '%s' is not registered in Route 53.",
                $validationDomainHost
            ));
            return null;
        }

        // Get certificate ARN
        $arn = $info['CertificateArn'];

        // Get certificate details
        $cert = $this->acmClient->describeCertificateWithValidationOptiions($arn);

        // If the certificate is already issued then do nothing
        if ($cert['Status'] === 'ISSUED') {
            $this->log("The certificate was already issued.");
            return null;
        }

        // Make sure the ceritficate is pending validation
        if ($cert['Status'] !== 'PENDING_VALIDATION') {
            $this->exception("The certificate is not pending validation (status={$cert['Status']}).");
        }

        // Get validation options
        $validationOptions = $cert['DomainValidationOptions'] ?? null;
        if (empty($validationOptions)) {
            $this->exception("Invalid validation options");
        }

        // Try to validate certificate by iterating over validation options
        $records = [];
        foreach ($validationOptions ?? [] as $option) {
            $records[] = $this->handleAcmCertificateDomainValidationOption($option, $hzId);
        }

        return $records;
    }

    private function handleAcmCertificateDomainValidationOption($option, $hzId)
    {
        $this->log("Handling ACM certificate domain validation option...", $option);

        // Get validation method
        $validationMethod = $option['ValidationMethod'] ?? null;
        if ($validationMethod !== 'DNS') {
            $this->log("The validation method is not DNS.");
            return;
        }

        // Get validation domain
        $validationDomainName = $option['DomainName'] ?? null;
        if (empty($validationDomainName)) {
            $this->exception("Invalid validation domain name.");
        }

        // Get resource record
        $resourceRecord = $option['ResourceRecord'];

        // Add record to complete validation
        try {
            return $this->route53Client->createResourceRecordSet(
                $hzId,
                $resourceRecord['Type'],
                $resourceRecord['Name'],
                [ $resourceRecord['Value'] ],
                null, // alias
                'ACM certificate validation',
                300
            );
        } catch (Exception $e) {
            $this->logWarning("ACM certificate domain validation error. Message: ".$e->getMessage());
        }
    }

    private function destroyAcmResources($rds)
    {
        //
    }

    /*******************************************
     * LAMBDA Methods
     *******************************************/

    private function buildLambdaResources($lambda)
    {
        $functions = $this->createLambdaFunctions($lambda['Functions'] ?? []);
        $statementsIds = $this->addLambdaPermissions($lambda['Permissions'] ?? []);
        return [
            'Functions' => $functions,
            'Permissions' => $statementsIds
        ];
    }

    private function destroyLambdaResources($lambda)
    {
        $this->deleteLambdaFunctions($lambda['Functions'] ?? []);
        $this->removeLambdaPermissions($lambda['Permissions'] ?? []);
    }

    private function createLambdaFunctions($functions)
    {
        $result = [];
        foreach ($functions ?? [] as $funcParams) {
            if ($funcParams['SendAsAwsParams']) {
                $result[] = $this->lambdaClient->createFunctionFromAwsParams($funcParams);
            } else {
                $this->exception('Not supported.');
            }
        }

        return $result;
    }

    private function addLambdaPermissions($permissions)
    {
        $stmtIds = [];
        foreach ($permissions ?? [] as $config) {
            if ($config['SendAsAwsParams']) {
                $stmtIds[] = $this->lambdaClient->addPermissionFromAwsParams($config);
            } else {
                $this->exception('Not supported.');
            }
        }

        return $stmtIds;
    }

    private function deleteLambdaFunctions($functions)
    {
        foreach ($functions ?? [] as $funcParams) {
            try {
                $this->lambdaClient->deleteFunction($funcParams['FunctionName']);
            } catch (Exception $e) {
                $this->logWarning('Failed to delete Lambda function. Error: '.$e->getMessage());
                continue;
            }
        }
    }

    private function removeLambdaPermissions($permissions)
    {
        foreach ($permissions ?? [] as $config) {
            try {
                $this->lambdaClient->removePermission(
                    $config['FunctionName'],
                    $config['StatementId']
                );
            } catch (Exception $e) {
                $this->logWarning('Failed to delete Lambda function. Error: '.$e->getMessage());
                continue;
            }
        }
    }

    /*******************************************
     * Helper Methods
     *******************************************/

    /**
     * Builds the specified template resources.
     *
     * @param   array   $template
     *
     * @return  void
     */
    private function buildTemplate()
    {
        $tmpltParams = $this->currentTemplate['Params'];
        $tmpltName   = $this->currentTemplate['Name'];

        $this->log("Start building '$tmpltName' template resources...", $tmpltParams);
        foreach ($tmpltParams['Resources'] ?? [] as $service => $params) {
            $this->log("Start building AWS $service service resources...");
            $this->lastResourceHandled = $service;
            $this->handleServiceResources($service, $params, true /* build */);
        }
    }

    /**
     * Cleans all the current template cached dependencies.
     *
     * @return void
     */
    private function cleanCurrentTemplate()
    {
        $this->currentTemplate = null;
        $this->vpcId = null;
        $this->vpcSubnetIds = null;
    }

    /**
     * Destroys all the specified template resources or the current template's
     * if no template is specified.
     *
     * @param array|null    $templateName
     * @param boolean       $stopAtLastHandledResource
     * @return void
     */
    private function destroyTemplate($template = null, $stopAtLastHandledResource = false)
    {
        $this->warnIfProduction();

        $template = $template ?? $this->currentTemplate;

        $tmpltParams = $template['Params'];
        $tmpltName   = $template['Name'];

        $this->log("Start destroying '$tmpltName' template resources...", $tmpltParams);
        foreach ($tmpltParams['Resources'] ?? [] as $service => $params) {
            $this->log("Start destroying AWS $service service resources...");
            $this->handleServiceResources($service, $params, false /* destroy */);
            if ($stopAtLastHandledResource &&
                $tmpltName === $this->lastTemplateHandled &&
                $service === $this->lastResourceHandled) {
                $this->log("Stopped destroying '$tmpltName' template after handling '$service' resource.");
                break;
            }
        }
    }

    private function handleServiceResources($service, $params, $action = true)
    {
        /**
         * Filters the resource parameters before the service resources
         * are created/destroyed.
         *
         * @param   array   $resources
         * @param   array   $awsResults
         */
        $filterName = sprintf(
            "modify_resource_params_before_%s_resources_%s",
            strtolower($service),
            $action ? "created" : "destroyed"
        );

        $params = $this->events->filter(
            $filterName,
            $params,
            $this->currentTemplate['AwsResults']
        );

        // Remove order suffix if the service is "EC2_1"
        $serviceType = explode("_", $service)[0];

        switch ($serviceType) {
            case 'EC2':
                $awsResult = $action
                        ? $this->buildEc2Resources($params)
                        : $this->destroyEc2Resources($params);
                break;
            case 'RDS':
                $awsResult = $action
                        ? $this->buildRdsResources($params)
                        : $this->destroyRdsResources($params);
                break;
            case 'IAM':
                $awsResult = $action
                        ? $this->buildIamResources($params)
                        : $this->destroyIamResources($params);
                break;
            case 'S3':
                $awsResult = $action
                        ? $this->buildS3Resources($params)
                        : $this->destroyS3Resources($params);
                break;
            case 'ElasticBeanstalk':
                $awsResult = $action
                        ? $this->buildEbResources($params)
                        : $this->destroyEbResources($params);
                break;
            case 'R53':
                $awsResult = $action
                        ? $this->buildR53Resources($params)
                        : $this->destroyR53Resources($params);
                break;
            case 'CloudFront':
                $awsResult = $action
                        ? $this->buildCloudFrontResources($params)
                        : $this->destroyCloudFrontResources($params);
                break;
            case 'ACM':
                $awsResult = $action
                        ? $this->buildAcmResources($params)
                        : $this->destroyAcmResources($params);
                break;
            case 'Lambda':
                $awsResult = $action
                        ? $this->buildLambdaResources($params)
                        : $this->destroyLambdaResources($params);
                break;
            case 'ElastiCache':
                $awsResult = $action
                        ? $this->buildElastiCacheResources($params)
                        : $this->destroyElastiCacheResources($params);
                break;
            default:
                $this->exception("Unsupported service '$service'.");
                break;
        }

        /**
         * This action is called after resources are created/deleted.
         *
         * @param   array   $awsResult
         *
         * @return  void
         */
        $actionName = sprintf(
            "after_%s_resources_%s",
            strtolower($service),
            $action ? "created" : "destroyed"
        );
        $this->events->action($actionName, $awsResult);

        // Save the resource results
        $this->currentTemplate['AwsResults'][$service] = $awsResult;
    }

    private function getAwsTagValue($tags, $key)
    {
        if (! is_array($tags)) {
            $this->exception("Expected tags to be array.");
        }

        foreach ($tags as $pair) {
            if (! isset($pair['Key']) || ! isset($pair['Value'])) {
                $this->exception("Invalid tag key/value pair.");
            }

            if ($pair['Key'] === $key) {
                return $pair['Value'];
            }
        }
    }

    private function warnIfProduction()
    {
        if ($this->ignoreProductionWarning || !$this->isTestingOrLocal()) {
            return;
        }

        // Check if production resource being called from dev/test app environment
        $tmpltSuffix = $this->currentTemplate['Params']['Variables']['<suffix>'] ?? null;
        $maybeProduction = $tmpltSuffix === 'prod' || empty($tmpltSuffix);
        if (! $maybeProduction) {
            return;
        }

        $this->exception("Oops- you might be destroying a production environment unintentionally! You must deliberately call ignoreProductionWarning() function to skip this warning.");
    }

    private function resolveTemplateParamsVars($data)
    {
        while (! $this->checkIfTemplateParamVarsAreResolved($data['Variables'])) {
            $data['Variables'] = Arr::replaceDataRec($data['Variables'], $data['Variables']);
        }

        return Arr::replaceDataRec($data, $data['Variables']);
    }

    private function checkIfTemplateParamVarsAreResolved($vars)
    {
        foreach ($vars as $key => $value) {
            if (preg_match("/.*<.+>.*/", $value)) {
                return false;
            }
        }

        return true;
    }

    private function validateTemplateParams($params)
    {
        if (empty($params)) {
            $this->exception("Invalid or empty template parameters.");
        }

        if (! isset($params['Resources'])) {
            $this->exception("Invalid template resources parameter.");
        }
    }

    private function validateBuildParams()
    {
        foreach ($this->templates ?? [] as $template) {
            $params = $template['Params'];
            $this->validateRdsBuildParams($params['Resources']['RDS'] ?? []);
        }
    }

    private function validateRdsBuildParams($rds)
    {
        if (empty($rds)) {
            return;
        }

        foreach ($rds['Instances'] ?? [] as $instance) {
            if (empty($instance['Spec']['MasterUsername'])) {
                $this->exception("Missing master username for instance {$instance['Name']}.");
            }
            if (empty($instance['Spec']['MasterUserPassword'])) {
                $this->exception("Missing master password for instance {$instance['Name']}.");
            }
            foreach ($instance['Users'] ?? [] as $user) {
                if (empty($user['Username'])) {
                    $this->exception("Missing username for {$instance['Name']}'s DB user.");
                }
                if (empty($user['Password'])) {
                    $this->exception("Missing password for {$instance['Name']}'s DB user.");
                }
            }
        }
    }

    private function validateAwsBuilderConfig()
    {
        if (empty(config('aws-builder.aws.region'))) {
            $this->exception("Invalid AWS region.");
        }

        if (empty(config('aws-builder.aws.access_key_id'))) {
            $this->exception("Invalid AWS access key ID.");
        }

        if (empty(config('aws-builder.aws.secret_access_key'))) {
            $this->exception("Invalid AWS secret key.");
        }
    }

    private function initAwsClients($template)
    {
        $this->ebClient             = new ElasticBeanstalkClient($template, $this->awsRegion);
        $this->rdsClient            = new RdsClient($template, $this->awsRegion);
        $this->s3Client             = new S3Client($template, $this->awsRegion);
        $this->iamClient            = new IamClient($template, $this->awsRegion);
        $this->ec2Client            = new Ec2Client($template, $this->awsRegion);
        $this->route53Client        = new Route53Client($template, $this->awsRegion);
        $this->cloudFrontClient     = new CloudFrontClient($template, $this->awsRegion);
        $this->lambdaClient         = new LambdaClient($template, $this->awsRegion);
        $this->elastiCacheClient   = new ElastiCacheClient($template, $this->awsRegion);

        $this->log("Initializing ACM Client in '{$this->awsAcmRegion}' region...");
        $this->acmClient        = new AcmClient($this->awsAcmRegion);
    }

    /**
     * Returns permissions by access level.
     *
     * @param   string    $level
     *
     * @return  string
     */
    private function getMysqlPermissionsByAccessLevel($level)
    {
        switch ($level) {
            case 'Migrate':
                return "INSERT,SELECT,DROP,CREATE,ALTER,INDEX,REFERENCES,DELETE,UPDATE";
                break;

            case 'DbManager':
                return "RELOAD,CREATE USER,CREATE,DROP,ALTER,DELETE,INDEX,INSERT,SELECT,UPDATE";
                break;

            case 'ReadOnly':
                return "SELECT";
                break;
            
            case 'Default':
            default:
                return "DELETE,INSERT,SELECT,UPDATE";
                break;
        }
    }
}
