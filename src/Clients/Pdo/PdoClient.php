<?php

namespace NizarBlond\AwsBuilder\Clients\Pdo;

use NizarBlond\AwsBuilder\Models\ActivityLog;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use PDO;

class PdoClient
{
    use InstanceHelpers;

    /**
     * The service name.
     *
     * @var string
     */
    private $service = "PdoClient";

    /**
     * The connection.
     *
     * @var PDO
     */
    private $conn;

    /**
     * Whether to log success.
     *
     * @var boolean
     */
    public $logSuccess;

    /**
     * Whether to log failure.
     *
     * @var boolean
     */
    public $logFailure;

    /**
     * The constructor.
     *
     * @param   array   $args
     *
     * @return  void
     */
    public function __construct($args)
    {
        $host   = $args['Host'];
        $port   = $args['Port'] ?? 3306;
        $user   = $args['Username'];
        $pass   = $args['Password'];
        $dbName = $args['Database'] ?? null;

        $dsn = "mysql:host=$host;port=$port";
        if (! empty($dbName)) {
            $dsn .= ";dbname=$dbName";
        }

        $this->conn = new PDO($dsn, $user, $pass);
        $this->logSuccess = config('aws-builder.clients.pdo.log_success');
        $this->logFailure = config('aws-builder.clients.pdo.log_failure');

        $this->exceptionModeOff();
    }

    /**
     * Returns the PDO connection.
     *
     * @return  PDO
     */
    public function getConnection()
    {
        return $this->conn;
    }

    /**
     * Turns on the exception mode.
     *
     * @return void
     */
    public function exceptionModeOn()
    {
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Turns on the exception mode.
     *
     * @return void
     */
    public function exceptionModeOff()
    {
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    }

    /**
     * Creates a new database user.
     *
     * @param   string    $username
     * @param   string    $password
     * @param   string    $privileges
     * @param   string    $privilegesOn
     * @param   string    $validHost
     * @param   string    $withGrantOption
     *
     * @return  void
     */
    public function createUser(
        $username,
        $password,
        $privileges = "ALL PRIVILEGES",
        $privilegesOn = "*.*",
        $validHost = "%",
        $withGrantOption = false
    ) {
        // Create new user
        $this->log("Creating new user '$username'@'$validHost'...");
        $cmd = sprintf(
            "CREATE USER %s IDENTIFIED BY %s",
            "'$username'@'$validHost'",
            "'$password'"
        );
        $this->exec($cmd);

        // Grant privileges
        $this->log("Granting '$username'@'$validHost' '$privileges' on '$privilegesOn'...");
        $cmd = sprintf(
            "GRANT %s ON %s TO %s %s",
            $privileges,
            $this->getPriviligesOnQueryString($privilegesOn),
            "'$username'@'$validHost'",
            $withGrantOption ? "WITH GRANT OPTION" : ""
        );
        $this->exec($cmd);

        // Flush privileges
        $this->log("Flushing privileges...");
        $cmd = "FLUSH PRIVILEGES";
        $this->exec($cmd);
    }

    /**
     * Creates a new database.
     *
     * @param   string    $dbName
     *
     * @return  void
     */
    public function createDatabase($dbName)
    {
        $this->log("Creating new database '$dbName'...");
        $cmd = "CREATE DATABASE `$dbName`;";
        $this->exec($cmd);

        $this->log("Flushing privileges...");
        $this->exec("FLUSH PRIVILEGES");
    }

    /**
     * Drops a database if exists.
     *
     * @param   string    $dbHost
     * @param   string    $dbUser
     * @param   string    $dbName
     * @param   string    $dbPass
     * @param   string    $dbPort
     *
     * @return  void
     */
    public function dropDatabase($dbName)
    {
        $this->exec("DROP DATABASE IF EXISTS $dbName");
    }

    /**
     * Executes an SQL command.
     *
     * @param   string    $cmd
     *
     * @return  integer     The number of rows affected.
     */
    public function exec($cmd)
    {
        $callerFunc = debug_backtrace()[1]['function'];

        $this->log($cmd);
        $result = $this->conn->exec($cmd);
        $this->killOnError($result, $callerFunc, $cmd);
        $this->recordActivity($callerFunc, $cmd);

        return $result;
    }

    /**
     * Queries the database.
     *
     * @param   string    $cmd
     *
     * @return  array
     *
     * @throws  \Exception
     */
    public function query($cmd)
    {
        $callerFunc = debug_backtrace()[1]['function'];
        
        $this->log($cmd);
        $result = $this->conn->query($cmd);
        $this->killOnError($result, $callerFunc, $cmd);
        $this->recordActivity($callerFunc, $cmd);

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get last SQL error as a pretty string.
     *
     * @return  string
     */
    public function lastErrorInfo()
    {
        $errorInfo = $this->conn->errorInfo();
        return sprintf(
            "%s (SQLSTATE=%s, CODE=%s).",
            $errorInfo[2],
            $errorInfo[0],
            $errorInfo[1]
        );
    }

    /**
     * Closes the database connection.
     *
     * @return  void
     */
    public function close()
    {
        $this->log("Closing database connection...");
        $this->conn = null;
    }

    /**
     * Records activity.
     *
     * @param   string      $function
     * @param   string      $cmd
     *
     * @return  void
     */
    public function recordActivity($function, $cmd)
    {
        if ($this->logSuccess) {
            ActivityLog::record($this->service, $function, [ 'CMD' => $cmd ]);
        }
    }
    
    /**
     * Records a failed activity to the database.
     *
     * @param   string      $function
     * @param   string      $cmd
     * @param   Exception   $e
     *
     * @return  void
     */
    public function recordFailedActivity($function, $cmd, $e)
    {
        if ($this->logFailure) {
            ActivityLog::recordFailure($this->service, $function, [ 'CMD' => $cmd ], $e);
        }
    }

    /**
     * Fixes priviliges on query string.
     *
     * @param   string      $privilegesOn
     *
     * @return  string
     */
    public function getPriviligesOnQueryString($privilegesOn)
    {
        $parts = explode(".", $privilegesOn);
        foreach ($parts ?? [] as $part) {
            if ($part !== "*") {
                $privilegesOn = str_replace($part, "`$part`", $privilegesOn);
            }
        }

        return $privilegesOn;
    }

    /**
     * Saves error and throws exception.
     *
     * @param   PDOResult   $result
     * @param   string      $callerFunc
     *
     * @return  void
     */
    private function killOnError($result, $callerFunc, $cmd)
    {
        if (false === $result) {
            $error = $this->lastErrorInfo();
            $this->logError($error);
            $this->recordFailedActivity($callerFunc, $cmd, $error);
            $this->exception($this->lastErrorInfo());
        }
    }
}
