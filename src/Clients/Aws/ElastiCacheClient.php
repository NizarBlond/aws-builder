<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;

class ElastiCacheClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-elasticache-2015-02-02.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2015-02-02';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'ElasticCache';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\ElastiCache\ElastiCacheClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    public function createCacheCluster(
        string $clusterId,
        array $config,
        array $securityGroupIds,
        string $subnetGroupName
    ) {
        $params = array_merge($config, [
            "CacheClusterId"        => $clusterId,
            "SecurityGroupIds"      => $securityGroupIds,
            "CacheSubnetGroupName"  => $subnetGroupName
        ]);

        return $this->sendRequest('createCacheCluster', $params)->get('CacheCluster');
    }

    public function deleteCacheCluster(string $clusterId)
    {
        $params = [
            'CacheClusterId' => $clusterId
        ];

        return $this->sendRequest('DeleteCacheCluster', $params);
    }

    public function describeCacheCluster($clusterId, $showNodeInfo = true)
    {
        $params = [
            'CacheClusterId' => $clusterId,
            'ShowCacheNodeInfo' => $showNodeInfo
        ];

        $clusters = $this->sendRequest('DescribeCacheClusters', $params)->get('CacheClusters');
        if (empty($clusters)) {
            $this->exception("Cluster was not found.");
        }

        return $clusters[0];
    }

    public function describeCacheClusters()
    {
        return $this->sendRequest('DescribeCacheClusters', [])->get('CacheClusters');
    }

    public function createCacheSubnetGroup($groupName, $groupDesc, array $subnetIds)
    {
        $params = [
            'CacheSubnetGroupDescription' => $groupDesc,
            'CacheSubnetGroupName' => $groupName,
            'SubnetIds' => $subnetIds,
        ];

        return $this->sendRequest('createCacheSubnetGroup', $params)->get('CacheSubnetGroup');
    }

    public function describeCacheSubnetGroup($groupName)
    {
        $params = [
            'CacheSubnetGroupName' => $groupName,
        ];

        try {
            $groups = $this->sendRequest('describeCacheSubnetGroups', $params)->get('CacheSubnetGroups');
            return $groups[0] ?? null;
        } catch (AwsException $e) {
            return null;
        }
    }

    public function deleteCacheSubnetGroup($groupName)
    {
        $params = [
            'CacheSubnetGroupName' => $groupName,
        ];

        try {
            return $this->sendRequest('deleteCacheSubnetGroup', $params);
        } catch (AwsException $e) {
            return;
        }
    }
}
