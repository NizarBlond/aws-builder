<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Exception;

class Ec2Client extends AwsClientBase
{
    /**
     * The AWS SDK version.
     *
     * Reference:
     * - http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-elasticbeanstalk-2010-12-01.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2016-11-15';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'Ec2';

    /**
     * The security group name to object mapper.
     *
     * @var array
     */
    private $_sgCache = [];

    /**
     * The security group name to object mapper.
     *
     * @var array
     */
    private $_sgNameToIdCache = [];

    /**
     * The VPC name/id to VPC object mapper.
     *
     * @var array
     */
    private $_vpcCache = [];

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct(string $templateName = null, string $awsRegion = null)
    {
        parent::__construct(
            '\Aws\Ec2\Ec2Client',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Launches the a single instance.
     *
     * @param   string  $name
     * @param   string  $amiId
     * @param   string  $subnetId
     * @param   string  $instanceType
     * @param   array   $securityGroupIds
     * @param   array   $tags
     * @param   string  $userData
     * @param   string  $iamInstanceProfileArn
     * @param   array   $blockDeviceMappings
     * @param   boolean $monitorEnabled
     *
     * @return  array
     */
    public function runSingleInstance(
        $name,
        $amiId,
        $subnetId,
        $instanceType,
        $securityGroupIds = [],
        $keyName = null,
        $tags = [],
        $userData = null,
        $iamInstanceProfileArn = null,
        $blockDeviceMappings = null,
        $monitorEnabled = false
    ) {
        $tags['Name']   = $tags['Name'] ?? $name;
        $tagsAwsFormat  = $this->prepareTagsArray($tags);

        $params = [
            'ImageId' => $amiId,
            'InstanceType' => $instanceType,
            'KeyName' => $keyName,
            'MaxCount' => 1,
            'MinCount' => 1,
            'Monitoring' => [
                'Enabled' => $monitorEnabled,
            ],
            'SecurityGroupIds' => $securityGroupIds,
            'SubnetId' => $subnetId,
            'TagSpecifications' => [
                [
                    'ResourceType' => 'instance',
                    'Tags' => $tagsAwsFormat,
                ],
            ],
            'IamInstanceProfile' => [
                'Arn' => $iamInstanceProfileArn
            ]
        ];

        if (! empty($blockDeviceMappings)) {
            $params['BlockDeviceMappings'] = $blockDeviceMappings;
        }

        if (! empty($userData)) {
            $params['UserData'] = base64_encode($userData);
        }

        return $this->sendRequest('runInstances', $params)->get('Instances')[0];
    }

    /**
     * Allocates a new EIP.
     *
     * @param   string  $domain
     * @param   boolean $getUnusedFirst
     *
     * @return  string  The public IP.
     */
    public function allocateAddress($domain = 'vpc', $getUnusedFirst = true)
    {
        $params = [
            'Domain' => $domain
        ];

        // Check if there are allocated IPs that are not associated with any instance
        if ($getUnusedFirst) {
            $addresses = $this->describeAddresses();
            foreach ($addresses as $address) {
                if (empty($address['InstanceId'])) {
                    $this->log("Found an EIP ({$address['PublicIp']}) that is not associated with any instance.");
                    return $address['PublicIp'];
                }
            }
        }

        // Otherwise; allocate a new one.
        return $this->sendRequest('allocateAddress', $params)->get('PublicIp');
    }

    /**
     * Allocates a new EIP.
     *
     * @param   string  $domain
     * @param   string  $instanceId
     *
     * @return  string
     */
    public function describeAddresses($instanceId = null, $domain = 'vpc')
    {
        $params = [
            'Domain' => $domain
        ];

        if (! empty($instanceId)) {
            $params['Filters'] = [
                [
                    'Name' => 'instance-id',
                    'Values' => [ $instanceId ],
                ]
            ];
        }

        $result = $this->sendRequest('describeAddresses', $params)->get('Addresses');

        return !empty($instanceId) && !empty($result) ? $result[0] : $result;
    }

    /**
     * Associates the specified EIP to an EC2 instance.
     *
     * @param   string  $instanceId
     * @param   string  $publicIp
     *
     * @return  array
     */
    public function associateAddress($instanceId, $publicIp)
    {
        $params = [
            'AllowReassociation'    => true,
            'InstanceId'            => $instanceId,
            'PublicIp'              => $publicIp,
        ];

        return $this->sendRequest('associateAddress', $params);
    }

    /**
     * Deassociates the specified EIP.
     *
     * @param   string  $publicIp
     *
     * @return  array
     */
    public function disassociateAddress($associationId = null, $publicIp = null)
    {
        if (empty($associationId) && empty($publicIp)) {
            $this->exception("Invalid Associated ID / Public IP.");
        }

        if (!empty($associationId)) {
            $params['AssociationId'] = $associationId;
        }

        if (!empty($publicIp)) {
            $params['PublicIp'] = $publicIp;
        }

        return $this->sendRequest('disassociateAddress', $params);
    }

    /**
     * Returns the image that has the specified ID.
     *
     * @param   string  $imageId
     *
     * @return  array
     */
    public function describeImage($imageId)
    {
        $params = [
            'ImageIds' => [
                $imageId
            ],
        ];
        $result = $this->sendRequest('describeImages', $params)->get('Images');
        return $result[0] ?? null;
    }

    /**
     * Returns the specified EBS volume.
     *
     * @param   string  $volumeId
     *
     * @return  array
     */
    public function describeVolume($volumeId)
    {
        $params = [
            'VolumeIds' => [ $volumeId ],
        ];
        $result = $this->sendRequest('describeVolumes', $params)->get('Volumes');
        return $result[0] ?? null;
    }

    /**
     * Returns the status of the specified volume.
     *
     * @param   string  $volumeId
     *
     * @return  array
     */
    public function describeVolumeStatus($volumeId)
    {
        $params = [
            'VolumeIds' => [ $volumeId ],
        ];
        $result = $this->sendRequest('describeVolumeStatus', $params)->get('VolumeStatuses');
        return $result[0] ?? null;
    }

    /**
     * Returns the most recent volume modification request for the specified EBS volume.
     *
     * If a volume has never been modified, some information in the output will be null.
     * If a volume has been modified more than once, the output includes only the most
     * recent modification request.
     *
     * @param   string  $volumeId
     *
     * @return  array
     */
    public function describeVolumeModifications($volumeId)
    {
        $params = [
            'VolumeIds' => [ $volumeId ],
        ];
        $result = $this->sendRequest('describeVolumesModifications', $params)->get('VolumesModifications');
        return $result[0] ?? null;
    }

    /**
     * Modifies several parameters of an existing EBS volume
     *
     * @param   string  $volumeId
     * @param   integer $newSize
     * @param   string  $volumeType
     *
     * @return  array
     */
    public function modifyVolumeSize($volumeId, $newSize, $volumeType = null)
    {
        $params = [
            'VolumeId' => $volumeId,
            'Size' => $newSize
        ];
        if (!empty($volumeType)) {
            if (in_array($volumeType, ['standard', 'io1', 'gp2', 'sc1', 'st1'])) {
                $params['VolumeType'] = $volumeType;
            } else {
                $this->exception("Invalid volume type: '$volumeType'.");
            }
        }
        return $this->sendRequest('modifyVolume', $params)->get('VolumeModification');
    }

    /**
     * Returns the instance that has the specified ID.
     *
     * @param   string  $instanceId
     *
     * @return  array
     */
    public function describeSingleInstance($instanceId)
    {
        return $this->describeInstances([
            [
                'Name' => 'instance-id',
                'Values' => [ $instanceId ],
            ]
        ])[0] ?? null;
    }

    /**
     * Returns the list of EC2 instances that has the specified name as tag.
     *
     * @param   array  $name
     *
     * @return  array
     */
    public function describeInstancesByNameTag($name)
    {
        return $this->describeInstances([
            [
                'Name' => 'tag:Name',
                'Values' => [ $name ],
            ]
        ]);
    }

    /**
     * Returns the list of EC2 instances in the specified VPC.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeInstancesByVpcId($vpcId)
    {
        return $this->describeInstances([
            [
                'Name' => 'vpc-id',
                'Values' => [ $vpcId ],
            ]
        ]);
    }

    /**
     * Returns the list of EC2 instances the answer the specified filters.
     *
     * @param   array  $filters
     *
     * @return  array
     */
    public function describeInstances($filters = [])
    {
        $params = [];
        if (! empty($filters)) {
            $params['Filters'] = $filters;
        }
        
        $reservations = $this->sendRequest('DescribeInstances', $params)->get('Reservations');
        return array_map(function ($item) {
            return $item['Instances'][0] ?? [];
        }, $reservations) ?? [];
    }

    /**
     * Returns the tags of the specified instance.
     *
     * @param   array  $instanceId
     *
     * @return  array
     */
    public function describeInstanceTags($instanceId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'resource-id',
                    'Values' => [
                        $instanceId,
                    ],
                ],
            ],
        ];

        return $this->sendRequest('describeTags', $params)->get('Tags');
    }

    /**
     * Returns the volume attached to the specified instance.
     *
     * @param   array  $instanceId
     *
     * @return  array
     */
    public function describeInstanceVolume($instanceId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'attachment.instance-id',
                    'Values' => [
                        $instanceId,
                    ],
                ],
            ],
        ];

        return $this->sendRequest('DescribeVolumes', $params)->get('Volumes')[0];
    }

    /**
     * Starts the specified EC2 instance.
     *
     * @param   string $instanceId
     *
     * @return  array
     */
    public function startInstance($instanceId)
    {
        $params = [
            'InstanceIds' => [ $instanceId ]
        ];

        return $this->sendRequest('startInstances', $params);
    }

    /**
     * Stops the specified EC2 instance.
     *
     * @param   string $instanceId
     *
     * @return  array
     */
    public function stopInstance($instanceId)
    {
        $params = [
            'InstanceIds' => [ $instanceId ]
        ];

        return $this->sendRequest('stopInstances', $params);
    }

    /**
     * Reboots the specified EC2 instance.
     *
     * @param   string $instanceId
     *
     * @return  void
     */
    public function rebootInstance($instanceId)
    {
        $params = [
            'InstanceIds' => [ $instanceId ]
        ];

        $this->sendRequest('rebootInstances', $params);
    }

    /**
     * Terminates the specified EC2 instance.
     *
     * @param   string $instanceId
     *
     * @return  array
     */
    public function terminateInstance($instanceId, $releaseIp = false)
    {
        $params = [
            'InstanceIds' => [ $instanceId ]
        ];

        return $this->sendRequest('terminateInstances', $params)->get('TerminatingInstances');
    }

    /**
     * Checks whether the specified EC2 instance's state is running.
     *
     * @param   string $instanceId
     *
     * @return  boolean
     */
    public function isInstanceRunning($instanceId)
    {
        return $this->isInstanceState($instanceId, 'running');
    }

    /**
     * Checks whether the specified EC2 instance is in the specified state.
     *
     * @param   string $instanceId
     * @param   string $state
     *
     * @return  boolean
     */
    public function isInstanceState($instanceId, $state)
    {
        if (! in_array($state, ['running','pending','shutting-down','terminated','stopping', 'stopped'])) {
            self::exception("The specified state is invalid: '$state'.");
        }

        return $this->describeInstanceStatus($instanceId)['InstanceState']['Name'];
    }

    /**
     * Returns the specified EC2 instance status.
     *
     * @param   string $instanceId
     *
     * @return  array
     */
    public function describeInstanceStatus($instanceId)
    {
        $params = [
            'InstanceIds' => [ $instanceId ]
        ];

        $result = $this->sendRequest('DescribeInstanceStatus', $params)->get('InstanceStatuses');
        if (empty($result)) {
            self::exception("The specified instance was not found.");
        }

        return $result[0];
    }

    /**
     * Returns the list of the AZ.
     *
     * @return  array
     */
    public function describeAvailabilityZones()
    {
        return $this->sendRequest('DescribeAvailabilityZones', []);
    }
    /**
     * Creates a resource tag.
     *
     * @return  array
     */
    public function createTags($resourceId, $tags)
    {
        $params = [
            'Resources' => [ $resourceId ],
            'Tags'      => $this->prepareTagsArray($tags),
        ];

        return $this->sendRequest('createTags', $params);
    }

    /**
     * Creates a new VPC with default configurations.
     *
     * @param   string  $cidrFormat
     *
     * @return  array
     */
    public function createDefaultVpc($cidrFormat, $name)
    {
        $vpcCidr    = sprintf($cidrFormat, "0", "0", "16");
        $vpc        = $this->createVpc($vpcCidr, $name);
        $vpcId      = $vpc['VpcId'];

        // Add availability zones
        $subnetCidrA = sprintf($cidrFormat, "0", "0", "20");
        $subnetA = $this->createSubnet(
            $vpcId,
            $subnetCidrA,
            $this->awsRegion."a"
        );

        $subnetCidrB = sprintf($cidrFormat, "16", "0", "20");
        $subnetB = $this->createSubnet(
            $vpcId,
            $subnetCidrB,
            $this->awsRegion."b"
        );

        $subnetCidrC = sprintf($cidrFormat, "32", "0", "20");
        $subnetC = $this->createSubnet(
            $vpcId,
            $subnetCidrC,
            $this->awsRegion."c"
        );

        // Internet Gateway
        $internetGateway = $this->createInternetGateway();

        // Attach to VPC
        $this->attachInternetGateway(
            $internetGateway['InternetGatewayId'],
            $vpcId
        );

        // Enable DNS resolution
        $this->modifyVpcAttribute($vpcId, true /* EnableDnsHostnames */);

        // Attach internet gateway to Route Table
        $routeTable = $this->describeRouteTables($vpcId)[0];
        $this->createIgwRoute(
            $routeTable['RouteTableId'],
            $internetGateway['InternetGatewayId']
        );

        return [
            "Vpc"               => $vpc,
            "Subnets"           => [ $subnetA, $subnetB, $subnetC ],
            "InternetGateway"   => $internetGateway
        ];
    }

    /**
     * Creates a new VPC.
     *
     * @param   string  $cidrBlock
     * @param   string  $instanceTenancy
     *
     * @return  array
     */
    public function createVpc($cidrBlock, $name = '', $instanceTenancy = 'default')
    {
        $params = [
            'CidrBlock'         => $cidrBlock,
            'InstanceTenancy'   => $instanceTenancy,
        ];

        $vpc = $this->sendRequest('createVpc', $params)->get('Vpc');

        $this->createTags($vpc['VpcId'], [
            'Name'      => $name ?? 'elm-vpc-'.date("y/m/d"),
            'CreatedAt' => date("Y-m-d H:i:s")
        ]);

        return $vpc;
    }

    /**
     * Returns the specified VPC's info.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeVpc($vpcId)
    {
        if (empty($vpcId)) {
            $this->exception("Invalid VPC ID.");
        }

        if (isset($this->_vpcCache[$vpcId])) {
            return $this->_vpcCache[$vpcId];
        }

        $params = [
            'VpcIds' => [ $vpcId ]
        ];

        $vpc = $this->sendRequest('DescribeVpcs', $params)->get('Vpcs')[0] ?? null;
        if (! empty($vpc)) {
            $this->_vpcCache[$vpcId] = $vpc;
        }

        return $vpc;
    }

    /**
     * Returns the specified VPC's info.
     *
     * @param   string  $vpcName
     *
     * @return  array
     */
    public function describeVpcByName($vpcName)
    {
        if (empty($vpcName)) {
            $this->exception("Invalid VPC name.");
        }

        if (isset($this->_vpcCache[$vpcName])) {
            return $this->_vpcCache[$vpcName];
        }

        $params = [
            'Filters' => [
                [
                    'Name' => 'tag:Name',
                    'Values' => [ $vpcName ]
                ]
            ]
        ];

        $vpc = $this->sendRequest('DescribeVpcs', $params)->get('Vpcs')[0] ?? null;
        if (! empty($vpc)) {
            $this->_vpcCache[$vpcName] = $vpc;
        }

        return $vpc;
    }

    /**
     * Deletes the specified VPC and its resources:
     * - Subnets
     * - Security Groups
     * - Network ACLs
     * - VPN Attachments
     * - Internet Gateways
     * - Route Tables
     * - Network Interfaces
     * - VPC Peering Connections
     *
     * @param   string      $vpcId
     * @param   boolean     $force
     *
     * @return  array
     */
    public function deleteVpc($vpcId, $force = true)
    {
        $vpc = $this->describeVpc($vpcId);

        // Check if any EC2 instances are running in the VPC
        $ec2Instances = $this->describeInstances($vpcId);
        if (! empty($ec2Instances)) {
            $this->exception("You must first delete all EC2 instances.");
        }

        // Delete internet gateways
        $igws = $this->describeInternetGateways($vpcId);
        foreach ($igws ?? [] as $igw) {
            $this->detachInternetGateway($vpcId, $igw['InternetGatewayId']);
            $this->deleteInternetGateway($igw['InternetGatewayId']);
        }

        // Delete all subnets
        $subnets = $this->describeSubnets($vpcId);
        foreach ($subnets ?? [] as $subnet) {
            $this->deleteSubnet($subnet['SubnetId']);
        }

        // Delete all route tables
        $rtbs = $this->describeRouteTables($vpcId);
        foreach ($rtbs ?? [] as $rtb) {
            $this->deleteRouteTable($rtb['RouteTableId']);
        }

        // Delete all security groups
        $sgrps = $this->describeSecurityGroups($vpcId);
        foreach ($sgrps ?? [] as $sg) {
            $this->deleteSecurityGroup($sg['GroupId']);
        }
        
        // Delete VPC
        return $this->sendRequest('deleteVpc', [
            'VpcId' => $vpcId
        ]);
    }

    /**
     * Modifies the specified VPC's attributes.
     *
     * @param   string  $vpcId
     * @param   string  $enableDnsHostnames
     * @param   boolean $enableDnsSupport
     *
     * @return  array
     */
    public function modifyVpcAttribute($vpcId, $enableDnsHostnames, $enableDnsSupport = false)
    {
        $params = [
            'EnableDnsHostnames' => [
                'Value' => $enableDnsHostnames,
            ],
            'VpcId' => $vpcId,
        ];

        return $this->sendRequest('modifyVpcAttribute', $params);
    }

    /**
     * Disassociates all VPC Cidr blocks.
     *
     * @param   string  $associationId
     *
     * @return  array
     */
    public function disassociateVpcCidrBlocks($vpc)
    {
        foreach ($vpc['CidrBlockAssociationSet'] ?? [] as $assoc) {
            $this->disassociateVpcCidrBlock($assoc['AssociationId']);
        }
    }

    /**
     * Disassociates VPC Cidr block.
     *
     * @param   string  $associationId
     *
     * @return  array
     */
    public function disassociateVpcCidrBlock($associationId)
    {
        $params = [
            'AssociationId' => $associationId
        ];

        return $this->sendRequest('disassociateVpcCidrBlock', $params);
    }

    /**
     * Creates a new subnet in the specified VPC, cidrBlock and AZ.
     *
     * @param   string  $vpcId
     * @param   string  $cidrBlock
     * @param   string  $availZone
     *
     * @return  array
     */
    public function createSubnet($vpcId, $cidrBlock, $availZone)
    {
        $params = [
            'AvailabilityZone' => $availZone,
            'CidrBlock' => $cidrBlock,
            // 'DryRun' => true || false,
            // 'Ipv6CidrBlock' => '<string>',
            'VpcId' => $vpcId,
        ];

        return $this->sendRequest('createSubnet', $params)->get('Subnet');
    }

    /**
     * Deletes a subnet in the specified VPC.
     *
     * @param   string  $subnetId
     *
     * @return  void
     */
    public function deleteSubnet($subnetId)
    {
        $params = [
            'SubnetId' => $subnetId
        ];

        $this->sendRequest('deleteSubnet', $params);
    }

    /**
     * Returns the specified VPC's subnets.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeSubnets($vpcId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'vpc-id',
                    'Values' => [ $vpcId ],
                ],
            ]
        ];
        
        return $this->sendRequest('describeSubnets', $params)->get('Subnets');
    }

    /**
     * Modifies the specified subnet.
     *
     * @param   string  $subnetId
     * @param   string  $mapPublicIpOnLaunch
     *
     * @return  array
     */
    public function modifySubnetAttribute($subnetId, $mapPublicIpOnLaunch)
    {
        $params = [
            'MapPublicIpOnLaunch' => [
                'Value' => true,
            ],
            'SubnetId' => $subnetId,
        ];

        return $this->sendRequest('modifySubnetAttribute', $params);
    }

    /**
     * Returns the specified subnet's info.
     *
     * @param   string  $subnetId
     *
     * @return  array
     */
    public function describeSubnet($subnetId)
    {
        $params = [
            'SubnetIds' => [ $subnetId ],
        ];

        $subnets = $this->sendRequest('describeSubnets', $params)->get('Subnets');

        return empty($subnets) ? [] : $subnets[0];
    }
    
    /**
     * Creates a new security group.
     *
     * @param   string  $groupName
     * @param   string  $vpcId
     * @param   string  $description
     *
     * @return  array
     */
    public function createSecurityGroup($groupName, $vpcId = null, $description = "")
    {
        $description = empty($description) ? "Created by Elemento" : $description;

        $params = [
            'Description' => $description,
            // 'DryRun' => true || false,
            'GroupName' =>  $groupName,
            'VpcId' => $vpcId
        ];

        if (! empty($vpcId)) {
            $params['VpcId'] = $vpcId;
        }

        $groupId = $this->sendRequest('CreateSecurityGroup', $params)->get('GroupId');

        $this->_sgNameToIdCache[$groupName] = $groupId;

        return $groupId;
    }

    /**
     * Returns the specified security group's info.
     *
     * @param   string  $groupName
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function getSecurityGroupId($groupName, $vpcId = null)
    {
        return $this->_sgNameToIdCache[$groupName] ?? $this->describeSecurityGroup($groupName, $vpcId)['GroupId'];
    }

    /**
     * Returns the security groups in the specified VPC.
     *
     * @param   string  $vpcId
     * @param   string  $groupName
     *
     * @return  array
     */
    public function describeSecurityGroups($vpcId = null, $groupName = null)
    {
        if (isset($this->_sgCache[$groupName])) {
            return $this->_sgCache[$groupName];
        }

        $params = [];

        if (! empty($vpcId)) {
            $params['Filters'][] = [
                'Name' => 'vpc-id',
                'Values' => [ $vpcId ]
            ];
        }

        if (! empty($groupName)) {
            $params['Filters'][] = [
                'Name' => 'group-name',
                'Values' => [ $groupName ]
            ];
        }

        try {
            return $this->sendRequest('describeSecurityGroups', $params)->get('SecurityGroups');
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Returns the specified security group's info.
     *
     * @param   string  $groupName
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeSecurityGroup($groupName, $vpcId = null)
    {
        if (isset($this->_sgCache[$groupName])) {
            return $this->_sgCache[$groupName];
        }

        $group = $this->describeSecurityGroups($vpcId, $groupName)[0] ?? null;
        if (! empty($group)) {
            $this->_sgCache[$groupName] = $group;
        }

        return $group;
    }
    
    /**
     * Deletes the specified security group.
     *
     * @param   string  $groupId
     *
     * @return  array
     */
    public function deleteSecurityGroup($groupId, $retryIfFailed = false)
    {
        if (empty($groupId)) {
            return;
        }

        $params = [
            'GroupId' => $groupId
        ];

        try {
            $this->sendRequest('deleteSecurityGroup', $params);
        } catch (Exception $e) {
            if ($retryIfFailed) {
                $this->print("Sleeping for 60s before retrying to delete security group '$groupId'...");
                sleep(60);
                $this->deleteSecurityGroup($groupId, false);
            }
        }
    }

    /**
     * Deletes the specified security group's info.
     *
     * @param   string  $groupName
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function deleteSecurityGroupByName($groupName, $vpcId = null)
    {
        $id = $this->getSecurityGroupId($groupName, $vpcId);
        return $this->deleteSecurityGroup($id);
    }

    /**
     * Adds a source security group rule to the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $sourceSecurityGroupName
     * @param   string  $groupId
     * @param   string  $sourceGroupId
     * @param   string  $customVpcId
     *
     * @return  array
     */
    public function addSecurityGroupIngressSourceSecurityGroupRule(
        $groupName,
        $sourceSecurityGroupName,
        $groupId = null,
        $sourceGroupId = null,
        $customVpcId = null
    ) {
        return $this->addOrRemoveSecurityGroupIngressSourceSecurityGroupRule(
            $groupName,
            $sourceSecurityGroupName,
            $customVpcId,
            $groupId,
            $sourceGroupId,
            'add'
        );
    }

    /**
     * Removes a source security group rule from the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $sourceSecurityGroupName
     * @param   string  $groupId
     * @param   string  $sourceGroupId
     * @param   string  $customVpcId
     *
     * @return  array
     */
    public function removeSecurityGroupIngressSourceSecurityGroupRule(
        $groupName,
        $sourceSecurityGroupName,
        $groupId = null,
        $sourceGroupId = null,
        $customVpcId = null
    ) {
        return $this->addOrRemoveSecurityGroupIngressSourceSecurityGroupRule(
            $groupName,
            $sourceSecurityGroupName,
            $customVpcId,
            $groupId,
            $sourceGroupId,
            'remove'
        );
    }

    /**
     * Adds an IP rule to the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $port
     * @param   string  $groupId
     * @param   string  $allowAll
     * @param   array   $ipV4Ranges
     * @param   array   $ipV6Ranges
     * @param   string  $ipProtocol
     *
     * @return  array
     */
    public function addSecurityGroupIngressIpRule(
        $groupName,
        $port,
        $groupId = null,
        $allowAll = false,
        $ipV4Ranges = [],
        $ipV6Ranges = [],
        $ipProtocol = 'tcp'
    ) {
        return $this->addOrRemoveSecurityGroupIngressIpRule(
            $groupName,
            $port,
            $groupId,
            $allowAll,
            $ipV4Ranges,
            $ipV6Ranges,
            $ipProtocol,
            'add'
        );
    }

    /**
     * Removes an IP rule from the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $port
     * @param   string  $groupId
     * @param   string  $allowAll
     * @param   array   $ipV4Ranges
     * @param   array   $ipV6Ranges
     * @param   string  $ipProtocol
     *
     * @return  array
     */
    public function removeSecurityGroupIngressIpRule(
        $groupName,
        $port,
        $groupId = null,
        $allowAll = false,
        $ipV4Ranges = [],
        $ipV6Ranges = [],
        $ipProtocol = 'tcp'
    ) {
        return $this->addOrRemoveSecurityGroupIngressIpRule(
            $groupName,
            $port,
            $groupId,
            $allowAll,
            $ipV4Ranges,
            $ipV6Ranges,
            $ipProtocol,
            'remove'
        );
    }

    /**
     * Creates a new key pair.
     *
     * @param   string  $keyName
     *
     * @return  array
     */
    public function createKeyPair($keyName)
    {
        $params = [
            'KeyName' => $keyName
        ];

        return $this->sendRequest('createKeyPair', $params);
    }

    /**
     * Deletes the specified key pair name.
     *
     * @param   string  $keyName
     *
     * @return  bool
     */
    public function keyPairExists($keyName)
    {
        $params = [
            'KeyNames' => [ $keyName ]
        ];

        try {
            $pairs = $this->sendRequest('describeKeyPairs', $params)->get('KeyPairs');
            return ! empty($pairs);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Deletes the specified key pair name.
     *
     * @param   string  $keyName
     *
     * @return  void
     */
    public function deleteKeyPair($keyName)
    {
        $params = [
            'KeyName' => $keyName
        ];

        return $this->sendRequest('deleteKeyPair', $params);
    }

    /**
     * Creates an internet gateway.
     *
     * @return  array
     */
    public function createInternetGateway()
    {
        $params = [];

        return $this->sendRequest('createInternetGateway', $params)->get('InternetGateway');
    }

    /**
     * Describes internet gateways belonging to the specified VPC.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeInternetGateways($vpcId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'attachment.vpc-id',
                    'Values' => [ $vpcId ],
                ]
            ],
        ];

        return $this->sendRequest('describeInternetGateways', $params)->get('InternetGateways');
    }

    /**
     * Detaches an internet gateway from the specified VPC.
     *
     * @param   string  $vpcId
     * @param   string  $igwId
     *
     * @return  void
     */
    public function detachInternetGateway($vpcId, $igwId)
    {
        $params = [
            'InternetGatewayId' => $igwId,
            'VpcId' => $vpcId
        ];

        $this->sendRequest('detachInternetGateway', $params);
    }

    /**
     * Deletes the internet gateway.
     *
     * Note that the internet gateway must be detached from the VPC.
     *
     * @param   string  $igwId
     *
     * @return  void
     */
    public function deleteInternetGateway($igwId)
    {
        $params = [
            'InternetGatewayId' => $igwId
        ];

        $this->sendRequest('deleteInternetGateway', $params);
    }


    /**
     * Attaches internet gateway to Vpc.
     *
     * @param   string  $internetGatewayId
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function attachInternetGateway($internetGatewayId, $vpcId)
    {
        $params = [
            // 'DryRun' => true || false,
            'InternetGatewayId' => $internetGatewayId,
            'VpcId' => $vpcId,
        ];

        return $this->sendRequest('attachInternetGateway', $params);
    }

    /**
     * Describes the route tables in the specified VPC.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeRouteTables($vpcId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'vpc-id',
                    'Values' => [ $vpcId ],
                ]
            ],
        ];

        return $this->sendRequest('DescribeRouteTables', $params)->get('RouteTables');
    }

    /**
     * Describes the specified route table.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeRouteTable($rtbId)
    {
        $params = [
            'RouteTableIds' => [ $rtbId ]
        ];

        return $this->sendRequest('DescribeRouteTables', $params)->get('RouteTables')[0] ?? null;
    }

    /**
     * Deletes a route table.
     *
     * You must disassociate the route table from any subnets before you can delete it.
     * You can't delete the main route table.
     *
     * @param   string  $rtbId
     *
     * @return  array
     */
    public function deleteRouteTable($rtbId, $force = true)
    {
        // Delete routes
        $rtb = $this->describeRouteTable($rtbId);
        foreach ($rtb['Routes'] ?? [] as $route) {
            $this->sendRequest('deleteRoute', [
                'RouteTableId' => $rtbId,
                'DestinationCidrBlock' => $route['DestinationCidrBlock']
            ]);
        }

        foreach ($rtb['Associations'] as $assoc) {
            $this->sendRequest('disassociateRouteTable', [
                'AssociationId' => $assoc['RouteTableAssociationId']
            ]);
        }

        // Delete table
        $params = [
            'RouteTableId' => $rtbId
        ];

        $this->sendRequest('deleteRouteTable', $params);
    }

    /**
     * Describes the network ACLs in the specified VPC.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function describeNetworkAcls($vpcId)
    {
        $params = [
            'Filters' => [
                [
                    'Name' => 'vpc-id',
                    'Values' => [ $vpcId ],
                ]
            ],
        ];

        return $this->sendRequest('DescribeNetworkAcls', $params)->get('NetworkAcls');
    }

    /**
     * Describes the network ACLs in the specified VPC.
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function deleteNetworkAcl($networkAclId)
    {
        $params = [
            'NetworkAclId' => $networkAclId,
        ];

        $this->sendRequest('deleteNetworkAcl', $params);
    }

    /**
     * Creates internet gateway route.
     *
     * @param   string  $routeTableId
     * @param   string  $igwId
     *
     * @return  array
     */
    public function createIgwRoute($routeTableId, $igwId)
    {
        $params = [
            'DestinationCidrBlock' => '0.0.0.0/0',
            'GatewayId' => $igwId,
            'RouteTableId' => $routeTableId
        ];

        return $this->sendRequest('createRoute', $params);
    }

    /**
     * Deletes
     *
     * @param   string  $vpcId
     *
     * @return  array
     */
    public function deleteRouteTableSubnets($routeTableId)
    {
        $params = [
            'RouteTableId' => $routeTableId
        ];

        $this->sendRequest('deleteRouteTable', $params);
    }

    /**
     * Adds/Removes an IP rule to/from the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $port
     * @param   string  $groupId
     * @param   string  $allowAll
     * @param   array   $ipV4Ranges
     * @param   array   $ipV6Ranges
     * @param   string  $ipProtocol
     * @param   string  $operation
     *
     * @return  array
     */
    private function addOrRemoveSecurityGroupIngressIpRule(
        $groupName,
        $port,
        $groupId = null,
        $allowAll = false,
        $ipV4Ranges = [],
        $ipV6Ranges = [],
        $ipProtocol = 'tcp',
        $operation = 'add'
    ) {
        // Allow access from all IPs
        if ($allowAll) {
            // Allow access from all IPv4
            $ipV4Ranges = [
                [
                    'CidrIp' => '0.0.0.0/0'
                ]
            ];
            // Allow access from all IPv6
            $ipV6Ranges = [
                [
                    'CidrIpv6' => '::/0'
                ]
            ];
        } else {
            // Validate IPv4 ranges and set default description
            foreach ($ipV4Ranges as $i => $ipRange) {
                if (empty($ipRange['CidrIp'])) {
                    $this->exception("IPv4 range missing CidrIp key.");
                }
                $ipV4Ranges[$i]['Description'] = $ipRange['Description'] ?? 'Created via AWS Builder';
            }
            // Validate IPv6 ranges and set default description
            foreach ($ipV6Ranges as $i => $ipRange) {
                if (empty($ipRange['CidrIpv6'])) {
                    $this->exception("IPv6 range missing CidrIpv6 key.");
                }
                $ipV6Ranges[$i]['Description'] = $ipRange['Description'] ?? 'Created via AWS Builder';
            }
        }

        $params = [
            // 'DryRun' => true || false,
            'GroupId' => $groupId ?? $this->getSecurityGroupId($groupName),
            // 'GroupName' => $groupName,
            'IpPermissions' => [
                [
                    'FromPort' => $port,
                    'ToPort' => $port,
                    'IpProtocol' => $ipProtocol,
                ],
            ],
        ];

        if (!empty($ipV4Ranges)) {
            $params['IpPermissions'][0]['IpRanges'] = $ipV4Ranges;
        }

        if (!empty($ipV6Ranges)) {
            $params['IpPermissions'][0]['Ipv6Ranges'] = $ipV6Ranges;
        }

        $functionName =  $operation === 'add' ? 'AuthorizeSecurityGroupIngress' : 'revokeSecurityGroupIngress';
        return $this->sendRequest($functionName, $params);
    }

    /**
     * Adds/removes a source security group rule to/from the specified security group.
     *
     * @param   string  $groupName
     * @param   string  $sourceSecurityGroupName
     * @param   string  $customVpcId
     * @param   string  $groupId
     * @param   string  $sourceGroupId
     * @param   string  $operation
     *
     * @return  array
     */
    private function addOrRemoveSecurityGroupIngressSourceSecurityGroupRule(
        $groupName,
        $sourceSecurityGroupName,
        $customVpcId = null,
        $groupId = null,
        $sourceGroupId = null,
        $operation = 'add'
    ) {
        $params = [
            'GroupId' => $groupId ?? $this->getSecurityGroupId($groupName),
        ];

        if (! empty($customVpcId)) {
            // Get source group ID if not set
            $sourceGroupId = $sourceGroupId ?? $this->getSecurityGroupId($sourceSecurityGroupName);

            // Set permissions
            $params['IpPermissions'] = [
                [
                    'IpProtocol' => '-1',
                    'UserIdGroupPairs' => [
                        [
                            'GroupId'   => $sourceGroupId,
                            // 'VpcId' => $customVpcId
                        ],
                    ],
                ],
            ];
        } else {
            $params['SourceSecurityGroupName'] = $sourceSecurityGroupName;
        }
            
        $functionName = $operation === 'add' ? 'AuthorizeSecurityGroupIngress' : 'revokeSecurityGroupIngress';
        return $this->sendRequest($functionName, $params);
    }
}
