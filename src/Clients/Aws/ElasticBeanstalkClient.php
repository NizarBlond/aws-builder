<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

class ElasticBeanstalkClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     *
     * Reference:
     * - http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-elasticbeanstalk-2010-12-01.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2010-12-01';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'EB';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\ElasticBeanstalk\ElasticBeanstalkClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Creates an application that has one configuration template named
     * default and no application versions.
     *
     * @param   string  $appName    The application name.
     * @return  ApplicationDescription
     */
    public function createApplication($appName)
    {
        $params = [
            'ApplicationName' => $appName
        ];

        return $this->sendRequest('createApplication', $params);
    }

    /**
     * Creates an application that has one configuration template named
     * default and no application versions.
     *
     * @param   string      $appName            The name of the application to delete.
     * @param   boolean     $forceDeleteEnv     If true, running environments will be terminated before deleting. Optional.
     * @return  []|Exception
     */
    public function deleteApplication($appName, $forceDeleteEnv = false)
    {
        $params = [
            'ApplicationName' => $appName, // REQUIRED
            'TerminateEnvByForce' => $forceDeleteEnv,
        ];

        return $this->sendRequest('deleteApplication', $params);
    }

    /**
     * Creates an application version for the specified application.
     *
     * @param   string  $appName    The application name.
     * @param   string  $label      The version label.
     * @param   string  $s3Bucket   The source S3 bucket.
     * @param   string  $s3Key      The source S3 path.
     * @return  ApplicationVersionDescription
     */
    public function createApplicationVersion($appName, $label, $s3Bucket, $s3Key)
    {
        $params = [
            'ApplicationName' => $appName,
            'SourceBundle' => [
                'S3Bucket' => $s3Bucket, // e.g. "mybucket"
                'S3Key' => $s3Key, // e.g. "/erm/v1.zip"
            ],
            'VersionLabel' => $label, // REQUIRED
        ];

        return $this->sendRequest('createApplicationVersion', $params)->get('ApplicationVersion');
    }
    
    /**
     * Creates a sample application version for the specified application.
     *
     * @param   string  $appName    The application name.
     * @param   string  $label      The version label.
     * @return  ApplicationVersionDescription
     */
    public function createSampleApplicationVersion($appName, $label = "Sample App")
    {
        $params = [
            'ApplicationName' => $appName,
            'VersionLabel' => $label,
        ];

        return $this->sendRequest('createApplicationVersion', $params)->get('ApplicationVersion');
    }

    /**
     * Returns the descriptions of existing applications.
     *
     * @param   array  $apps    If specified, AWS Elastic Beanstalk restricts
     *                          the returned descriptions to only include those
     *                          with the specified names.
     * @return  array
     */
    public function describeApplications($apps = [])
    {
        $params = [
            'ApplicationNames' => $apps,
        ];

        return $this->sendRequest('describeApplications', $params);
    }

    /**
     * Returns the descriptions of all environments.
     *
     * @return  array
     */
    public function describeEnvironments()
    {
        return $this->sendRequest('describeEnvironments', [])->get('Environments');
    }

    /**
     * Returns the descriptions of all environment config settings.
     *
     * @param   string  $envName
     * @param   string  $appName
     *
     * @return  array
     */
    public function describeConfigurationSettings($envName, $appName = null)
    {
        $params = [
            'ApplicationName' => $appName,
            'EnvironmentName' => $envName,
        ];

        return $this->sendRequest('describeConfigurationSettings', $params)
                    ->get('ConfigurationSettings');
    }

    /**
     * Returns the the managed actions of the specified env.
     *
     * @param   string  $envId
     *
     * @return  array
     */
    public function describeManagedActions($envId)
    {
        $params = [
            // 'EnvironmentName' => $name,
            'EnvironmentId' => $envId
        ];

        return $this->sendRequest('DescribeEnvironmentManagedActions', $params)->get('ManagedActions');
    }

    /**
     * Returns whether the application with the specified name exists.
     *
     * @param   string  $appName
     * @return  boolean
     */
    public function applicationExists($appName)
    {
        $result = $this->describeApplications([$appName]);

        return ! empty($result['Applications']);
    }

    /**
     * Returns a list of the available solution stack names, with the public
     * version first and then in reverse chronological order.
     *
     * @return SolutionStackDescription
     */
    public function listAvailableSolutionStacks()
    {
        return $this->sendRequest('ListAvailableSolutionStacks', []);
    }

    /**
     * Creates a new env in the specified app.
     *
     * @param   string          $appName
     * @param   string          $envName
     * @param   string          $versionLabel
     * @param   string|null     $solutionStackName
     * @param   string|null     $platformArn
     * @param   array           $options
     * @param   array           $tags
     *
     * @return array
     */
    public function createEnvironment(
        $appName,
        $envName,
        $versionLabel,
        $solutionStackName = null,
        $platformArn = null,
        $options = [],
        $tags = []
    ) {
        // Prepare parameters
        $optionSettings = $this->ungroupOptions($options);
        $params = [
            'ApplicationName'   => $appName,
            'EnvironmentName'   => $envName,
            'OptionSettings'    => $optionSettings,
            'Tags'              => $tags,
            'VersionLabel'      => $versionLabel,
        ];

        if (! empty($solutionStackName)) {
            $params['SolutionStackName'] = $solutionStackName;
        }

        if (! empty($platformArn)) {
            $params['PlatformArn'] = $platformArn;
        }

        return $this->sendRequest('createEnvironment', $params);
    }

    public function configureEnvironmentCapacity(
        $environmentId,
        $minInstances,
        $maxInstances,
        $scheduleAt = null,
        $scheduleResourceName = "awsbuilder"
    ) {
        $ns = empty($scheduleAt)
                ? 'aws:autoscaling:asg'
                : 'aws:autoscaling:scheduledaction';

        $params = [
            'EnvironmentId' => $environmentId,
            'OptionSettings' => [
                [
                    'Namespace' => $ns,
                    'OptionName' => 'MinSize',
                    'ResourceName' => 'AWSEBAutoScalingGroup',
                    'Value' => $minInstances,
                ],
                [
                    'Namespace' => $ns,
                    'OptionName' => 'MaxSize',
                    'ResourceName' => 'AWSEBAutoScalingGroup',
                    'Value' => $maxInstances,
                ],
            ]
        ];

        if (! empty($scheduleAt)) {
            $params['OptionSettings'][] = [
                'Namespace' => 'aws:autoscaling:scheduledaction',
                'OptionName' => 'StartTime',
                'Value' => $scheduleAt,
                'ResourceName' => $scheduleResourceName,
            ];

            $params['OptionSettings'][0]['ResourceName'] = $scheduleResourceName;
            $params['OptionSettings'][1]['ResourceName'] = $scheduleResourceName;
        }

        return $this->sendRequest('updateEnvironment', $params);
    }

    public function rebuildEnvironment($environmentId, $forceTerminate = true)
    {
        $params = [
            'EnvironmentId' => $environmentId,
            'ForceTerminate' => $forceTerminate,
            'TerminateResources' => true,
        ];
        
        return $this->sendRequest('rebuildEnvironment', $params);
    }

    public function stopEnvironment($environmentId)
    {
        $scheduleAt = gmdate("Y-m-d\TH:i:s\Z", strtotime("+3 minutes"));
        $this->configureEnvironmentCapacity($environmentId, 0, 0, $scheduleAt, "stoponce");
    }

    public function startEnvironment($environmentId)
    {
        $scheduleAt = gmdate("Y-m-d\TH:i:s\Z", strtotime("+3 minutes"));
        $this->configureEnvironmentCapacity($environmentId, 1, 1, $scheduleAt, "startonce");
    }

    public function terminateEnvironment($environmentId)
    {
        $params = [
            'EnvironmentId' => $environmentId
        ];
        
        return $this->sendRequest('terminateEnvironment', $params);
    }

    private function ungroupOptions($optionGroups)
    {
        if (empty($optionGroups)) {
            return [];
        }

        $result = [];
        foreach ($optionGroups as $namespace => $options) {
            if (empty($options)) {
                continue;
            }
            foreach ($options as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                $result[] = $this->formatOption($namespace, $key, $value);
            }
        }

        return $result;
    }

    private function formatOption($namespace, $optionName, $value)
    {
        if (is_bool($value)) {
            $value = $value ? 'true' : 'false';
        }

        return [
            'Namespace' => $namespace,
            'OptionName' => $optionName,
            'Value' => $value
        ];
    }
}
