<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;
use Illuminate\Support\Arr;
use NizarBlond\AwsBuilder\Helpers\StringHelper;

class CloudFrontClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-cloudfront-2018-11-05.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = 'latest';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'CloudFront';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\CloudFront\CloudFrontClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Creates a new distribution.
     *
     * @param   array  $userConfig
     *
     * @return  array
     */
    public function createDistribution($userConfig)
    {
        // Distribution config
        $config = [
            "Aliases" => [
                "Items" => [],
                "Quantity" => 0
            ],
            'CallerReference' => time(),
            'Comment' => "",
            'DefaultRootObject' => '',
            'Enabled' => true,
            'HttpVersion' => 'http2',
            'IsIPV6Enabled' => true,
            'Logging' => [
                'Bucket' => '',
                'Enabled' => false,
                'IncludeCookies' => false,
                'Prefix' => '',
            ],
            'PriceClass' => "PriceClass_All",
            'Restrictions' => [
                'GeoRestriction' => [
                    'Quantity' => 0,
                    'RestrictionType' => 'none',
                ],
            ],
            'WebACLId' => ''
        ];

        $config = array_merge($config, $userConfig);

        $params = [];
        if (empty($userConfig['Tags'])) {
            $params['DistributionConfig'] = $config;
            $method = 'createDistribution';
        } else {
            $params['DistributionConfigWithTags'] = [
                'DistributionConfig' => $config,
                'Tags' => [
                    'Items' => $userConfig['Tags']
                ],
                'Quantity' => count($userConfig['Tags'])
            ];
            $method = 'CreateDistributionWithTags';
        }

        return $this->sendRequest($method, $params)->get('Distribution') ?? null;
    }

    /**
     * Returns a the distribution config.
     *
     * @param   string  $distributionId
     *
     * @return  array
     */
    public function getDistribution($distributionId, $incIfMatch = false)
    {
        $params = [
            'Id' => $distributionId
        ];

        $result = $this->sendRequest('GetDistribution', $params);
        return $incIfMatch ? $result : $result->get('Distribution');
    }

    /**
     * Returns a the distribution config or fails if not found.
     *
     * @param   string  $distributionId
     *
     * @return  array
     */
    public function getDistributionOrFail($distributionId)
    {
        // Get distribution details
        $result = $this->getDistribution($distributionId, true);
        $dist = $result['Distribution'];

        // Get config
        $distConfig = $dist['DistributionConfig'] ?? [];
        if (empty($distConfig)) {
            $this->exception("Distribution with ID='$distributionId' was not found.");
        }

        // Get the current version of the configuration
        $ifMatch = $result['ETag'] ?? null;
        if (empty($ifMatch)) {
            $this->exception("The current config version is missing.");
        }

        return [
            'DistributionConfig' => $distConfig,
            'IfMatch' => $ifMatch,
            'ARN' => $dist['ARN']
        ];
    }

    /**
     * Returns a the distribution config.
     *
     * @param   string  $distributionId
     *
     * @return  array
     */
    public function getDistributionConfig($distributionId)
    {
        $params = [
            'Id' => $distributionId
        ];

        $result = $this->sendRequest('getDistributionConfig', $params);
        return $result ? $result->get('DistributionConfig') : null;
    }

    /**
     * Lists all distributions.
     *
     * @return  array
     */
    public function listDistributions()
    {
        $allDists = [];
        $marker = null;
        $params = [];

        while (true) {
            // Add marker to parameters if exists
            if ($marker) {
                $params['Marker'] = $marker;
            }

            // Get max users possible (default is 100)
            $result = $this->sendRequest('listDistributions', $params)->get('DistributionList');

            // Add users to result
            $allDists = array_merge($allDists, $result['Items'] ?? []);

            // Check if there are more results to fetch
            if ($result['IsTruncated'] === false) {
                break;
            }

            $marker = $result['NextMarker'];
        }

        return $allDists;
    }

    /**
     * Finds the distributions that have the specified origin IDs.
     *
     * @param   array  $originIds
     *
     * @return  array
     */
    public function getDistributionByOriginIds($originIds)
    {
        $allDists = $this->listDistributions();

        $results = [];
        foreach ($allDists ?? [] as $dist) {
            $currOriginIds = Arr::pluck($dist['Origins']['Items'] ?? [], 'Id');
            if (Str::compareArrays($originIds, $currOriginIds)) {
                $results[] = $dist;
            }
        }

        return $results;
    }

    /**
     * Disables the specified distribution.
     *
     * @param   string  $distributionId
     * @param   array   $tags
     *
     * @return  array
     */
    public function disableDistribution($distributionId, $tags = [])
    {
        return $this->updateDistribution($distributionId, [
            'Enabled' => false
        ], $tags);
    }

    /**
     * Updates the SSL certificate of the specified distribution.
     *
     * @param   string  $distributionId
     * @param   array   $tags
     *
     * @return  array
     */
    public function updateDistributionViewerCertificate(
        $distributionId,
        $certificateArn,
        $certificateSource = 'acm',
        $sslSupportedMethod = 'sni-only',
        $minProtocolVersion = 'TLSv1.1_2016',
        $tags = []
    ) {
        return $this->updateDistribution($distributionId, [
            'ViewerCertificate' => [
                'ACMCertificateArn' => $certificateArn,
                'CertificateSource' => $certificateSource,
                'MinimumProtocolVersion' => $minProtocolVersion,
                'SSLSupportMethod' => $sslSupportedMethod
            ]
        ], $tags);
    }

    /**
     * Updates the custom origin of distribution.
     *
     * @param   string  $distributionId
     * @param   array   $tags
     *
     * @return  array
     */
    public function updateDistributionCustomOrigin($distributionId, $newOrigin)
    {
        $dist = $this->getDistributionOrFail($distributionId);
        $distConfig = $dist['DistributionConfig'];
        $ifMatch = $dist['IfMatch'];

        foreach ($distConfig['Origins']['Items'] ?? [] as $key => $item) {
            if (! isset($item['CustomOriginConfig'])) {
                continue;
            }

            $distConfig['Origins']['Items'][$key]['DomainName'] = $newOrigin;
        }

        $params = [
            'DistributionConfig' => $distConfig,
            'Id' => $distributionId,
            'IfMatch' => $ifMatch
        ];

        return $this->sendRequest('updateDistribution', $params)->get('ETag') ?? null;
    }

    /**
     * Returns the default S3 origin configuration.
     *
     * @param   string  $distributionId
     * @param   string  $eventType
     * @param   string  $lambdaFunctionArn
     *
     * @return  array
     */
    public function associateLambdaFunction($distributionId, $eventType, $lambdaFunctionArn, ?string $pathPattern, bool $applyToDefault)
    {
        if ($applyToDefault && !empty($pathPattern) || !$applyToDefault && empty($pathPattern)) {
            $this->exception('You must either choose default behavior or path pattern.');
        }

        $dist = $this->getDistributionOrFail($distributionId);
        $distConfig = $dist['DistributionConfig'];

        if ($applyToDefault) {
            // Default behavior
            $defaultBehavior = $distConfig['DefaultCacheBehavior'];
            $lambdaFuncs = $defaultBehavior['LambdaFunctionAssociations']['Items'] ?? [];
        } else {
            // Custom behaviors
            $cacheBehaviors = $distConfig['CacheBehaviors']['Items'] ?? [];
            $cbItemKey = null;
            foreach ($cacheBehaviors as $key => $item) {
                if ($item['PathPattern'] === $pathPattern) {
                    $cbItemKey = $key;
                    break;
                }
            }
            if (empty($cbItemKey)) {
                $this->exception('Cache behavior item not found.');
            }
            $lambdaFuncs = $cacheBehaviors[$cbItemKey]['LambdaFunctionAssociations']['Items'] ?? [];
        }

        // Add or edit associated function
        $updated = false;
        foreach ($lambdaFuncs ?? [] as $key => $item) {
            if ($lambdaFuncs[$key]['EventType'] === $eventType) {
                $lambdaFuncs[$key]['LambdaFunctionARN'] = $lambdaFunctionArn;
                $updated = true;
                break;
            }
        }
        if (!$updated) {
            $lambdaFuncs[] = [
                'EventType' => $eventType,
                'LambdaFunctionARN' => $lambdaFunctionArn
            ];
        }

        // Update cache behavior
        $toUpdate = [];
        if ($applyToDefault) {
            $defaultBehavior['LambdaFunctionAssociations'] = $this->convertToConfigItems($lambdaFuncs);
            $toUpdate['DefaultCacheBehavior'] = $defaultBehavior;
        } else {
            $cacheBehaviors[$cbItemKey]['LambdaFunctionAssociations'] = $this->convertToConfigItems($lambdaFuncs);
            $toUpdate['CacheBehaviors'] = $this->convertToConfigItems($cacheBehaviors);
        }

        return $this->updateDistribution($distributionId, $toUpdate);
    }

    /**
     * Updates the config of the specified distribution.
     *
     * @param   string  $distributionId
     * @param   array   $toUpdate
     * @param   array   $tags
     *
     * @return  array
     */
    public function updateDistribution($distributionId, $toUpdate, $tags = [])
    {
        $dist = $this->getDistributionOrFail($distributionId);

        $distConfig = $dist['DistributionConfig'];
        $ifMatch = $dist['IfMatch'];

        foreach ($toUpdate as $key => $value) {
            $distConfig[$key] = $value;
        }

        $params = [
            'DistributionConfig' => $distConfig,
            'Id' => $distributionId,
            'IfMatch' => $ifMatch
        ];

        $ifMatch = $this->sendRequest('updateDistribution', $params)->get('ETag') ?? null;
        if (! empty($tags)) {
            $this->tagResource($dist['ARN'], $tags);
        }

        return $ifMatch;
    }

    /**
     * Add tags to resource.
     *
     * @param   string  $arn
     * @param   array   $tags
     *
     * @return  void
     */
    public function tagResource($arn, $tags)
    {
        if (empty($arn)) {
            $this->exception("Invalid resource ARN.");
        }

        $params = [
            'Resource' => $arn,
            'Tags' => [
                'Items' => $this->prepareTagsArray($tags),
            ]
        ];

        $this->sendRequest('tagResource', $params);
    }

    /**
     * Deletes the specified distribution or fails if not totally disabled (enabled or
     * in progress of disabling).
     *
     * @param   string  $distributionId
     *
     * @return  void
     */
    public function deleteDistribution($distributionId)
    {
        // Get distribution details
        $config = $this->getDistributionOrFail($distributionId);
        $distConfig = $config['DistributionConfig'];
        $ifMatch = $config['IfMatch'];

        // Check current state and disable if enabled
        if ($distConfig['Enabled']) {
            $this->exception("The distribution is not yet disabled.");
        }

        // Delete the distribution
        $params = [
            'Id' => $distributionId,
            'IfMatch' => $ifMatch
        ];

        $this->sendRequest('DeleteDistribution', $params);
    }

    /**
     * Create a new invalidation.
     *
     * @param   string  $distributionId
     * @param   array   $objectPaths
     *
     * @return  void
     */
    public function createInvalidation($distributionId, $objectPaths)
    {
        if (empty($objectPaths)) {
            $this->exception("Object path array is empty.");
        }

        $params = [
            'DistributionId' => $distributionId,
            'InvalidationBatch' => [
                'CallerReference' => time(),
                'Paths' => [
                    'Items' => $objectPaths,
                    'Quantity' => count($objectPaths)
                ],
            ],
        ];

        $this->sendRequest('createInvalidation', $params);
    }

    /**
     * Creates default cache behavior.
     *
     * @param   array  $config
     *
     * @return  array
     */
    public function createCacheBehaviorConfig($config)
    {
        $default = [
            'AllowedMethods' => [
                'Items' => [ "GET", "HEAD" ],
                'Quantity' => 2,
                'CachedMethods' => [
                    'Items' => [ "GET", "HEAD" ],
                    'Quantity' => 2,
                ]
            ],
            'DefaultTTL' => 86400,
            'MaxTTL' => 31536000,
            'MinTTL' => 0,
            'ViewerProtocolPolicy' => 'redirect-to-https',
            'SmoothStreaming' => false,
            'Compress' => true,
            'PathPattern' => '*',
            'ForwardedValues' => [
                 'Cookies' => [
                    'Forward' => 'none'
                 ],
                 'Headers' => [
                    'Quantity' => 0,
                 ],
                 'QueryString' => true,
                 'QueryStringCacheKeys' => [
                    'Quantity' => 0,
                 ]
            ],
            'TargetOriginId' => '', // REQUIRED
            'TrustedSigners' => [
                'Enabled' => false,
                'Quantity' => 0,
            ]
        ];

        if (!empty($config['CachePolicyId'])) {
            unset($default['MaxTTL']);
            unset($default['MinTTL']);
            unset($default['DefaultTTL']);
            unset($default['ForwardedValues']);
        }
        if (!empty($config['OriginRequestPolicyId'])) {
            unset($default['ForwardedValues']);
        }

        return array_merge($default, $config);
    }

    /**
     * Returns the default viewer certificate configuration.
     *
     * @param   string  $certificateArn
     *
     * @return  array
     */
    public function createDefaultViewerCertificate($certificateArn)
    {
        return [
            "ACMCertificateArn" => $certificateArn,
            "SSLSupportMethod" => "sni-only",
            "CertificateSource" => "acm",
            "MinimumProtocolVersion" => "TLSv1.2_2021",
            "Certificate" => $certificateArn
        ];
    }

    /**
     * Returns the default custom origin configuration.
     *
     * @param   string  $originId
     * @param   string  $domainName
     * @param   string  $domainPath
     * @param   string  $originPolicy
     *
     * @return  array
     */
    public function createDefaultCustomOriginConfig($originId, $domainName, $domainPath = "", $originPolicy = null)
    {
        return [
            "CustomHeaders" => [
                "Quantity" => 0
            ],
            "CustomOriginConfig" => [
                "OriginReadTimeout" => 30,
                "OriginSslProtocols" => [
                    "Items" => [
                        "TLSv1",
                        "TLSv1.1",
                        "TLSv1.2"
                    ],
                    "Quantity" => 3
                ],
                "OriginKeepaliveTimeout" => 5,
                "HTTPSPort" => 443,
                "HTTPPort" => 80,
                "OriginProtocolPolicy" => $originPolicy ?? 'match-viewer'
            ],
            "OriginPath" => $domainPath,
            "DomainName" => $domainName,
            "Id" => $originId
        ];
    }

    /**
     * Returns the default S3 origin configuration.
     *
     * @param   string  $domainName
     * @param   string  $domainPath
     *
     * @return  array
     */
    public function createDefaultS3OriginConfig($originId, $bucketName, $bucketPath = "")
    {
        return [
            "CustomHeaders" => [
                "Quantity" => 0
            ],
            "S3OriginConfig" => [
                "OriginAccessIdentity" => "",
            ],
            "OriginPath" => $bucketPath,
            "DomainName" => sprintf("%s.s3.amazonaws.com", $bucketName),
            "Id" => $originId
        ];
    }

    /**
     * Returns the default S3 origin configuration.
     *
     * @param   string  $domainName
     * @param   string  $domainPath
     *
     * @return  array
     */
    public function createDefaultOriginGroupConfig(string $groupId, array $members, ?array $failoverCriteria = [])
    {
        // Add default criteria if none specified
        if (empty($failoverCriteria)) {
            $failoverCriteria = [ 403, 404, 500, 502, 503, 504 ];
        }

        // Build members in the expected format
        $members = array_map(function ($originId) {
            return [ 'OriginId' => $originId ];
        }, $members);

        return [
            'Id' => $groupId,
            'FailoverCriteria' => [
                'StatusCodes' => $this->convertToConfigItems($failoverCriteria)
            ],
            'Members' => $this->convertToConfigItems($members)
        ];
    }
}
