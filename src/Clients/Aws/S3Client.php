<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\CommandPool;
use Aws\Result;
use Aws\S3\Exception\S3Exception;
use NizarBlond\LaravelPlus\Support\Str;
use NizarBlond\LaravelPlus\Support\File;

class S3Client extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-s3-2006-03-01.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2006-03-01';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'S3';

    /**
     * Constructor.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\S3\S3Client',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Creates a new bucket.
     *
     * @param   string  $name
     * @param   string  $acl
     * @param   string  $region
     *
     * @return  string
     */
    public function createBucket($name, $acl = "private", $region = null)
    {
        if (! in_array($acl, [ "private", "public-read", "public-read-write", "authenticated-read" ])) {
            $this->exception("Invalid ACL.");
        }

        $params = [
            'Bucket' => $name, // REQUIRED
            'ACL' => $acl,
            'CreateBucketConfiguration' => [
                'LocationConstraint' => $region ?? $this->awsRegion,
            ],
        ];

        return $this->sendRequest('createBucket', $params)->get('Location');
    }

    /**
     * Checks whether the specified bucket exists.
     *
     * @param   string  $name
     *
     * @return  boolean
     */
    public function bucketExists($name)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
        ];

        try {
            $this->sendRequest('headBucket', $params);
             return true;
        } catch (S3Exception $e) {
            return false;
        }
    }

    /**
     * Deletes an existing bucket.
     *
     * @param   string  $name
     * @param   bool    $force
     *
     * @return  void
     */
    public function deleteBucket($name, $force = true)
    {
        if (! $this->bucketExists($name)) {
            $this->log("The specified bucket doesn't exist.");
            return;
        }

        if ($force) {
            $this->emptyBucket($name);
        }

        $params = [
            'Bucket' => $name, // REQUIRED
        ];

        $this->sendRequest('deleteBucket', $params);
    }

    /**
     * Empties an existing bucket.
     *
     * @param   string  $name
     *
     * @return  void
     */
    public function emptyBucket($name)
    {
        $objects = $this->getAllObjects($name, '/');
        if (empty($objects)) {
            return true;
        }

        return $this->deleteObjects($name, $objects);
    }

    /**
     * Create a pre-signed URL for the given S3 object.
     *
     * @param   string  $bucket
     * @param   string  $path
     *
     * @return  string
     */
    public function createPresignedUrl($bucket, $path, $expires = '+20 minutes', $versionId = null)
    {
        $params = [
            'Bucket' => $bucket,
            'Key' => $path
        ];

        if ($versionId) {
            $params['VersionId'] = $versionId;
        }

        $cmd = $this->awsClient->getCommand('GetObject', $params);

        $request = $this->awsClient->createPresignedRequest($cmd, $expires);
        $presignedUrl = (string)$request->getUri();

        return $presignedUrl;
    }

    /**
     * Adds multiple objects to a bucket.
     *
     * @param   array   $objects
     * @param   array   $succeeded
     * @param   array   $failed
     *
     * @return  bool
     */
    public function putMultiObjects(array $objects, ?array &$succeeded = [], ?array &$failed = [])
    {
        $commands = [];
        $iterToKey = [];

        foreach ($objects as $object) {
            // Required fields
            $key = $object['Key'] ?? $this->exception("Upload path is missing.");
            $filePath = $object['File'] ?? $this->exception("Local file not found for {$object['Key']}.");
            $bucket = $object['Bucket'] ?? $this->exception("Bucket is missing for '$key'.");
            $body = File::read($object['File']) ?? $this->exception("Failed to read '$filePath' content.");
            // Optional fields
            $acl = $object['ACL'] ?? 'private';
            // Prepare args
            $commands[] = $this->awsClient->getCommand('PutObject', [
                'ACL' => $acl,
                'Bucket' => $bucket,
                'Key' => $key,
                'Body' => $body
            ]);
            $iterToKey[] = $key;
        }

        $results = CommandPool::batch($this->awsClient, $commands);

        $succeeded = [];
        $failed = [];
        foreach ($results as $iter => $result) {
            $s3Key = $iterToKey[$iter];
            if ($result instanceof S3Exception) {
                $failed[$s3Key] = $result;
            } else if ($result instanceof Result) {
                $succeeded[$s3Key] = $result;
            }
        }

        return empty($failed);
    }

    /**
     * Adds an object to a bucket.
     *
     * @param   string  $bucket
     * @param   string  $path
     * @param   mixed   $content
     * @param   array   $metadata
     * @param   array   $awsParams
     *
     * @return  array
     */
    public function putObject(
        $bucket,
        $path,
        $content,
        $metadata = [],
        $awsParams = []
    ) {
        $params = array_merge([
            'Bucket' => $bucket,
            'Key'    => $path,
            'Body'   => $content
        ], $awsParams);

        if (! empty($metadata)) {
            $params['Metadata'] = $metadata;
        }
        
        return $this->sendRequest('putObject', $params);
    }

    /**
     * Adds an object to a bucket.
     *
     * @param   string  $bucket
     * @param   string  $path
     * @param   string  $source
     * @param   array   $metadata
     * @param   array   $awsParams
     *
     * @return  array
     */
    public function putObjectFromSource(
        $bucket,
        $path,
        $source,
        $metadata = [],
        $awsParams = []
    ) {
        if (!File::exists($source)) {
            $this->exception("File $source does not exist on local disk.");
        }

        $params = array_merge([
            'Bucket'        => $bucket,
            'Key'           => $path,
            'SourceFile'    => $source
        ], $awsParams);

        if (! empty($metadata)) {
            $params['Metadata'] = $metadata;
        }

        return $this->sendRequest('putObject', $params);
    }

    /**
     * Adds an object to the bucket if no objects exists on S3 or if
     * the existing object is different.
     *
     * @param   string  $bucket
     * @param   string  $key
     *
     * @return  mixed
     */
    public function syncObjectFromSource($bucket, $path, $source, $metadata = [])
    {
        if (!File::exists($source)) {
            $this->exception("File $source does not exist on local disk.");
        }

        try {
            $objectHead = $this->headObject($bucket, $path);
            $objectExists = true;
        } catch (S3Exception $e) {
            if ($e->getAwsErrorCode() !== 'NotFound') {
                throw $e;
            }
            $objectHead = false;
            $objectExists = false;
        }

        // In case the object exists on S3 compare the size then if the
        // size is the same then compare Md5 if available.
        $currentMd5 = $metadata['md5'] ?? md5_file($source);
        if ($objectExists) {
            $objectSize = $objectHead->get('ContentLength');
            if (File::size($source) == $objectSize) {
                $objectMd5 = $objectHead->get('Metadata')['md5'] ?? null;
                if ($objectMd5 && $objectMd5 === $currentMd5) {
                    return false;
                }
            }
        }

        $metadata['md5'] = $currentMd5;
        return $this->putObjectFromSource($bucket, $path, $source, $metadata);
    }

    /**
     * Creates a copy of an object that is already stored in Amazon S3.
     *
     * @param   string  $srcBucket
     * @param   string  $srcKey
     * @param   string  $dstBucket
     * @param   string  $dstKey
     * @param   array   $awsParams
     *
     * @return  array
     */
    public function copyObject(
        $srcBucket,
        $srcKey,
        $dstBucket,
        $dstKey,
        $awsParams = []
    ) {
        $params = array_merge([
            'Bucket'        => $dstBucket,
            'Key'           => $dstKey,
            'CopySource'    => urlencode($srcBucket . '/' . $srcKey),
        ], $awsParams);
        
        return $this->sendRequest('copyObject', $params);
    }

    /**
     * Updates the metadata of an object by creating a new copy.
     *
     * @param   string  $bucket
     * @param   string  $key
     * @param   array   $metadata
     *
     * @return  array
     */
    public function replaceMetadata($bucket, $key, $metadata)
    {
        // Get the current version
        $versions = $this->listObjectVersions($bucket, $key);
        $currentVersionId = $versions[0]['VersionId'] ?? $this->exception('Object not found.');

        // Copy object to self
        $this->copyObject($bucket, $key, $bucket, $key, [
            'Metadata' => $metadata,
            'MetadataDirective' => 'REPLACE'
        ]);

        // Delete the previous version
        $this->deleteObject($bucket, $key, $currentVersionId);
    }

    /**
     * Returns only top level folders of bucket.
     *
     * @param   string  $bucket
     * @param   string  $prefix
     * @param   string  $delimiter
     *
     * @return  array
     */
    public function getTopLevelFolders($bucket, $prefix = '', $delimiter = '/')
    {
        $params = [
            'Bucket' => $bucket,
            'Delimiter' => $delimiter,
            'Prefix' => $prefix
        ];
        $result = $this->sendRequest('ListObjectsV2', $params);

        $folders = [];
        foreach ($result['CommonPrefixes'] ?? [] as $object) {
            $folders[] = trim($object['Prefix'], '/');
        }

        return $folders;
    }

    /**
     * Returns some or all objects metadata in subset of a bucket.
     *
     * @param   string  $bucket
     * @param   string  $path
     * @param   int     $maxKeys
     * @param   bool    $recursive
     *
     * @return  array
     */
    public function getAllObjects(
        $bucket,
        $path,
        $maxKeys = 1000,
        $recursive = true
    ) {
        if ($maxKeys > 1000 || ! is_int($maxKeys)) {
            $this->exception("Invalid max keys (max is 1000).");
        }

        $returnResult   = [];
        $contToken      = null;

        while (true) {
            // Set request params
            $params = [
                'Bucket'    => $bucket,
                'Prefix'    => ltrim($path, '/'),
                'MaxKeys'   => $maxKeys
            ];

            // Check if continuation token is available
            if (! empty($contToken)) {
                $params['ContinuationToken'] = $contToken;
            }

            // Try to get first MaxKey objects
            $result = $this->sendRequest('ListObjectsV2', $params);

            // Check if we returned all results
            $contents = $result->get('Contents');
            if (empty($contents)) {
                return $returnResult;
            }

            // Add objects to result
            foreach ($contents as $object) {
                $returnResult[$object['Key']] = [
                    "Key"           => $object['Key'],
                    "LastModified"  => $object['LastModified']->getTimestamp(),
                    "ETag"          => $object['ETag'],
                    "Size"          => $object['Size'],
                ];
            }

            // Check if there are results left to be returned
            if (! $result['IsTruncated'] || ! $recursive) {
                return $returnResult;
            }

            $contToken = $result['NextContinuationToken'];
        }
        
        $this->exception("Oops! Something must have been wrong in order to get here!");
    }

    /**
     * Deletes an S3 object and all its versions.
     *
     * @param string $s3Key
     * @return array
     */
    public function deleteObjectPermanently($bucket, $s3Key, $includeDeleteMarkers = true)
    {
        $versions = $this->listObjectVersions($bucket, $s3Key, false, $includeDeleteMarkers);
        foreach ($versions ?? [] as $data) {
            $this->deleteObject($bucket, $data['Key'], $data['VersionId']);
        }
        return $versions;
    }

    /**
     * Lists all the attachment versions.
     *
     * @param string $bucket
     * @param string $s3Key
     * @param bool $includeMeta
     * @param bool $includeDeleteMarkers
     * @return array
     */
    public function listObjectVersions(
        $bucket,
        $s3Key,
        $includeMeta = false,
        $includeDeleteMarkers = false
    ) {
        $objectMeta = $this->sendRequest('listObjectVersions', [
            'Bucket'    => $bucket,
            'Prefix'    => $s3Key,
            'MaxKeys'   => 1000
        ]);

        // Filter precise S3 key
        $versions = array_filter($objectMeta['Versions'] ?? [], function ($version) use ($s3Key) {
            return $version['Key'] === $s3Key;
        });

        if ($includeMeta) {
            foreach ($versions ?? [] as $i => $version) {
                $versionHead = $this->headObject($bucket, $s3Key, $version['VersionId']);
                $versions[$i]['Metadata'] = $versionHead->get('Metadata');
            }
        }

        if ($includeDeleteMarkers) {
            return array_merge($versions ?? [], $objectMeta['DeleteMarkers'] ?? []);
        }

        return $versions;
    }

    /**
     * Retrieves metadata from an object without returning the object itself.
     *
     * @param   string  $bucket
     * @param   string  $key
     * @param   string  $versionId
     *
     * @return  mixed
     */
    public function headObject($bucket, $key, $versionId = null)
    {
        return $this->sendRequest('headObject', [
            'Bucket'    => $bucket,
            'Key'       => $key,
            'VersionId' => $versionId,
        ]);
    }

    /**
     * Retrieves objects from Amazon S3.
     *
     * @param   string  $bucket
     * @param   string  $key
     *
     * @return  mixed
     */
    public function getObject($bucket, $key, $versionId = null)
    {
        $params = [
            'Bucket'    => $bucket,
            'Key'       => $key,
        ];

        if (!empty($versionId)) {
            $params['VersionId'] = $versionId;
        }

        $object = $this->sendRequest('getObject', $params);

        return $object['Body'];
    }

    /**
     * Rollbacks object from Amazon S3.
     *
     * @param   string  $bucket
     * @param   string  $key
     * @param   string  $versionId
     *
     * @return  array
     */
    public function rollbackObjectVersion($bucket, $key, $versionId)
    {
        $content = $this->getObject($bucket, $key, $versionId);
        return $this->putObject($bucket, $key, $content);
    }

    /**
     * Deletes an object from a bucket.
     *
     * @param   string  $bucket
     * @param   string  $key
     * @param   string  $versionId
     *
     * @return  void
     */
    public function deleteObject($bucket, $key, $versionId = null)
    {
        $params = [
            'Bucket'    => $bucket,
            'Key'       => $key,
            'VersionId' => $versionId
        ];
        
        $this->sendRequest('deleteObject', $params);
    }

    /**
     * Deletes multiple objects from a bucket using a single HTTP request.
     *
     * @param   string  $bucket
     * @param   array   $objects
     *
     * @return  void
     */
    public function deleteObjects($bucket, $objects)
    {
        if (count($objects) > 1000) {
            $this->exception("You may specify up to 1000 keys only.");
        }

        $params = [
            'Bucket' => $bucket,
            'Delete' => [
                'Objects' => [],
            ],
        ];

        foreach ($objects ?? [] as $object) {
            $params['Delete']['Objects'][] = [
                'Key' => $object['Key']
            ];
        }
        
        $this->sendRequest('deleteObjects', $params);
    }

    /**
     * Deletes all objects in bucket's path.
     *
     * @param   string  $bucket
     * @param   string  $path
     *
     * @return  void
     */
    public function deleteAllObjects($bucket, $path)
    {
        $objects = $this->getAllObjects($bucket, $path);
        if (empty($objects)) {
            return;
        }

        $chunks = array_chunk($objects, 1000, true /* preserve keys */);
        foreach ($chunks as $chunk) {
            $this->deleteObjects($bucket, $chunk);
        }
    }

    /**
     * Returns the policy of a specified bucket.
     *
     * @param   string  $name
     * @param   string  $owner
     *
     * @return  string
     */
    public function getBucketPolicy($name, $owner = null)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
        ];
        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        return $this->sendRequest('getBucketPolicy', $params)->get('Policy');
    }

    /**
     * Applies an Amazon S3 bucket policy to an Amazon S3 bucket.
     *
     * @param   string  $name
     * @param   string  $policy
     * @param   string  $owner
     *
     * @return  void
     */
    public function putBucketPolicy($name, $policy, $owner = null)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
            'Policy' => $policy
        ];
        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        $this->sendRequest('putBucketPolicy', $params);
    }

    /**
     * Returns the website configuration for a bucket.
     *
     * @param   string  $name
     * @param   string  $owner
     *
     * @return  string
     */
    public function getBucketWebsite($name, $owner = null)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
        ];
        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        return $this->sendRequest('getBucketWebsite', $params);
    }

    /**
     * Sets the configuration of the website that is specified in the website subresource.
     *
     * @param   string  $name
     * @param   string  $config
     * @param   string  $owner
     *
     * @return  string
     */
    public function putBucketWebsite($name, $config = [], $owner = null)
    {
        if (empty($config['ErrorDocument']) || empty($config['ErrorDocument']['Key'])) {
            $config['ErrorDocument']['Key'] = 'error.html';
        }
        if (empty($config['IndexDocument']) || empty($config['IndexDocument']['Suffix'])) {
            $config['IndexDocument']['Suffix'] = 'index.html';
        }

        $params = [
            'Bucket' => $name, // REQUIRED
            'WebsiteConfiguration' => $config
        ];

        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        return $this->sendRequest('putBucketWebsite', $params);
    }

    /**
     * Retrieves the PublicAccessBlock configuration for an Amazon S3 bucket.
     *
     * @param   string  $name
     *
     * @return  string
     */
    public function getPublicAccessBlock($name, $owner = null)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
        ];
        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        return $this->sendRequest('getPublicAccessBlock', $params)->get('PublicAccessBlockConfiguration');
    }

    /**
     * Sets the PublicAccessBlock configuration for an Amazon S3 bucket.
     *
     * @param   string  $name
     * @param   string  $owner
     *
     * @return  string
     */
    public function putPublicAccessBlock($name, $config, $owner = null)
    {
        $params = [
            'Bucket' => $name, // REQUIRED
            'PublicAccessBlockConfiguration' => $config
        ];
        if ($owner) {
            $params['ExpectedBucketOwner'] = $owner;
        }

        return $this->sendRequest('putPublicAccessBlock', $params);
    }

    /**
     * Puts bucket notification config for Lambda.
     * See: https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-s3-2006-03-01.html#putbucketnotificationconfiguration
     *
     * @param   string  $bucket
     * @param   string  $lambdaFuncArn
     * @param   array   $events
     *
     * @return  void
     */
    public function putLambdaBucketNotificationConfiguration(
        $bucket,
        $lambdaFuncArn,
        $events = []
    ) {
        $params = [
            'Bucket' => $bucket,
            'NotificationConfiguration' => [
                'LambdaFunctionConfigurations' => [
                    [
                        'LambdaFunctionArn' => $lambdaFuncArn,
                        'Events' => $events
                    ]
                ],
            ],
        ];

        return $this->sendRequest('putBucketNotificationConfiguration', $params);
    }

    /**
     * Set the ACL parameters for a bucket.
     * See: https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-s3-2006-03-01.html#putbucketacl
     *
     * @param   array  $config
     *
     * @return  void
     */
    public function putBucketAcl($bucket, $config)
    {
        $config['Bucket'] = $bucket;
        return $this->sendRequest('putBucketAcl', $config);
    }

    /**
     * Set the logging parameters for a bucket and to specify permissions
     * for who can view and modify the logging parameters.
     * See: https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-s3-2006-03-01.html#putbucketlogging
     *
     * @param   string  $origin
     * @param   string  $target
     * @param   string  $prefix
     *
     * @return  void
     */
    public function putBucketLogging($origin, $target, $prefix = 'logs/')
    {
        $params = [
            'Bucket' => $origin,
            'BucketLoggingStatus' => [
                'LoggingEnabled' => [
                    'TargetBucket' => $target,
                    'TargetPrefix' => $prefix,
                ],
            ]
        ];

        return $this->sendRequest('putBucketLogging', $params);
    }
}
