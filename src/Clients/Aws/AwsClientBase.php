<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use NizarBlond\AwsBuilder\Models\ActivityLog;
use NizarBlond\LaravelPlus\Traits\InstanceHelpers;
use Exception;

abstract class AwsClientBase
{
    use InstanceHelpers;

    /**
     * The AWS client.
     *
     * @var mixed
     */
    protected $awsClient = null;

    /**
     * The AWS Service.
     *
     * @var string
     */
    protected $awsService = 'AWS';

    /**
     * The AWS region.
     *
     * @var string
     */
    protected $awsRegion;

    /**
     * The template being processed.
     *
     * @var string
     */
    protected $templateName;

    /**
     * Whether to log success.
     *
     * @var boolean
     */
    private $suppressSuccessLog = false;

    /**
     * Whether to log success.
     *
     * @var boolean
     */
    public $logSuccess;

    /**
     * Whether to log failure.
     *
     * @var boolean
     */
    public $logFailure;

    /**
     * Constructor.
     *
     * @param string    $clientClass    The AWS client class.
     * @param string    $awsService     The AWS service name used for logging.
     * @param string    $sdkVersion     The AWS Client's PHP SDK version.
     * @param string    $awsRegion      The AWS region.
     * @param string    $templateName   The template being processed.
     */
    public function __construct(
        $clientClass,
        string $awsService,
        string $sdkVersion,
        string $awsRegion = null,
        string $templateName = null
    ) {
        $this->awsRegion = empty($awsRegion) ? config('aws-builder.aws.region') : $awsRegion;
        
        $args = [
            'version' => $sdkVersion,
            'region'  => $this->awsRegion
        ];

        if (! empty(config('aws-builder.aws.access_key_id'))) {
            $args['credentials'] = [
                'key'       => config('aws-builder.aws.access_key_id'),
                'secret'    => config('aws-builder.aws.secret_access_key'),
            ];
        }
        
        $this->logSuccess = config('aws-builder.clients.aws.log_success');
        $this->logFailure = config('aws-builder.clients.aws.log_failure');
        $this->templateName = $templateName;
        $this->awsClient  = new $clientClass($args);
        $this->awsService = $awsService;
    }

    /**
     * Determines whether to log success.
     *
     * @param   bool    $active
     * @return  void
     */
    public function setLogSuccess(bool $active)
    {
        $this->logSuccess = $active;
    }

    /**
     * Determines whether to log failure.
     *
     * @param   bool    $active
     * @return  void
     */
    public function setLogFailure(bool $active)
    {
        $this->logFailure = $active;
    }

    /**
     * Sends AWS request.
     *
     * @param   string  $apiFunction
     * @param   array   $parameters
     * @return  mixed
     */
    public function sendRequest($apiFunction, $parameters)
    {
        $requestId = sprintf("%s/%s", $this->awsService, $apiFunction);

        $this->log("$requestId: sending request...", $parameters);

        $startTime = time();
        $result = null;

        try {
            if ($apiFunction === 'getIterator') {
                $result = $this->awsClient->getIterator($apiFunction, $parameters);
            } else {
                $result = $this->awsClient->$apiFunction($parameters);
            }

            $duration = time() - $startTime;

            $logResult = $this->suppressSuccessLog
                            ? [ 'Notice' => 'Log suppressed by user' ]
                            : (array) $result;

            $this->recordActivity($apiFunction, $parameters, $logResult, $duration);

            $this->log("$requestId: request has succeeded.");

            return $result;
        } catch (Exception $e) {
            $duration = time() - $startTime;

            $this->recordFailedActivity($apiFunction, $parameters, $e, $duration);

            throw $e;
        }
    }
    
    /**
     * Records activity.
     *
     * @param string    $function   The AWS function name.
     * @param array     $input      The function input.
     * @param array     $output     The function output.
     * @param string    $duration   The duration in seconds. Optional.
     * @return ActivityLog          The created activity log instance.
     */
    protected function recordActivity($function, $input, $output, $duration = null)
    {
        if (!$this->logSuccess) {
            return;
        }

        return ActivityLog::record(
            $this->awsService,
            $function,
            $input,
            $output,
            $duration,
            null, // error
            -1, // error level
            null, // error stack
            $this->templateName
        );
    }
    
    /**
     * Records a failed activity to the database.
     *
     * @param string    $function   The AWS function name.
     * @param array     $input      The function input..
     * @param string    $e          The activity exception.
     * @param string    $duration   The duration in seconds. Optional.
     * @return ActivityLog          The created activity log instance.
     */
    protected function recordFailedActivity($function, $input, $e, $duration = null)
    {
        if (!$this->logFailure) {
            return;
        }

        return ActivityLog::recordFailure(
            $this->awsService,
            $function,
            $input,
            $e,
            $duration,
            null, // output
            1, // error level
            $this->templateName
        );
    }

    /**
     * Prepares the tags input as per AWS API requirements.
     *
     * @param   array  $tags    The <key,value> tags array we want to convert.
     *
     * @return  array
     */
    public function prepareTagsArray(array $tags)
    {
        if (empty($tags)) {
            return [];
        }

        return array_map(function ($k, $v) {
            return [
                'Key' => $k,
                'Value' => $v
            ];
        }, array_keys($tags), $tags);
    }

    /**
     * Converts values to configurable items.
     *
     * @param   array  $values
     *
     * @return  array
     */
    public function convertToConfigItems(array $values)
    {
        return [
            'Quantity' => count($values),
            'Items' => $values
        ];
    }

    /**
     * Indexes aws tags structure by key value.
     *
     * @param   array  $awsTags
     *
     * @return  array
     */
    public function indexTagsByKey($awsTags, $allowEmpty = true)
    {
        if (empty($awsTags)) {
            return [];
        }
        
        $result = [];
        foreach ($awsTags as $tag) {
            if (empty($tag['Key'])) {
                $this->exception("Invalid tag key.");
            }
            if (!$allowEmpty && empty($tag['Value'])) {
                $this->exception("Empty tag value found for '{$tag['Key']}' key.");
            }
            $result[$tag['Key']] = $tag['Value'];
        }

        return $result;
    }
    
    /**
     * Suppresses logging on successful operations.
     *
     * @return void
     */
    public function suppressLogOnSuccess()
    {
        $this->suppressSuccessLog = true;
    }

    /**
     * Unsuppress logging on successful operations.
     *
     * @return void
     */
    public function unsuppressLogOnSuccess()
    {
        $this->suppressSuccessLog = false;
    }
}
