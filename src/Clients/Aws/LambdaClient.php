<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;

class LambdaClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-rds-2014-09-01.html
     *
     * References:
     * https://docs.aws.amazon.com/lambda/latest/dg/access-control-identity-based.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = 'latest';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'Lambda';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\Lambda\LambdaClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    public function createFunctionFromAwsParams($config)
    {
        return $this->sendRequest('createFunction', $config);
    }

    public function addPermissionFromAwsParams($config)
    {
        return $this->sendRequest('addPermission', $config);
    }

    public function deleteFunction($functionName)
    {
        return $this->sendRequest('deleteFunction', [
            'FunctionName' => $functionName
        ]);
    }

    public function listFunctionEventInvokeConfigs($functionName)
    {
        return $this->sendRequest('listFunctionEventInvokeConfigs', [
            'FunctionName' => $functionName
        ]);
    }

    public function listEventSourceMappings($functionName)
    {
        return $this->sendRequest('listEventSourceMappings', [
            'FunctionName' => $functionName
        ]);
    }

    public function removePermission($functionName, $statementId)
    {
        $params = [
            'FunctionName' => $functionName,
            'StatementId' => $statementId
        ];

        return $this->sendRequest('removePermission', $params);
    }
}
