<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;
use Aws\Iam\Exception\IamException;

class IamClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2010-05-08';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'IAM';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\Iam\IamClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    public function createInstanceProfile(
        $instanceProfileName,
        $path = '/',
        $roleName = null
    ) {
        $params = [
            'InstanceProfileName' => $instanceProfileName,
            'Path' => $path
        ];

        $profile = $this->sendRequest('createInstanceProfile', $params)->get('InstanceProfile');

        if (! empty($roleName)) {
            $this->sendRequest('addRoleToInstanceProfile', [
                'InstanceProfileName' => $instanceProfileName,
                'RoleName' => $roleName,
            ]);
        }

        return $profile;
    }

    public function deleteInstanceProfile($instanceProfileName)
    {
        $params = [
            'InstanceProfileName' => $instanceProfileName
        ];

        return $this->sendRequest('deleteInstanceProfile', $params);
    }

    public function createRole(
        $roleName,
        $assumeRolePolicyDocument,
        $path = '/'
    ) {
        $params = [
            'AssumeRolePolicyDocument' => $assumeRolePolicyDocument,
            'RoleName' => $roleName,
            'Path' => $path
        ];

        return $this->sendRequest('createRole', $params)->get('Role');
    }

    public function deleteRole($roleName)
    {
        $params = [
            'RoleName' => $roleName
        ];

        // Delete role policies
        $policies = $this->listRolePolicies($roleName);
        foreach ($policies as $policyName) {
            $this->deleteRolePolicy($roleName, $policyName);
        }

        // Delete role from instance profiles
        $instanceProfiles = $this->listInstanceProfilesForRole($roleName);
        foreach ($instanceProfiles ?? [] as $profile) {
            $this->removeRoleFromInstanceProfile($roleName, $profile['InstanceProfileName']);
        }

        return $this->sendRequest('deleteRole', $params);
    }

    public function listInstanceProfilesForRole($roleName)
    {
        $params = [
            // 'Marker' => '<string>',
            // 'MaxItems' => <integer>,
            'RoleName' => $roleName, // REQUIRED
        ];

        return $this->sendRequest('listInstanceProfilesForRole', $params)->get('InstanceProfiles');
    }

    public function listRolePolicies($roleName)
    {
        $params = [
            // 'Marker' => '<string>',
            // 'MaxItems' => <integer>,
            'RoleName' => $roleName, // REQUIRED
        ];

        return $this->sendRequest('listRolePolicies', $params)->get('PolicyNames');
    }

    public function putRolePolicy($roleName, $policyName, $policyDocument)
    {
        $params = [
            'RoleName'          => $roleName,
            'PolicyName'        => $policyName,
            'PolicyDocument'    => $policyDocument,
        ];
        
        return $this->sendRequest('putRolePolicy', $params);
    }

    public function removeRoleFromInstanceProfile($roleName, $instanceProfileName)
    {
        $params = [
            'RoleName'              => $roleName,
            'InstanceProfileName'   => $instanceProfileName,
        ];
        
        return $this->sendRequest('removeRoleFromInstanceProfile', $params);
    }

    public function deleteRolePolicy($roleName, $policyName)
    {
        $params = [
            'RoleName'          => $roleName,
            'PolicyName'        => $policyName,
        ];
        
        return $this->sendRequest('deleteRolePolicy', $params);
    }

    public function createGroup($groupName)
    {
        // Check if group exists
        $group = $this->getGroup($groupName);
        if (! empty($group)) {
            $this->exception("IAM group already exists.");
        }

        $params = [
            'GroupName' => $groupName, // REQUIRED
        ];

        return $this->sendRequest('createGroup', $params)->get('Group');
    }

    public function getGroup($groupName)
    {
        $params = [
            'GroupName' => $groupName, // REQUIRED
        ];

        try {
            return $this->sendRequest('getGroup', $params);
        } catch (IamException $e) {
            return null;
        } catch (AwsException $e) {
            return null;
        }
    }

    public function deleteGroup($groupName)
    {
        // Get group users
        $users = $this->getGroup($groupName)->get('Users');

        // Remove all users from group
        foreach ($users as $user) {
            $this->removeUserFromGroup($user['UserName'], $groupName);
        }

        // Get group policies
        $policies = $this->listGroupPolicies($groupName);

        // Remove all group policies
        foreach ($policies as $policyName) {
            $this->deleteGroupPolicy($groupName, $policyName);
        }

        // Delete group
        $params = [
            'GroupName' => $groupName, // REQUIRED
            // 'Path' => '<string>',
        ];

        return $this->sendRequest('deleteGroup', $params)->get('Group');
    }

    public function listGroupPolicies($groupName)
    {
        $params = [
            'GroupName' => $groupName, // REQUIRED
            // 'Marker' => '<string>',
            // 'MaxItems' => <integer>,
        ];

        return $this->sendRequest('listGroupPolicies', $params)->get('PolicyNames');
    }

    public function deleteGroupPolicy($groupName, $policyName)
    {
        $params = [
            'GroupName' => $groupName, // REQUIRED
            'PolicyName' => $policyName, // REQUIRED
        ];

        return $this->sendRequest('deleteGroupPolicy', $params);
    }

    /**
     * Creates a new IAM user.
     *
     * @param   string  $username       The user name.
     * @param   string  $group          The group to add user to. Optional.
     * @param   string  $createKeys     Whether to create user keys automatically. Optional.
     * @param   string  $createKeys     Whether to create user keys automatically. Optional.
     * @return  User|Exception
     */
    public function createUser($username, $group = null, $createKeys = true, $path = '/')
    {
        $returnResult = [];

        // CreateUser
        $params = [
            'Path' => $path,
            'UserName' => $username,
        ];

        // Check if user exists
        $user = $this->getUser($username);
        if (! empty($user)) {
            $this->exception("IAM user already exists.");
        }

        // Create user
        $user = $this->sendRequest('createUser', $params)->get('User');

        // Save user to result
        $returnResult['User'] = $user;

        // addUserToGroup
        if (! empty($group)) {
            $this->addUserToGroup($username, $group);
            $returnResult['Group'] = $group;
        }

        // CreateAccessKey
        if ($createKeys) {
            $returnResult['AccessKeys'] = $this->createAccessKey($username);
        }

        return $returnResult;
    }

    /**
     * Deletes an IAM User.
     *
     * @param   string  $username   The user name.
     * @return  array
     */
    public function deleteUser($username)
    {
        // Check if user exists
        $user = $this->getUser($username);
        if (empty($user)) {
            $this->log("IAM user doesn't exist.");
            return;
        }

        // Get user groups
        $groups = $this->listGroupsForUser($username);

        // Remove user from groups
        if (! empty($groups) && isset($groups['Groups'])) {
            foreach ($groups['Groups'] as $group) {
                $this->removeUserFromGroup($username, $group['GroupName']);
            }
        }

        // Get user's access keys
        $this->deleteAccessKeys($username);

        // Delete user policies
        $policies = $this->listUserPolicies($username);
        foreach ($policies as $policyName) {
            $this->deleteUserPolicy($username, $policyName);
        }
        
        // Delete user
        $params = [
            'UserName' => $username,
        ];

        return $this->sendRequest('deleteUser', $params);
    }

    /**
     * Lists the IAM users that have the specified path prefix.
     * If no path prefix is specified, the operation returns all
     * users in the AWS account.
     *
     * @param   string  $pathPrefix
     * @return  array
     */
    public function listUsers($pathPrefix = null)
    {
        $allUsers = [];
        $marker = null;
        $params = [];

        if (! empty($pathPrefix)) {
            $params['PathPrefix'] = $pathPrefix;
        }

        while (true) {
            // Add marker to parameters if exists
            if ($marker) {
                $params['Marker'] = $marker;
            }

            // Get max users possible (default is 100)
            $result = $this->sendRequest('listUsers', $params);

            // Add users to result
            $allUsers = array_merge($allUsers, $result->get('Users'));

            // Check if there are more results to fetch
            if ($result['IsTruncated'] === false) {
                break;
            }

            $marker = $result['Marker'];
        }

        return $allUsers;
    }

    /**
     * Retrieves information about the specified IAM user, including the user's
     * creation date, path, unique ID, and ARN.
     *
     * @param   string  $username   The user name.
     * @return  User|null|Exception
     */
    public function getUser($username)
    {
        $params = [
            'UserName' => $username,
        ];
        
        try {
            return $this->sendRequest('getUser', $params);
        } catch (IamException $e) {
            return null;
        } catch (AwsException $e) {
            return null;
        }
    }

    /**
     * Adds or updates an inline policy document that is embedded in the specified IAM user.
     *
     * @param   string  $username       The user name.
     * @param   string  $policyName     The policy name.
     * @param   string  $policyDocument The policy document.
     * @return  []|Exception
     */
    public function putUserPolicy($username, $policyName, $policyDocument)
    {
        $params = [
            'UserName'          => $username,
            'PolicyName'        => $policyName,
            'PolicyDocument'    => $policyDocument,
        ];
        
        return $this->sendRequest('putUserPolicy', $params);
    }

    /**
     * Deletes the specified inline policy that is embedded in the specified IAM user.
     *
     * @param   string  $username       The user name.
     * @param   string  $policyName     The policy name.
     * @return  []|Exception
     */
    public function deleteUserPolicy($username, $policyName)
    {
        $params = [
            'UserName'          => $username,
            'PolicyName'        => $policyName,
        ];
        
        return $this->sendRequest('deleteUserPolicy', $params);
    }

    /**
     * Lists the names of the inline policies embedded in the specified IAM user.
     *
     * @param   string  $username       The user name.
     * @return  []|Exception
     */
    public function listUserPolicies($username)
    {
        $params = [
            // 'Marker' => '<string>',
            // 'MaxItems' => <integer>,
            'UserName' => $username, // REQUIRED
        ];

        return $this->sendRequest('listUserPolicies', $params)->get('PolicyNames');
    }

    /**
     * Adds or updates an inline policy document that is embedded in the specified IAM group.
     *
     * @param   string  $group          The group name.
     * @param   string  $policyName     The policy name.
     * @param   string  $policyDocument The policy document.
     * @return  []|Exception
     */
    public function putGroupPolicy($group, $policyName, $policyDocument)
    {
        $params = [
            'GroupName'         => $group,
            'PolicyName'        => $policyName,
            'PolicyDocument'    => $policyDocument,
        ];

        return $this->sendRequest('putGroupPolicy', $params);
    }

    /**
     * Adds the specified user to the specified group.
     *
     * @param   string  $username
     * @param   string  $group
     * @param   string  $deleteUserIfFailed
     * @return  []|Exception
     */
    public function addUserToGroup($username, $group, $deleteUserIfFailed = true)
    {
        $params = [
            'GroupName' => $group,
            'UserName'  => $username,
        ];

        try {
            return $this->sendRequest('addUserToGroup', $params);
        } catch (AwsException $e) {
            $this->deleteUser($username);
            throw $e;
        }
    }

    /**
     * Removes the specified user from the specified group.
     *
     * @param   string  $username
     * @param   string  $group
     * @return  []|Exception
     */
    public function removeUserFromGroup($username, $group)
    {
        $params = [
            'GroupName' => $group,
            'UserName'  => $username,
        ];

        return $this->sendRequest('removeUserFromGroup', $params);
    }

    public function listGroupsForUser($username)
    {
        $params = [
            'UserName'  => $username,
            // 'MaxItems'  => 100,
            // 'Marker'    => '<string>',
        ];

        return $this->sendRequest('listGroupsForUser', $params);
    }

    /**
     * Creates a new AWS secret access key and corresponding AWS access
     * key ID for the specified user.
     *
     * @param   string  $username
     * @return  AccessKey
     */
    public function createAccessKey($username)
    {
        $params = [
            'UserName'  => $username,
        ];

        return $this->sendRequest('createAccessKey', $params)->get('AccessKey');
    }

    /**
     * Deletes the access key pair associated with the specified IAM user.
     *
     * @param   string  $username
     * @param   string  $accessKeyId
     * @return  []|Exception
     */
    public function deleteAccessKey($username, $accessKeyId)
    {
        $params = [
            'UserName'      => $username,
            'AccessKeyId'   => $accessKeyId,
        ];

        return $this->sendRequest('deleteAccessKey', $params);
    }


    /**
     * Deletes access keys for user.
     *
     * @param   string  $username   The user name.
     * @return  bool
     */
    public function deleteAccessKeys($username)
    {
        // Get user's access keys
        $keys = $this->listAccessKeys($username);

        // Remove user's access keys
        $deleted = false;
        foreach ($keys['AccessKeyMetadata'] ?? [] as $key) {
            $this->deleteAccessKey($username, $key['AccessKeyId']);
            $deleted = true;
        }

        return $deleted;
    }

    /**
     * Returns information about the access key IDs associated with
     * the specified IAM user.
     *
     * @param   string  $username
     * @return  array
     */
    public function listAccessKeys($username)
    {
        $params = [
            'UserName'  => $username,
        ];

        return $this->sendRequest('listAccessKeys', $params);
    }
}
