<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;
use Aws\Acm\Exception\AcmException;

class AcmClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-acm-2015-12-08.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2015-12-08';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'ACM';

    /**
     * The available certificate statuses.
     *
     * @var array
     */
    const CERTIFICATE_STATUSES = [
        'PENDING_VALIDATION',
        'ISSUED',
        'INACTIVE',
        'EXPIRED',
        'VALIDATION_TIMED_OUT',
        'REVOKED',
        'FAILED',
    ];

    /**
     * The available certificate validation methods.
     *
     * @var array
     */
    const VALIDATION_METHODS = [
        'DNS',
        'EMAIL',
    ];

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        $awsRegion = $awsRegion ?? config('aws-builder.aws.acm_region');

        parent::__construct(
            '\Aws\Acm\AcmClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Lists all available certificates.
     *
     * @param   array  $statuses
     *
     * @return  array
     */
    public function listCertificates($statuses = [])
    {
        $this->validateStatuses($statuses);

        $params = [];
        if (! empty($statuses)) {
            $params['CertificateStatuses'] = $statuses;
        }

        $response = $this->sendRequest('listCertificates', $params);
        $allCerts = $response->get('CertificateSummaryList');
        while (!empty($response['NextToken'])) {
            $params['NextToken'] = $response['NextToken'];
            $response = $this->sendRequest('listCertificates', $params);
            $allCerts = array_merge($allCerts, $response->get('CertificateSummaryList'));
        }

        return $allCerts;
    }

    /**
     * Describes certificate with the specified ARN.
     *
     * @param   string  $arn
     *
     * @return  array
     */
    public function describeCertificate($arn)
    {
        $params = [
            'CertificateArn' => $arn
        ];

        return $this->sendRequest('DescribeCertificate', $params)->get('Certificate');
    }

    /**
     * Describes certificate but waits until validation options are ready.
     *
     * @param   string      $arn                The certificate ARN.
     * @param   integer     $maxWaitUntilValid  The max wait time in minutes.
     *
     * @return  array
     */
    public function describeCertificateWithValidationOptiions($arn, $maxWaitUntilValid = 1)
    {
        $sleepTimeInSecs    = 15;
        $maxWaitInSecs      = $maxWaitUntilValid * 60;
        $sleepCount         = 0;

        while (true) {
            // Get certificate
            $cert = $this->describeCertificate($arn);

            // Check if status is pending validation
            if ($cert['Status'] !== 'PENDING_VALIDATION') {
                $this->log(sprintf(
                    "The certificate '%s' is not pending validation (status=%s).",
                    $arn,
                    $cert['Status']
                ));
                return $cert;
            }

            // Check if validation options exist
            $validationOptions = $cert['DomainValidationOptions'][0] ?? null;
            if (! empty($validationOptions['ResourceRecord'] ?? null)) {
                return $cert;
            }

            $this->log("Sleep #".($sleepCount+1)." for $sleepTimeInSecs seconds...");

            sleep($sleepTimeInSecs);

            $sleepCount++;

            if ($sleepCount * $sleepTimeInSecs > $maxWaitInSecs) {
                $this->exception("Max wait time reached ($maxWaitInSecs mins).");
            }
        }
    }

    /**
     * Returns certificate by the specified domain name.
     *
     * @param   string  $domainName
     *
     * @return  string
     */
    public function getCertificateByDomainName($domainName)
    {
        $certs = $this->listCertificates();
        foreach ($certs ?? [] as $cert) {
            if ($cert['DomainName'] === $domainName) {
                return $cert;
            }
        }

        return null;
    }

    /**
     * Resends certificate validation email.
     *
     * @param   array  $arn
     *
     * @return  array
     */
    public function resendValidationEmail($arn)
    {
        $cert = $this->describeCertificate($arn);

        $params = [
            'CertificateArn' => $arn, // REQUIRED
            'Domain' => $cert['DomainName'], // REQUIRED
            // 'ValidationDomain' => '', // REQUIRED
        ];

        $this->sendRequest('resendValidationEmail', $params);
    }

    /**
     * Requests a new certificate.
     *
     * @param   string  $domainName
     * @param   array   $validationMethods
     * @param   array   $ignoreIfExists
     *
     * @return  string
     */
    public function requestCertificate(
        $domainName,
        $validationMethod = 'DNS',
        $subjectAlternativeNames = [],
        $ignoreIfExists = false
    ) {
        if (! in_array($validationMethod, self::VALIDATION_METHODS)) {
            $this->exception("Invalid validation method.");
        }

        $cert = $this->getCertificateByDomainName($domainName);
        if (! empty($cert) && !$ignoreIfExists) {
            $this->log("A certificate with domain '$domainName' already exists.", $cert);
            // Check if still valid or delete otherwise
            if (isset($cert['Status']) && in_array($cert['Status'], ['VALIDATION_TIMED_OUT', 'EXPIRED'])) {
                $this->log("The certificate is invalid (status={$cert['Status']}.");
                $this->deleteCertificate($cert['CertificateArn']);
            } else {
                return $cert['CertificateArn'];
            }
        }

        $params = [
            'DomainName' => $domainName,
            'ValidationMethod' => $validationMethod,
            'SubjectAlternativeNames' => $subjectAlternativeNames
        ];

        return $this->sendRequest('requestCertificate', $params)->get('CertificateArn');
    }

    /**
     * Deletes the certificate with the specified ARN.
     *
     * @param   string  $arn
     *
     * @return  array
     */
    public function deleteCertificate($arn)
    {
        $params = [
            'CertificateArn' => $arn
        ];

        $this->sendRequest('deleteCertificate', $params);
    }

    /**
     * Adds tags the certificate with the specified ARN.
     *
     * @param   string  $arn
     * @param   array   $tags
     *
     * @return  array
     */
    public function addTagsToCertificate($arn, $tags)
    {
        if (empty($tags)) {
            $this->exception("Invalid tags array.");
        }

        $params = [
            'CertificateArn' => $arn,
            'Tags' => $this->prepareTagsArray($tags)
        ];

        $this->sendRequest('addTagsToCertificate', $params);
    }

    /**
     * Lists the tags that have been applied to the ACM certificate.
     *
     * @param   string  $arn
     *
     * @return  array
     */
    public function listTagsForCertificate($arn)
    {
        $params = [
            'CertificateArn' => $arn,
        ];

        $tags = $this->sendRequest('listTagsForCertificate', $params)->get('Tags');
        return $this->indexTagsByKey($tags);
    }

    /**
     * Validates certificate statuses input.
     *
     * @param   array  $statuses
     *
     * @return  void
     *
     * @throws  \Exception
     */
    private function validateStatuses($statuses = [])
    {
        if (! empty($statuses)) {
            foreach ($statuses as $status) {
                if (! in_array($status, self::CERTIFICATE_STATUSES)) {
                    $this->exception("Invalid certificate status '$status'.");
                }
            }
        }
    }
}
