<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use NizarBlond\LaravelPlus\Support\Str;

class Route53Client extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See https://docs.aws.amazon.com/aws-sdk-php/v3/api/class-Aws.Route53.Route53Client.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2013-04-01';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'Route53';

    /**
     * Constructor.
     *
     * @param string $awsRegion
     *
     * @return void
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\Route53\Route53Client',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Gets information about a specified hosted zone including the
     * four name servers assigned to the hosted zone.
     *
     * @param   string  $id The ID of the hosted zone that you want to get information about.
     *
     * @return  array
     */
    public function getHostedZone($id)
    {
        $params = [
            'Id' => $id
        ];

        return $this->sendRequest('GetHostedZone', $params);
    }

    /**
     * Creates a new public hosted zone, which you use to specify
     * how the Domain Name System (DNS) routes traffic on the
     * Internet for a domain, such as example.com, and its subdomains.
     *
     * @param   string  $name
     *
     * @return  array
     */
    public function createHostedZone($name)
    {
        $params = [
            /* A unique string that identifies the request and that allows failed CreateHostedZone requests to be retried without the risk of executing the operation twice. You must use a unique CallerReference string every time you submit a CreateHostedZone request. CallerReference can be any unique string, for example, a date/time stamp. */
            'CallerReference' => date(DATE_RFC2822), // REQUIRED
            'Name' => $name, // REQUIRED
        ];

        return $this->sendRequest('createHostedZone', $params)->get('HostedZone');
    }

    /**
     * Lists all hosted zones.
     *
     * @return  array
     */
    public function listHostedZones()
    {
        $params = [
            // MaxItems max value is 100, we set 1000 by AWS as of 12-05-2020 will
            // replace it back to 100, but it can be ignored as next marker is checked
            'MaxItems' => 1000,
        ];
        $nextMarker     = null;
        $returnResult   = [];

        while (true) {
            // Check if next marker is available
            if (! empty($nextMarker)) {
                $params['Marker'] = $nextMarker;
            }

            // Try to get first MaxItems objects
            $result = $this->sendRequest('listHostedZones', $params);

            // If hosted zones returned then add to result array
            $hostedZones = $result->get('HostedZones');
            if (! empty($hostedZones)) {
                $returnResult = array_merge($returnResult, $hostedZones);
            }

            // Check if there are results left to be returned
            if (! $result['IsTruncated'] || empty($hostedZones)) {
                return $returnResult;
            }

            // Set next marker
            $nextMarker = $result['NextMarker'];
        }

        $this->exception("Oops! Something must have been wrong in order to get here!");
    }

    /**
     * Retrieves a list of your hosted zones in lexicographic order.
     *
     * @param   string  $name
     *
     * @return  array
     */
    public function listHostedZonesByName($recursive = true)
    {
        $params = [];

        $result = $this->sendRequest('ListHostedZonesByName', $params);

        if ($result['IsTruncated']) {
            self::exception("There are more hosted zones. Please implement recursive.");
        }

        return $result->get("HostedZones");
    }

    /**
     * Retrieves the hosted zone by the specified host name.
     *
     * @param   string  $host
     *
     * @return  array|null
     */
    public function getHostedZoneByName($host)
    {
        $hostedZones = $this->listHostedZonesByName();
        foreach ($hostedZones ?? [] as $hz) {
            // Route53 saves the host with "." in the end
            if ($hz['Name'] == $host || rtrim($hz['Name'], ".") == $host) {
                return $hz;
            }
        }

        return null;
    }

    /**
     * Deletes a hosted zone.
     *
     * @param   string  $id
     *
     * @return  array
     */
    public function deleteHostedZone($id)
    {
        $params = [
            'Id' => $id
        ];

        return $this->sendRequest('deleteHostedZone', $params);
    }

    /**
     * Lists the resource record sets in the specified hosted zone.
     *
     * @param   string      $hostedZoneId
     *
     * @return  array
     */
    public function listResourceRecordSets($hostedZoneId)
    {
        if (empty($hostedZoneId)) {
            self::exception("Invalid Hosted Zone ID.");
        }

        $params = [
            'HostedZoneId' => $hostedZoneId,
            'MaxItems' => 1000,
        ];

        $sets = [];

        while (true) {
            $result = $this->sendRequest('listResourceRecordSets', $params);
            $sets = array_merge($sets, array_map(function ($set) {
                $set['Name'] = str_replace('\052', '*', $set['Name']); // Replace '\052' with '*'
                return $set;
            }, $result->get('ResourceRecordSets')));
            
            if (empty($result['IsTruncated'])) {
                break;
            }

            $params['StartRecordName'] = $result['NextRecordName'];
        }

        return $sets;
    }

    /**
     * Creates a resource record set.
     *
     * @param   string      $hostedZoneId
     * @param   string      $type
     * @param   string      $name
     * @param   array       $records
     * @param   string      $alias
     * @param   string      $comment
     * @param   int         $ttl
     *
     * @return  array
     */
    public function createResourceRecordSet(
        string $hostedZoneId,
        string $type,
        string $name,
        array $records,
        ?string $alias = null,
        string $comment = 'Created by AwsBuilder',
        ?int $ttl = null
    ) {
        return $this->changeResourceRecordSets(
            $hostedZoneId,
            'CREATE',
            $type,
            $name,
            $records,
            $alias,
            $comment,
            $ttl
        );
    }

    /**
     * Updates a resource record set.
     *
     * @param   string      $hostedZoneId
     * @param   string      $type
     * @param   string      $name
     * @param   array       $records
     * @param   string      $alias
     * @param   string      $comment
     * @param   int         $ttl
     *
     * @return  array
     */
    public function upsertResourceRecordSet(
        string $hostedZoneId,
        string $type,
        string $name,
        array $records,
        ?string $alias = null,
        string $comment = 'Updated by AwsBuilder',
        ?int $ttl = null
    ) {
        return $this->changeResourceRecordSets(
            $hostedZoneId,
            'UPSERT',
            $type,
            $name,
            $records,
            $alias,
            $comment,
            $ttl
        );
    }

    /**
     * Deletes a resource record set.
     *
     * @param   string      $hostedZoneId
     * @param   string      $type
     * @param   string      $name
     * @param   string      $comment
     * @param   string      $alias
     * @param   array       $records
     * @param   int         $ttl
     *
     * @return  array
     */
    public function deleteResourceRecordSet(
        string $hostedZoneId,
        string $type,
        string $name,
        array $records = [],
        ?string $alias = null,
        string $comment = 'Deleted by AwsBuilder',
        ?int $ttl = null
    ) {
        if (empty($records) && empty($alias)) {
            $records = $this->getResourceRecordSetValueByName($hostedZoneId, $name);
            if (empty($records)) {
                $this->log("Empty Route53 resource record set received.");
                return;
            }
        }

        return $this->changeResourceRecordSets(
            $hostedZoneId,
            'DELETE',
            $type,
            $name,
            $records,
            $alias,
            $comment,
            $ttl
        );
    }

    /**
     * Checks whether a record set exists.
     *
     * @param   string      $hostedZoneId
     * @param   string      $name
     *
     * @return  boolean
     */
    public function resourceRecordSetExists($hostedZoneId, $name)
    {
        return ! empty($this->getResourceRecordSetValueByName($hostedZoneId, $name));
    }

    /**
     * Creates, changes, or deletes a resource record set, which contains
     * authoritative DNS information for a specified domain name or subdomain name.
     *
     * @param   string      $hostedZoneId
     * @param   string      $action
     * @param   string      $type
     * @param   string      $name
     * @param   array       $records
     * @param   string      $alias
     * @param   string      $comment
     * @param   integer     $ttl
     *
     * @return  array
     */
    private function changeResourceRecordSets(
        string $hostedZoneId,
        string $action,
        string $type,
        string $name,
        array $records,
        ?string $alias,
        string $comment,
        ?int $ttl
    ) {
        $hostedZoneId = str_replace("/hostedzone/", "", $hostedZoneId);

        $params = [
            'ChangeBatch' => [
                'Changes' => [
                    $this->createRecordSetChangeAction(
                        $action,
                        $type,
                        $name,
                        $records,
                        $alias,
                        $ttl ?? 300
                    )
                ],
                'Comment' => $comment,
            ],
            'HostedZoneId' => $hostedZoneId,
        ];

        return $this->sendRequest('changeResourceRecordSets', $params)->get('ChangeInfo');
    }

    /**
     * Creates a change action for record set.
     *
     * @param   string      $action
     * @param   string      $type
     * @param   string      $name
     * @param   array       $records
     * @param   string      $alias
     * @param   integer     $ttl
     * @param   string      $hostedZoneId
     *
     * @return  array
     */
    private function createRecordSetChangeAction(
        string $action,
        string $type,
        string $name,
        array $records,
        ?string $alias,
        int $ttl
    ) {
        if (! in_array($action, [ 'CREATE', 'DELETE', 'UPSERT' ])) {
            self::exception("Invalid record set action.");
        }

        if ($action !== 'DELETE') {
            if ((empty($records) && empty($alias)) || (!empty($records) && !empty($alias))) {
                self::exception("Invalid combination of records and alias target.");
            }
        }

        $change = [
            'Action' => $action, // REQUIRED
            'ResourceRecordSet' => [ // REQUIRED
                'Name' => $name, // REQUIRED
                'Type' => $type // REQUIRED
            ],
        ];

        // If records exist then add them
        if (!empty($records)) {
            $change['ResourceRecordSet']['ResourceRecords'] = array_map(
                function ($r) {
                    return [ 'Value' => $r ];
                },
                $records
            );
            $change['ResourceRecordSet']['TTL'] = $ttl;
        }

        // Otherwise, check for Alias Target
        if (!empty($alias)) {
            $hostedZoneId = null;
            if (Str::contains($alias, 'cloudfront.net')) {
                // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-route53-2013-04-01.html#shape-aliastarget
                $hostedZoneId = 'Z2FDTNDATAQYW2';
            }

            $change['ResourceRecordSet']['AliasTarget'] = [
                'DNSName' => $alias,
                'EvaluateTargetHealth' => false,
                'HostedZoneId' => $hostedZoneId ?? $this->exception('Invalid hosted zone ID.')
            ];
        }

        return $change;
    }

    /**
     * Returns values of the specified resource record set.
     *
     * @param   string      $hostedZoneId
     * @param   string      $name
     *
     * @return  array
     */
    private function getResourceRecordSetValueByName($hostedZoneId, $name)
    {
        $sets = $this->listResourceRecordSets($hostedZoneId);
        foreach ($sets ?? [] as $set) {
            if ($set['Name'] == $name || rtrim($set['Name']) == $name) {
                return array_map(function ($r) {
                    return $r['Value'];
                }, $set['ResourceRecords']);
            }
        }

        return [];
    }
}
