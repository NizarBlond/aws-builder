<?php

namespace NizarBlond\AwsBuilder\Clients\Aws;

use Aws\Exception\AwsException;

class RdsClient extends AwsClientBase
{
    /**
     * The AWS SDK version.
     * See http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-rds-2014-09-01.html
     *
     * @var string
     */
    const AWS_SDK_VERSION = '2014-10-31';
    
    /**
     * The AWS Service.
     *
     * @var string
     */
    const AWS_SERVICE = 'RDS';

    /**
     * Constructor.
     *
     * @param string    $awsRegion     The AWS region.
     */
    public function __construct($templateName = null, $awsRegion = null)
    {
        parent::__construct(
            '\Aws\Rds\RdsClient',
            self::AWS_SERVICE,
            self::AWS_SDK_VERSION,
            $awsRegion,
            $templateName
        );
    }

    /**
     * Creates a DB instance.
     *
     * For full configs list see:
     * http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-rds-2014-09-01.html#createdbinstance
     *
     * @param   string  $dbInstanceId
     * @param   array   $config
     * @param   string  $dbName
     * @return  DBInstance
     */
    public function createDbInstance(
        $dbInstanceId,
        $config = [],
        $dbName = null,
        $securityGroupIds = [],
        $dbSubnetGroupName = null,
        $publiclyAccessible = true
    ) {
        if (is_string($securityGroupIds)) {
            $securityGroupIds = [ $securityGroupIds ];
        }

        $params = array_merge($config, [
            "DBInstanceIdentifier"  => $dbInstanceId,
            "DBName"                => $dbName,
            'VpcSecurityGroupIds'   => $securityGroupIds,
            "PubliclyAccessible"    => $publiclyAccessible
        ]);

        if (! empty($dbSubnetGroupName)) {
            $params['DBSubnetGroupName'] = $dbSubnetGroupName;
        }

        return $this->sendRequest('createDBInstance', $params)->get('DBInstance');
    }

    public function startDbInstance($dbInstanceId)
    {
        $params = [
            'DBInstanceIdentifier' => $dbInstanceId,
        ];

        return $this->sendRequest('startDBInstance', $params);
    }

    public function stopDbInstance($dbInstanceId)
    {
        $params = [
            'DBInstanceIdentifier' => $dbInstanceId,
        ];

        return $this->sendRequest('stopDBInstance', $params);
    }

    public function deleteDbInstance($dbInstanceId, $skipFinalSnapshot = true)
    {
        $params = [
            'DBInstanceIdentifier' => $dbInstanceId,
            'SkipFinalSnapshot' => $skipFinalSnapshot,
        ];

        return $this->sendRequest('deleteDBInstance', $params);
    }

    public function describeDbInstances()
    {
        return $this->sendRequest('describeDBInstances', [])->get('DBInstances');
    }

    public function describeDbInstance($dbInstanceId)
    {
        $params = [
            'DBInstanceIdentifier' => $dbInstanceId
        ];

        return $this->sendRequest('describeDBInstances', $params)->get('DBInstances')[0];
    }

    public function createDbSubnetGroup($groupName, $groupDesc, $subnetIds)
    {
        if (! is_array($subnetIds)) {
            $this->exception("Invalid subnet ID.");
        }

        $params = [
            'DBSubnetGroupDescription' => $groupDesc,
            'DBSubnetGroupName' => $groupName,
            'SubnetIds' => $subnetIds,
        ];

        return $this->sendRequest('createDBSubnetGroup', $params)->get('DBSubnetGroup');
    }

    public function deleteDbSubnetGroup($groupName)
    {
        $params = [
            'DBSubnetGroupName' => $groupName,
        ];

        try {
            return $this->sendRequest('deleteDBSubnetGroup', $params);
        } catch (AwsException $e) {
            return;
        }
    }
}
