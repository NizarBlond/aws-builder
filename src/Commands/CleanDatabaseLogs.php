<?php

namespace NizarBlond\AwsBuilder\Commands;

use NizarBlond\LaravelPlus\Abstracts\CommandBase;
use NizarBlond\AwsBuilder\Models\ActivityLog;
use \Carbon\Carbon;

class CleanDatabaseLogs extends CommandBase
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aws-builder:clean-db-logs {days-ago} {--exclude-errors}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans database logs older than the specified days.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days-ago');
        if (empty($days) || $days <= 0) {
            return;
        }

        $logs = ActivityLog::where(
            'created_at',
            '<',
            Carbon::now()->subDays($days)
        );

        if ($this->option('exclude-errors')) {
            $logs = $logs->where('error_level', '=', -1);
        }

        $this->log("Attempting to delete {$logs->count()} entries from $days ago...");
        $logs->delete();
    }
}
