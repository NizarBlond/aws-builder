<?php

namespace NizarBlond\AwsBuilder\Models;

use NizarBlond\LaravelPlus\Abstracts\ModelBase;
use NizarBlond\LaravelPlus\Support\Arr;
use Carbon\Carbon;

class ActivityLog extends ModelBase
{
   /**
    * Whether timestamps exist.
    *
    * @var bool
    */
    public $timestamps = false;

   /**
    * The casting array.
    *
    * @var array
    */
    protected $casts = [
        'input' => 'array',
        'output' => 'array'
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->table = config('aws-builder.database.table');

        parent::__construct($attributes);
    }
    
    /**
     * Records an activity to the database.
     *
     * @param   string    $service        The activity service name.
     * @param   string    $function       The activity function name.
     * @param   array     $input          The activity function input.
     * @param   array     $output         The activity function output. Optional.
     * @param   string    $duration       The duration in seconds. Optional.
     * @param   string    $error          The activity error. Optional.
     * @param   string    $errorLevel     The activity error level. Optional.
     * @param   string    $errorStack     The activity error stack. Optional.
     * @param   string    $templateName   The activity template. Optional.
     *
     * @return  ActivityLog
     */
    public static function record(
        $service,
        $function,
        $input,
        $output = null,
        $duration = null,
        $error = null,
        $errorLevel = -1,
        $errorStack = null,
        $templateName = null
    ) {
        $activity               = new ActivityLog;
        $activity->service      = $service;
        $activity->function     = $function;
        $activity->input        = $input;
        $activity->output       = $output;
        $activity->duration_s   = $duration;
        $activity->template     = $templateName;
        $activity->error_msg    = $error;
        $activity->error_stack  = $errorStack;
        $activity->error_level  = $errorLevel;
        $activity->save();

        return $activity;
    }

    /**
     * Records a failed activity to the database.
     *
     * @param   string              $service        The activity service name.
     * @param   string              $function       The activity function name.
     * @param   string              $input          The activity function input.
     * @param   string|Exception    $e              The activity exception.
     * @param   string              $duration       The duration in seconds. Optional.
     * @param   string              $errorLevel     The activity error level. Optional.
     * @param   string              $templateName   The activity template. Optional.
     *
     * @return  ActivityLog
     */
    public static function recordFailure(
        $service,
        $function,
        $input,
        $e,
        $duration = null,
        $output = null,
        $errorLevel = 1,
        $templateName = null
    ) {
        return self::record(
            $service,
            $function,
            $input,
            $output,
            $duration,
            is_string($e) ? $e : $e->getMessage(),
            $errorLevel,
            is_string($e) ? null : $e->getTraceAsString(),
            $templateName
        );
    }

    /**
     * Scope a query to get failed logs only.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetFailed($query)
    {
        return $query->where('error_level', '!=', -1);
    }

    /**
     * Scope a query to get successful logs only.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetSuccessful($query)
    {
        return $query->where('error_level', -1);
    }

    /**
     * Scope a query to get logs by service.
     *
     * @param string $service
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByService($query, $service)
    {
        return $query->where('service', $service);
    }
    
    /**
     * Scope a query to get logs by function.
     *
     * @param string $function
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetByFunction($query, $function)
    {
        return $query->where('function', $function);
    }
    
    /**
     * Scope a query to get logs by days ago.
     *
     * @param string $days
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeGetFromDaysAgo($query, $days)
    {
        return $query->where('created_at', '>', Carbon::now()->subDays($days));
    }

    /**
     * Encodes input to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setInputAttribute($value)
    {
        $value = $this->sanitizeArray($value);
        $this->attributes['input'] = $this->convertToJson($value);
    }

    /**
     * Encodes output to JSON in case of array given.
     *
     * @param   string|array  $value
     *
     * @return  void
     */
    public function setOutputAttribute($value)
    {
        $value = $this->sanitizeArray($value);
        $this->attributes['output'] = $this->convertToJson($value);
    }
}
