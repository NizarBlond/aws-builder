<?php


return [

    'aws' => [
        'region'            => env('AWS_REGION', 'ap-southeast-2'),
        'acm_region'        => env('AWS_ACM_REGION', env('AWS_REGION', 'us-east-1')),
        'access_key_id'     => env('AWS_ACCESS_KEY_ID', ''),
        'secret_access_key' => env('AWS_SECRET_ACCESS_KEY', ''),
    ],

    'database' => [

        'table' => 'aws_builder_activity_logs',
    ],

    'clients' => [

        'aws' => [
            'log_success' => env('AWS_CLIENT_LOG_SUCCESS', false),
            'log_failure' => env('AWS_CLIENT_LOG_FAILURE', true),
        ],

        'pdo' => [
            'log_success' => env('PDO_CLIENT_LOG_SUCCESS', false),
            'log_failure' => env('PDO_CLIENT_LOG_FAILURE', true),
        ]
    ],

];
